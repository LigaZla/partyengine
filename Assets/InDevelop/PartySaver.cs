﻿using UnityEngine;
using System;

namespace PartyEngine {
public static class PartySaver 
{
	
	const string PREFIX_X = "_x";
	const string PREFIX_Y = "_y";
	const string PREFIX_Z = "_z";
	const string PREFIX_W = "_w";
	
	public static void Save(){
		PlayerPrefs.Save();
	}
	
	public static void DeleteKey(string key){
		PlayerPrefs.DeleteKey(key);
	}
	
	public static void DeleteAllKeys(){
		PlayerPrefs.DeleteAll();
	}
	
	#region SET_VALUE
	
	public static void SetString(string key, string value){
		PlayerPrefs.SetString(key, value);
	}
	
	public static void SetInt(string key, int value){
		PlayerPrefs.SetInt(key, value);
	}
	
	public static void SetFloat(string key, float value){
		PlayerPrefs.SetFloat(key, value);
	}
	
	public static void SetByte(string key, byte value){
		SetInt(key, (int)value);
	}
	
	public static void SetBoolean(string key, bool value){
		SetString(key, value.ToString());
	}
	public static void SetBoolean(string key, int value){
		SetBoolean(key, value != 0);
	}
	public static void SetBoolean(string key, float value){
		SetBoolean(key, value != 0);
	}
	
	public static void SetVector(string key, Vector4 value){
		SetFloat(key+PREFIX_X, value.x);
		SetFloat(key+PREFIX_Y, value.y);
		SetFloat(key+PREFIX_Z, value.z);
		SetFloat(key+PREFIX_W, value.w);
	}
	
	public static void SetQuaternion(string key, Quaternion value){
		SetVector(key, new Vector4(value.x, value.y, value.z, value.w));
	}
	
	#endregion
	
	
	#region GET_VALUE
	
	public static string GetString(string key){
		return PlayerPrefs.GetString(key, "");
	}
	
	public static int GetInt(string key){
		return PlayerPrefs.GetInt(key, 0);
	}
	
	public static float GetFloat(string key){
		return PlayerPrefs.GetFloat(key, 0f);
	}
	
	public static byte GetByte(string key){
		return (byte)GetInt(key);
	}
	
	public static bool GetBoolean(string key){
		return Convert.ToBoolean(GetString(key));
	}
	
	public static Vector4 GetVector(string key){
		return new Vector4(
			GetFloat(key+PREFIX_X),
			GetFloat(key+PREFIX_Y),
			GetFloat(key+PREFIX_Z),
			GetFloat(key+PREFIX_W)
		);
	}
	
	public static Quaternion GetQuaternion(string key){
		return new Quaternion(
			GetFloat(key+PREFIX_X),
			GetFloat(key+PREFIX_Y),
			GetFloat(key+PREFIX_Z),
			GetFloat(key+PREFIX_W)	
		);
	}
	
	#endregion

}}