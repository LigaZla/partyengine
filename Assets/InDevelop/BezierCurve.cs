﻿using UnityEngine;
using System.Collections.Generic;

namespace PartyEngine.Blueprints {
public class BezierCurve {
	
	const int SEGMENTS = 25;
	const float SEGMENTS_FLOAT = SEGMENTS-1f;
	
	public readonly Vector3[] points = new Vector3[SEGMENTS-1];
	
	public BezierCurve(Vector3 _from, Vector3 _fromLever, Vector3 _to, Vector3 _toLever)
	{
		float t = 0; 
		float t1 = 0;
		for (int i = 1; i < SEGMENTS; i++)
		{
			t = (i - 1f) / SEGMENTS_FLOAT;
			t1 = i / SEGMENTS_FLOAT;
			points[i-1] = Evaluate(t, _from, _fromLever, _to, _toLever);
			points[i-1] = Evaluate(t1, _from, _fromLever, _to, _toLever);
		}
	}
	Vector3 Evaluate(float t, Vector3 _from, Vector3 _fromLever, Vector3 _to, Vector3 _toLever)
	{
		float t1 = 1 - t;
		return 
			t1 * t1 * t1 * _from + 
			3 * t * t1 * t1 * _fromLever +
			3 * t * t * t1 * _toLever + 
			t * t * t * _to
		;
	}
}}