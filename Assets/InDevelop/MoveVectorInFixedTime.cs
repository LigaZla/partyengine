﻿using UnityEngine;

namespace PartyEngine {
public class MoveVectorInFixedTime {

Vector4 target;
float time;

public MoveVectorInFixedTime(Vector4 start, Vector4 target, float time){
	this.target = target;
	this.time = Vector4.Distance(target, start) / (time / Time.fixedDeltaTime);
}

public Vector4 MoveTowards(Vector4 value){
	return Vector4.MoveTowards(value, target, time);
}

	
}}