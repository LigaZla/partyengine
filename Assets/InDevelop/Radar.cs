﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PartyEngine.Blueprints {
public class Radar {
	
	public bool IsEmpty { get { return hits == null || hits.Length < 1;}}
		
	Transform center;
	Collider[] hits;
	LayerMask layerMask;
	int currentIndex;
	
	public Radar(Transform _transform, LayerMask _mask)
	{
		center = _transform;
		layerMask = _mask;
	}
	
	public void Search(float _distance) 
	{
		hits = Physics.OverlapSphere(center.position, _distance, layerMask);
		
		MinToMaxSort();
		
		currentIndex = 0;
	}
	void MinToMaxSort()
	{
		Collider temp;
		for(int i = 0; i < hits.Length; i++)
		{
			for(int j = 0; j < hits.Length - 1; j++)
			{
				if(
					Vector3.Distance(center.position, hits[j].transform.position) 
					>
					Vector3.Distance(center.position, hits[j + 1].transform.position)
				)
				{
					temp = hits[j];
					hits[j] = hits[j + 1];
					hits[j + 1] = temp;
				}
			}
		}
	}
	
	public Transform GetNearestTarget()
	{
		if(!IsEmpty) 
			return hits[0].transform;

		return null;
	}
	
	public Transform GetFurtherTarget()
	{
		if(!IsEmpty) 
			return hits[hits.Length-1].transform;

		return null;
	}
	
	public Transform GetRandomTarget()
	{
		if(!IsEmpty)
			return Array.GetRandomItem(hits).transform;
			
		return null;
	}
	
	public Transform GetNextTarget()
	{
		for(; currentIndex<hits.Length; )
		{
			return hits[currentIndex++].transform;
		}
		
		return null;
	}
	
}}