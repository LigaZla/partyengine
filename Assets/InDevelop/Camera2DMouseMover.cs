﻿using UnityEngine;
using PartyEngine.Ext;
//TODO : Need rework
namespace PartyEngine {
public class Camera2DMouseMover : MonoBehaviour 
{
	public static Camera2DMouseMover Instance {get; private set;}
	
	[SerializeField] float minZoom;
	[SerializeField] float maxZoom;
	[SerializeField] Vector2 mapSize;
	[SerializeField] Vector2 center;
	[SerializeField] float zoomSensivity = 2;
	[SerializeField] Camera mainCamera;
	[SerializeField] Camera[] otherCameras;
	[SerializeField] Transform movedTransform;
	[SerializeField] bool unclamped;

	public bool IsMoved {get; private set;}

	float AspectRatio { get { return (float)Screen.width / (float)Screen.height;}}
	Vector2 CameraVisibleSize { get { return new Vector2(mainCamera.orthographicSize*2f*AspectRatio, mainCamera.orthographicSize*2f);}}
	Vector2 Max { get { return (Vector2)VectorExt.Positive((mapSize-CameraVisibleSize) / 2f) + center;}}
	Vector2 Min { get { return -(Vector2)VectorExt.Positive((mapSize-CameraVisibleSize) / 2f) + center;}}
	
	Vector2 basePos, startPos;
	
	void Awake()
	{
		Instance = this;
		
		if(unclamped)
		{
			mapSize = new Vector2(float.MaxValue-1, float.MaxValue-1);
		}
	}
	
	void Update()
	{
		//Move();
		if(PartyInput.MiddleMouseDown) StartMove();
		if(PartyInput.MiddleMouse) Move();
		if(PartyInput.MiddleMouseUp) StopMove();
		
		Zoom(PartyInput.MouseScroll);
	}
	
	public void StartMove()
	{
		basePos = movedTransform.transform.position;
		startPos = Input.mousePosition;
		IsMoved = true;
	}
	void Move()
	{
		if(!IsMoved) return;

		Vector2 step = (Vector2)Input.mousePosition - startPos;
		SetPosition(new Vector2(
			basePos.x - step.x * (mainCamera.orthographicSize * 2 * AspectRatio / Screen.width),
			basePos.y - step.y * (mainCamera.orthographicSize * 2 / Screen.height)
		));
	}
	
	public void StopMove()
	{
		IsMoved = false;
	}
	
	public void Zoom(float speed)
	{
		if(speed != 0)
			SetZoom(mainCamera.orthographicSize - speed*zoomSensivity);
	}
		
	public void SetZoom(float f)
	{
		mainCamera.orthographicSize = Mathf.Clamp(f, minZoom, maxZoom);
		SetPosition(movedTransform.transform.position);
		
		otherCameras.DoAction(x => x.orthographicSize = mainCamera.orthographicSize);
	}
	
	void SetPosition(Vector2 pos)
	{
		movedTransform.transform.position = new Vector3
		(
			Mathf.Clamp(pos.x, Min.x, Max.x), 
			Mathf.Clamp(pos.y, Min.y, Max.y),
			movedTransform.transform.position.z
		);
	}
	
	public Vector3 MouseToWorld()
	{
		return mainCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, -mainCamera.transform.position.z));
	}
}}