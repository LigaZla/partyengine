﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;

namespace PartyEngine {
public class XML {
	public static readonly XML Instance = new XML();
	
	XmlDocument doc;

	public void LoadDoc(string path)
	{
		doc = new XmlDocument();
		doc.Load(path);
	}
	
	
	public static string GetAttribute(XmlNode node, string attribute)
	{
		return (node as XmlElement).GetAttribute(attribute);
	}
	public static int GetAttributeAsInt(XmlNode node, string attribute)
	{
		return System.Convert.ToInt32(GetAttribute(node, attribute));
	}
	
	
}}