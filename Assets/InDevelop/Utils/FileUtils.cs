﻿using System;
using System.IO;

namespace TalkingHeads.CameraApp
{
    public static class FileUtils
    {
        /// <summary>
        ///     Gets the relative path ("..\..\to_file_or_dir") of the current file/dir (to) in relation to another (from)
        /// </summary>
        /// <param name="to"></param>
        /// <param name="from"></param>
        /// <returns></returns>
        public static string GetRelativePathFrom(this FileSystemInfo to, FileSystemInfo from)
        {
            return from.GetRelativePathTo(to);
        }

        /// <summary>
        ///     Gets the relative path ("..\..\to_file_or_dir") of another file or directory (to) in relation to the current
        ///     file/dir (from)
        /// </summary>
        /// <param name="to"></param>
        /// <param name="from"></param>
        /// <returns></returns>
        public static string GetRelativePathTo(this FileSystemInfo from, FileSystemInfo to)
        {
            Func<FileSystemInfo, string> getPath = fsi =>
            {
                var d = fsi as DirectoryInfo;
                return d == null ? fsi.FullName : d.FullName.TrimEnd(Path.DirectorySeparatorChar) + Path.DirectorySeparatorChar;
            };

            string fromPath = getPath(from);
            string toPath = getPath(to);

            var fromUri = new Uri(fromPath);
            var toUri = new Uri(toPath);

            var relativeUri = fromUri.MakeRelativeUri(toUri);
            string relativePath = Uri.UnescapeDataString(relativeUri.ToString());

            return relativePath.Replace('/', Path.DirectorySeparatorChar);
        }

        public static bool IsPathsEqual(string a, string b)
        {
            return string.Compare(
                Path.GetFullPath(a).TrimEnd('\\'),
                Path.GetFullPath(b).TrimEnd('\\'),
                StringComparison.InvariantCultureIgnoreCase) == 0;
        }
    }
}