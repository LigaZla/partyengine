﻿using System;
using UnityEngine.Profiling;
using Object = UnityEngine.Object;

namespace TalkingHeads.CameraApp
{
    public struct ProfilerSample : IDisposable
    {
        public ProfilerSample(string name)
        {
            Profiler.BeginSample(name);
        }

        public ProfilerSample(string name, Object targetObject)
        {
            Profiler.BeginSample(name, targetObject);
        }

        public void Dispose()
        {
            Profiler.EndSample();
        }
    }
}