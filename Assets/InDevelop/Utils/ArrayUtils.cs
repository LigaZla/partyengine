﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TalkingHeads.CameraApp
{
    public static class ArrayUtils
    {
        public static T Find<T>(this T[] array, Predicate<T> match)
        {
            return Array.Find(array, match);
        }

        public static bool Exists<T>(this T[] array, Predicate<T> match)
        {
            return Array.Exists(array, match);
        }

        public static bool Contains<T>(this T[] array, T value)
        {
            return Array.IndexOf(array, value) != -1;
        }

        public static int FindIndex<T>(this T[] array, Predicate<T> match)
        {
            return Array.FindIndex(array, match);
        }

        public static int IndexOf<T>(this T[] array, T value)
        {
            return Array.IndexOf(array, value);
        }

        public static bool SequenceEqualNullable<T>(this IEnumerable<T> a, IEnumerable<T> b)
        {
            return a == null || b == null ? a == b : a.SequenceEqual(b);
        }
    }
}