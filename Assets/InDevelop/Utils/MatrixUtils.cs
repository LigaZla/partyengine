﻿using UnityEngine;
using UnityEngine.Assertions;

namespace TalkingHeads.CameraApp
{
    public static class MatrixUtils
    {
        public static Matrix4x4 ArrayToMatrix(double[] array)
        {
            Assert.AreEqual(array.Length, 16);
            var mat = new Matrix4x4();
            for (int y = 0; y < 4; y++)
            for (int x = 0; x < 4; x++)
                mat[y, x] = (float) array[y * 4 + x];
            return mat;
        }

        public static Matrix4x4 ArrayToMatrix(float[] array)
        {
            Assert.AreEqual(array.Length, 16);
            var mat = new Matrix4x4();
            for (int y = 0; y < 4; y++)
            for (int x = 0; x < 4; x++)
                mat[y, x] = array[y * 4 + x];
            return mat;
        }

        public static float GetProjectionFieldOfView(this Matrix4x4 proj)
        {
            float t = proj.m11;
            float fov = Mathf.Atan(1.0f / t) * 2.0f * Mathf.Rad2Deg;
            return fov;
        }
    }
}