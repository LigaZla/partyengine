﻿using System;
using System.Collections.Generic;

namespace TalkingHeads.CameraApp
{
    internal class ListPool<T> : CollectionPool<List<T>, T>
    {
    }

    internal class HashSetPool<T> : CollectionPool<HashSet<T>, T>
    {
    }

    internal class DictionaryPool<K, V> : CollectionPool<Dictionary<K, V>, KeyValuePair<K, V>>
    {
    }

    internal struct CollectionPoolScope<T, V> : IDisposable where T : ICollection<V>, new()
    {
        public readonly T Data;

        public CollectionPoolScope(T data)
        {
            Data = data;
        }

        public void Dispose()
        {
            CollectionPool<T, V>.Release(Data);
        }
    }

    internal class CollectionPool<T, V> where T : ICollection<V>, new()
    {
        private static readonly ObjectPool<T> _listPool = new ObjectPool<T>(null, Clear);

        protected CollectionPool() { }

        public static T Get()
        {
            return _listPool.Get();
        }

        public static CollectionPoolScope<T, V> GetScoped()
        {
            return new CollectionPoolScope<T, V>(_listPool.Get());
        }

        public static void Release(T toRelease)
        {
            _listPool.Release(toRelease);
        }

        private static void Clear(T l)
        {
            l.Clear();
        }
    }
}