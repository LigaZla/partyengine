﻿using System.IO;
using UnityEngine;

namespace TalkingHeads.CameraApp
{
    public static class BuildInfo
    {
#if RELEASE_STORE_BUILD
        public const bool ReleaseStoreBuild = true;
#else
        public static bool ReleaseStoreBuild => false;
#endif
        public static bool EditorMode => Application.isEditor;
        public static string ContentSaveDir
        {
            get
            {
                string dir = Path.Combine(Application.temporaryCachePath, "shareable");
                if (!Directory.Exists(dir))
                    Directory.CreateDirectory(dir);
                return dir;
            }
        }
    }
}