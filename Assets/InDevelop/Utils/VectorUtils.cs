﻿using UnityEngine;

namespace TalkingHeads.CameraApp
{
    public static class VectorUtils
    {
        public static Vector3 Clamp(this Vector3 v, Vector3 min, Vector3 max)
        {
            return new Vector3(
                Mathf.Clamp(v.x, min.x, max.x),
                Mathf.Clamp(v.y, min.y, max.y),
                Mathf.Clamp(v.z, min.z, max.z));
        }

        public static Vector2 Clamp(this Vector2 v, float min, float max)
        {
            return new Vector2(
                Mathf.Clamp(v.x, min, max),
                Mathf.Clamp(v.y, min, max));
        }

        public static Vector3 Clamp(this Vector3 v, float min, float max)
        {
            return new Vector3(
                Mathf.Clamp(v.x, min, max),
                Mathf.Clamp(v.y, min, max),
                Mathf.Clamp(v.z, min, max));
        }

        public static Vector4 Clamp(this Vector4 v, float min, float max)
        {
            return new Vector4(
                Mathf.Clamp(v.x, min, max),
                Mathf.Clamp(v.y, min, max),
                Mathf.Clamp(v.z, min, max),
                Mathf.Clamp(v.w, min, max));
        }

        public static Vector3 WithX(this Vector3 v, float x)
        {
            v.x = x;
            return v;
        }

        public static Vector3 WithY(this Vector3 v, float y)
        {
            v.y = y;
            return v;
        }

        public static Vector3 WithZ(this Vector3 v, float z)
        {
            v.z = z;
            return v;
        }

        public static Vector2 WithX(this Vector2 v, float x)
        {
            v.x = x;
            return v;
        }

        public static Vector2 WithY(this Vector2 v, float y)
        {
            v.y = y;
            return v;
        }

        public static Vector3 WithZ(this Vector2 v, float z)
        {
            return new Vector3(v.x, v.y, z);
        }

        public static Vector3 Pow(this Vector3 v, float pow)
        {
            return new Vector3(Mathf.Pow(v.x, pow), Mathf.Pow(v.y, pow), Mathf.Pow(v.z, pow));
        }

        public static Vector4 Pow(this Vector4 v, float pow)
        {
            return new Vector4(Mathf.Pow(v.x, pow), Mathf.Pow(v.y, pow), Mathf.Pow(v.z, pow), Mathf.Pow(v.w, pow));
        }

        public static Vector2 Mul(this Vector2 a, Vector2 b)
        {
            return new Vector2(a.x * b.x, a.y * b.y);
        }

        public static Vector3 Mul(this Vector3 a, Vector3 b)
        {
            return new Vector3(a.x * b.x, a.y * b.y, a.z * b.z);
        }

        public static Vector4 Mul(this Vector4 a, Vector4 b)
        {
            return new Vector4(a.x * b.x, a.y * b.y, a.z * b.z, a.w * b.w);
        }

        public static Vector4 Div(this Vector4 a, Vector4 b)
        {
            return new Vector4(a.x / b.x, a.y / b.y, a.z / b.z, a.w / b.w);
        }

        public static Vector3 Div(this Vector3 a, Vector3 b)
        {
            return new Vector3(a.x / b.x, a.y / b.y, a.z / b.z);
        }

        public static Vector2 Div(this Vector2 a, Vector2 b)
        {
            return new Vector2(a.x / b.x, a.y / b.y);
        }

        public static float Average(this Vector2 v)
        {
            return (v.x + v.y) / 2f;
        }

        public static float Average(this Vector3 v)
        {
            return (v.x + v.y + v.z) / 3f;
        }

        public static float Average(this Vector4 v)
        {
            return (v.x + v.y + v.z + v.w) / 4f;
        }

        public static Vector2 Abs(this Vector2 v)
        {
            return new Vector2(Mathf.Abs(v.x), Mathf.Abs(v.y));
        }

        public static Vector3 Abs(this Vector3 v)
        {
            return new Vector3(Mathf.Abs(v.x), Mathf.Abs(v.y), Mathf.Abs(v.z));
        }

        public static Vector4 Abs(this Vector4 v)
        {
            return new Vector4(Mathf.Abs(v.x), Mathf.Abs(v.y), Mathf.Abs(v.z), Mathf.Abs(v.w));
        }

        public static float AngleZ(this Vector2 v)
        {
            return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
        }

        public static float Min(this Vector2 v)
        {
            return Mathf.Min(v.x, v.y);
        }

        public static float Min(this Vector3 v)
        {
            return Mathf.Min(Mathf.Min(v.x, v.y), v.z);
        }

        public static float Min(this Vector4 v)
        {
            return Mathf.Min(Mathf.Min(v.x, v.y), Mathf.Min(v.z, v.w));
        }

        public static float Max(this Vector2 v)
        {
            return Mathf.Max(v.x, v.y);
        }

        public static float Max(this Vector3 v)
        {
            return Mathf.Max(Mathf.Max(v.x, v.y), v.z);
        }

        public static float Max(this Vector4 v)
        {
            return Mathf.Max(Mathf.Max(v.x, v.y), Mathf.Max(v.z, v.w));
        }

        public static Vector3 ReplaceInfNans(this Vector3 vec, float value)
        {
            return new Vector3(
                float.IsNaN(vec.x) || float.IsInfinity(vec.x) ? value : vec.x,
                float.IsNaN(vec.y) || float.IsInfinity(vec.y) ? value : vec.y,
                float.IsNaN(vec.z) || float.IsInfinity(vec.z) ? value : vec.z);
        }

        public static Vector2 ReplaceInfNans(this Vector2 vec, float value)
        {
            return new Vector2(
                float.IsNaN(vec.x) || float.IsInfinity(vec.x) ? value : vec.x,
                float.IsNaN(vec.y) || float.IsInfinity(vec.y) ? value : vec.y);
        }
    }
}