﻿using UnityEngine;

namespace TalkingHeads.CameraApp
{
    public static class GameObjectUtils
    {
        public static void SetActiveNullCheck(this GameObject obj, bool active)
        {
            if (obj != null)
            {
                obj.SetActive(active);
            }
        }
    }
}