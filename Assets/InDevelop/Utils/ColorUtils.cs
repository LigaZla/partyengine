﻿using UnityEngine;

namespace TalkingHeads.CameraApp
{
    public static class ColorUtils
    {
        public static Color WithA(this Color color, float a)
        {
            color.a = a;
            return color;
        }

        public static Vector4 ToVectorRGBA(this Color c)
        {
            return new Vector4(c.r, c.g, c.b, c.a);
        }

        public static Vector3 ToVectorRGB(this Color c)
        {
            return new Vector3(c.r, c.g, c.b);
        }

        public static Color ToColor(this Vector4 c)
        {
            return new Color(c.x, c.y, c.z, c.w);
        }

        public static Color ToColor(this Vector3 c)
        {
            return new Color(c.x, c.y, c.z, 1);
        }

        public static ColorHSVA ToColorHSVA(this Vector4 c)
        {
            return new ColorHSVA(c.x, c.y, c.z, c.w);
        }

        public static int ToInt(this Color32 c)
        {
            return c.r + (c.g << 8) + (c.b << 16) + (c.a << 24);
        }

        public static Color32 ParseInt(int bin)
        {
            return new Color32(
                (byte) (bin & 0xFF),
                (byte) ((bin & (0xFF << 8)) >> 8),
                (byte) ((bin & (0xFF << 16)) >> 16),
                (byte) ((bin & (0xFF << 24)) >> 24));
        }

        public static ColorHSVA ToHSVA(this Color color)
        {
            float h, s, v;
            Color.RGBToHSV(color, out h, out s, out v);
            return new ColorHSVA(h, s, v, color.a);
        }
    }
}