﻿/*using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.Experimental.Rendering;
using UnityEngine.Rendering;

namespace TalkingHeads.CameraApp
{
    public static class TextureUtils
    {
        public static Texture2D ToTexture2D(this RenderTexture rt, Texture2D outputTex = null)
        {
            var prevRt = RenderTexture.active;
            RenderTexture.active = rt;

            var tex = outputTex != null ? outputTex : new Texture2D(rt.width, rt.height, TextureFormat.ARGB32, false, false);
            tex.ReadPixels(new Rect(0, 0, tex.width, tex.height), 0, 0);
            tex.Apply();

            RenderTexture.active = prevRt;
            return tex;
        }

        public static async Task<Texture2D> ToTexture2DWithCopyAsync(this RenderTexture tex, Vector2Int outputSize = default)
        {
            var inputSize = new Vector2Int(tex.width, tex.height);
            if (outputSize == Vector2Int.zero)
                outputSize = inputSize;

            var texCopy = RenderTexture.GetTemporary(outputSize.x, outputSize.y, 0, tex.graphicsFormat);
            Graphics.Blit(tex, texCopy);

            var texture = await texCopy.ToTexture2DAsync();
            RenderTexture.ReleaseTemporary(texCopy);
            return texture;
        }

        public static RenderTexture CopyTemporary(this RenderTexture tex)
        {
            var copy = RenderTexture.GetTemporary(tex.descriptor);
            Graphics.Blit(tex, copy);
            return copy;
        }

        public static async Task<Texture2D> ToTexture2DAsync(this RenderTexture tex)
        {
            if (!SystemInfo.supportsAsyncGPUReadback || !SystemInfo.IsFormatSupported(tex.graphicsFormat, FormatUsage.ReadPixels))
                return ToTexture2D(tex);

            var texReq = AsyncGPUReadback.Request(tex, 0, TextureFormat.ARGB32);
            while (!texReq.done)
                await Awaiters.NextFrame;

            var texture = new Texture2D(tex.width, tex.height, TextureFormat.ARGB32, false, false);
            texture.LoadRawTextureData(texReq.GetData<uint>());
            texture.Apply();
            return texture;
        }

        public static RenderTexture CropAndResizeTemporary(this Texture src, int width, int height, bool flipVertically = false)
        {
            var (downsampled, releaseDownsampled) = DownsampleSSAA(src, width, height);

            var renderSize = new Vector2Int(downsampled.width, downsampled.height);
            var targetSize = new Vector2Int(width, height);
            var sizeDiff = Mathf.Max(renderSize.x, renderSize.y) / (float)Mathf.Max(targetSize.x, targetSize.y);
            var targetFullSize = (Vector2)targetSize * sizeDiff;

            var rt = RenderTexture.GetTemporary(targetSize.x, targetSize.y, 0, RenderTextureFormat.ARGB32);
            var scale = targetFullSize / renderSize;
            var offset = (Vector2.one - scale) / 2;
            if (flipVertically)
            {
                scale.y *= -1;
                offset.y = 1 - offset.y;
            }

            Graphics.Blit(downsampled, rt, scale, offset);

            if (releaseDownsampled && downsampled is RenderTexture srcRt)
                RenderTexture.ReleaseTemporary(srcRt);

            return rt;
        }

        public static (Texture tex, bool temporaryOutput) DownsampleSSAA(Texture src, int desiredWidth, int desiredHeight)
        {
            bool temporaryOutput = false;
            while (src.width >= desiredWidth * 2 && src.height >= desiredHeight * 2)
            {
                int w = Mathf.Max(desiredWidth, src.width / 2);
                int h = Mathf.Max(desiredHeight, src.height / 2);
                var downsampled = RenderTexture.GetTemporary(w, h, 0, RenderTextureFormat.ARGB32);
                Graphics.Blit(src, downsampled);
                if (temporaryOutput)
                    RenderTexture.ReleaseTemporary((RenderTexture) src);
                src = downsampled;
                temporaryOutput = true;
            }
            return (src, temporaryOutput);
        }

        public static void Clear(this RenderTexture tex, bool clearDepth, bool clearColor, Color color)
        {
            var prevRt = RenderTexture.active;
            RenderTexture.active = tex;
            GL.Clear(clearDepth, clearColor, color);
            RenderTexture.active = prevRt;
        }

        public static async UniTask<Texture2D> CreateReadableCopy(this Texture2D src)
        {
            var tempRt = RenderTexture.GetTemporary(src.width, src.height, 0, src.graphicsFormat);
            Graphics.Blit(src, tempRt);
            var res = await tempRt.ToTexture2DAsync();
            RenderTexture.ReleaseTemporary(tempRt);
            return res;
        }
        
        public static Vector2 ScreenToTexturePosition(Vector2 screenPosition, Camera uiCamera, RectTransform screenTransform, Vector2 textureSize)
        {
            RectTransformUtility.ScreenPointToLocalPointInRectangle(screenTransform, screenPosition, uiCamera, out var localPos);
            var rectSize = screenTransform.rect.size;
            return (localPos + rectSize / 2) / rectSize * textureSize;
        }
    }
}*/