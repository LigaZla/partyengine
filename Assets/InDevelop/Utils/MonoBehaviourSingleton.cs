﻿/*using System;
using TalkingHeads.CameraApp.Events;
using TalkingHeads.CameraApp.UI;
using UnityEngine;
using ZStickers.UI;

namespace TalkingHeads.CameraApp
{
    public abstract class MonoBehaviourSingleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T _instance;
        public static T Instance => _instance == null ? _instance = FindObjectOfType<T>() : _instance;

        protected virtual void Awake()
        {
            if (_instance != null && _instance != this)
                throw new InvalidOperationException($"_instance != null in {this}");
            _instance = this as T;
        }

        protected virtual void OnDestroy()
        {
            _instance = null;
        }
    }

    public abstract class MonoBehaviourAutoCreate<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T _instance;
        public static T Instance => _instance == null
            ? _instance = new GameObject(typeof(T).Name, typeof(T)).GetComponent<T>()
            : _instance;

        protected virtual void Awake()
        {
            if (_instance != null && _instance != this)
                throw new InvalidOperationException($"_instance != null in {this}");
            _instance = this as T;
        }

        protected virtual void OnDestroy()
        {
            _instance = null;
        }
    }

    public abstract class Singleton<T> where T : new()
    {
        private static T _instance;
        public static T Instance => _instance == null ? _instance = new T() : _instance;
    }

    public abstract class EventBehaviourSingleton<T> : EventBehaviour where T : EventBehaviour
    {
        private static T _instance;
        public static T Instance => _instance == null ? _instance = FindObjectOfType<T>() : _instance;
        public static T InstanceRefNullCheck => _instance ?? (_instance = FindObjectOfType<T>());

        protected virtual void Awake()
        {
            if (_instance != null && _instance != this)
                throw new InvalidOperationException($"_instance != null in {this}");
            _instance = this as T;
        }

        protected override void OnDestroy()
        {
            _instance = null;
            base.OnDestroy();
        }
    }

    public abstract class ScreenBehaviourSingleton<T> : ScreenBehaviour where T : ScreenBehaviour
    {
        private static T _instance;
        public static T Instance => _instance == null ? _instance = FindObjectOfType<T>() : _instance;

        protected virtual void Awake()
        {
            if (_instance != null && _instance != this)
                throw new InvalidOperationException($"_instance != null in {this}");
            _instance = this as T;
        }

        protected override void OnDestroy()
        {
            _instance = null;
            base.OnDestroy();
        }
    }
}*/