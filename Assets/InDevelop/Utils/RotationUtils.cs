﻿using UnityEngine;

namespace TalkingHeads.CameraApp
{
    public static class RotationUtils
    {
        public static float FixAngle(float angle)
        {
            if (angle > 180)
                return angle - 360;
            if (angle < -180)
                return angle + 360;
            return angle;
        }

        public static Vector3 FixAngle(Vector3 angle)
        {
            return new Vector3(FixAngle(angle.x), FixAngle(angle.y), FixAngle(angle.z));
        }

        public static Vector3 ToEulerFixed(this Quaternion q)
        {
            return FixAngle(q.eulerAngles);
        }
    }
}