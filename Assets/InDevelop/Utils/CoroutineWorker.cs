﻿using System.Collections;
using UnityEngine;

namespace TalkingHeads.CameraApp
{
    public class CoroutineWorker : MonoBehaviour
    {
        private static MonoBehaviour _instance;
        private static MonoBehaviour Instance
        {
            get
            {
                if (_instance != null)
                    return _instance;
                _instance = new GameObject("CoroutineWorker").AddComponent<CoroutineWorker>();
                _instance.gameObject.hideFlags = HideFlags.HideAndDontSave;
                DontDestroyOnLoad(_instance.gameObject);
                return _instance;
            }
        }

        public new static Coroutine StartCoroutine(IEnumerator ie)
        {
            return Instance.StartCoroutine(ie);
        }

        public new static Coroutine StartCoroutine(string methodName)
        {
            return Instance.StartCoroutine(methodName);
        }
    }
}