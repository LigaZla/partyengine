﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using Object = UnityEngine.Object;

namespace TalkingHeads.CameraApp
{
    public struct TRS
    {
        public Vector3 Position;
        public Quaternion Rotation;
        public Vector3 Scale;

        public TRS(Vector3 position, Quaternion rotation, Vector3 scale)
        {
            Position = position;
            Rotation = rotation;
            Scale = scale;
        }
    }

    public static class TransformUtils
    {
        public static void DestroyAllChildren(Transform transform)
        {
            for (int i = transform.childCount - 1; i >= 0; i--)
            {
                var tr = transform.GetChild(i);
                Object.Destroy(tr.gameObject);
                tr.SetParent(null);
            }
        }

        // to be used in Editor
        public static void DestroyAllChildrenImmediate(Transform transform)
        {
            for (int i = transform.childCount - 1; i >= 0; i--)
                Object.DestroyImmediate(transform.GetChild(i).gameObject);
        }

        public static Transform FindDeepChild(this Transform parent, string name)
        {
            var result = parent.Find(name);
            if (result != null)
                return result;
            foreach (Transform child in parent)
            {
                result = child.FindDeepChild(name);
                if (result != null)
                    return result;
            }

            return null;
        }

        public static List<Transform> FindDeepChildren(this Transform parent, string name)
        {
            return parent.GetComponentsInChildren<Transform>(true).Where(t => t.name == name).ToList();
        }

        public static Transform FindDeepChildrendRegex(this Transform parent, string pattern)
        {
            var result = parent.FindRegex(pattern);
            if (result != null)
                return result;
            foreach (Transform child in parent)
            {
                result = child.FindDeepChildrendRegex(pattern);
                if (result != null)
                    return result;
            }

            return null;
        }

        public static Transform FindRegex(this Transform trans, string pattern)
        {
            foreach (Transform child in trans)
            {
                if (Regex.IsMatch(child.name, pattern))
                    return child;
            }

            return null;
        }

        public static Transform FindParentDeep(this Transform trans, string name)
        {
            while (true)
            {
                if (trans.name == name) return trans;
                var p = trans.parent;
                if (p == null) return null;
                trans = p;
            }
        }

        public static void SetIdentity(this Transform t)
        {
            t.localPosition = Vector3.zero;
            t.localScale = Vector3.one;
            t.localRotation = Quaternion.identity;
        }

        public static void CopyLocalTRSFrom(this Transform dst, Transform src)
        {
            dst.localPosition = src.localPosition;
            dst.localScale = src.localScale;
            dst.localRotation = src.localRotation;
        }

        /// <summary>
        ///     Local to world
        /// </summary>
        public static Quaternion TransformRotation(this Transform t, Quaternion rot)
        {
            return t.rotation * rot;
        }

        /// <summary>
        ///     World to local
        /// </summary>
        public static Quaternion InverseTransformRotation(this Transform t, Quaternion rot)
        {
            return Quaternion.Inverse(t.rotation) * rot;
        }

        public static Transform GetChildActive(this Transform tr, int index)
        {
            int curIndex = 0;
            foreach (Transform child in tr)
            {
                if (!child.gameObject.activeSelf)
                    continue;
                if (index == curIndex)
                    return child;
                curIndex++;
            }

            return null;
        }

        public static int GetSiblingIndexActive(this Transform tr)
        {
            int curIndex = 0;
            foreach (Transform child in tr.parent)
            {
                if (child == tr)
                    return curIndex;
                if (child.gameObject.activeSelf)
                    curIndex++;
            }

            throw new InvalidOperationException();
        }

        public static TRS GetTRS(this Transform tr)
        {
            return new TRS(tr.position, tr.rotation, tr.lossyScale);
        }

        public static TRS GetLocalTRS(this Transform tr)
        {
            return new TRS(tr.localPosition, tr.localRotation, tr.localScale);
        }

        public static void SetTR(this Transform tr, TRS trs)
        {
            tr.SetPositionAndRotation(trs.Position, trs.Rotation);
        }

        public static void SetLocalTRS(this Transform tr, TRS trs)
        {
            tr.localPosition = trs.Position;
            tr.localRotation = trs.Rotation;
            tr.localScale = trs.Scale;
        }

        // set Layer for the object and all its children
        public static void SetLayerRecursively(GameObject obj, int newLayer)
        {
            if (null == obj)
                return;

            obj.layer = newLayer;

            foreach (Transform child in obj.transform)
            {
                if (null == child)
                    continue;
                SetLayerRecursively(child.gameObject, newLayer);
            }
        }

        public static string GetPathTo(this Transform from, Transform to, bool includeFrom)
        {
            var path = new List<Transform> { to };
            while (to != from)
            {
                to = to.parent;
                if (to == null)
                    return null;
                path.Insert(0, to);
            }
            if (!includeFrom)
                path.RemoveAt(0);
            return string.Join("/", path.Select(t => t.name));
        }
    } // class TransformUtils
} // namespace TalkingHeads.CameraApp