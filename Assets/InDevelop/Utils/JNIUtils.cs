﻿using System;
using UnityEngine;

namespace TalkingHeads.CameraApp
{
    public static class JNIUtils
    {
        public static Func<T> GetMethod<T>(this AndroidJavaObject obj, string methodName)
        {
            object[] emptyArgs = new object[0];
            var jclass = obj.GetRawClass();
            var method = AndroidJNIHelper.GetMethodID<T>(
                jclass, methodName, emptyArgs, false);

            var jobj = obj.GetRawObject();
            var emptyJArgs = new jvalue[0];
            var type = typeof(T);
            if (type == typeof(long))
                return (Func<T>) (object) (Func<long>) (() => AndroidJNI.CallLongMethod(jobj, method, emptyJArgs));
            if (type == typeof(int))
                return (Func<T>) (object) (Func<int>) (() => AndroidJNI.CallIntMethod(jobj, method, emptyJArgs));
            if (type == typeof(bool))
                return (Func<T>) (object) (Func<bool>) (() => AndroidJNI.CallBooleanMethod(jobj, method, emptyJArgs));
            if (type == typeof(string))
                return (Func<T>) (object) (Func<string>) (() => AndroidJNI.CallStringMethod(jobj, method, emptyJArgs));
            if (type == typeof(IntPtr))
                return (Func<T>) (object) (Func<IntPtr>) (() => AndroidJNI.CallObjectMethod(jobj, method, emptyJArgs));
            throw new NotImplementedException($"Unknown {type} for {methodName}");
        }
    }
}