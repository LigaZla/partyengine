﻿using System;
using UnityEngine;

namespace TalkingHeads.CameraApp
{
    [Serializable]
    public struct ColorHSVA : IEquatable<ColorHSVA>
    {
        public float h;
        public float s;
        public float v;
        public float a;

        public ColorHSVA(float h, float s, float v, float a)
        {
            this.h = h;
            this.s = s;
            this.v = v;
            this.a = a;
        }

        public Vector4 ToVector()
        {
            return new Vector4(h, s, v, a);
        }

        public Color ToRGBA()
        {
            return Color.HSVToRGB(h, s, v).WithA(a);
        }

        public static ColorHSVA FromRGBA(Color color)
        {
            Color.RGBToHSV(color, out float h, out float s, out float v);
            return new ColorHSVA(h, s, v, color.a);
        }

        public Color ToColorMemcpy()
        {
            return new Color(h, s, v, a);
        }

        public ColorHSVA FromColorMemcpy(Color color)
        {
            return new ColorHSVA(color.r, color.g, color.b, color.a);
        }

        public ColorHSVA WithH(float h)
        {
            return new ColorHSVA(h, s, v, a);
        }

        public ColorHSVA WithS(float s)
        {
            return new ColorHSVA(h, s, v, a);
        }

        public ColorHSVA WithV(float v)
        {
            return new ColorHSVA(h, s, v, a);
        }

        public ColorHSVA WithA(float a)
        {
            return new ColorHSVA(h, s, v, a);
        }

        public override string ToString()
        {
            return $"HSVA({h:0.00}, {s:0.00}, {v:0.00}, {a:0.00})";
        }

        public static ColorHSVA Lerp(ColorHSVA a, ColorHSVA b, float t)
        {
            var v = Vector4.Lerp(a.ToVector(), b.ToVector(), t);
            return new ColorHSVA(v.x, v.y, v.z, v.w);
        }

        public static float InverseLerp(ColorHSVA a, ColorHSVA b, ColorHSVA value)
        {
            var gr = b.ToVector() - a.ToVector();
            var col = value.ToVector() - a.ToVector();
            return col.magnitude / Mathf.Max(1e-6f, gr.magnitude);
        }

        public static readonly ColorHSVA white = new ColorHSVA(0, 0, 1, 1);
        public static readonly ColorHSVA black = new ColorHSVA(0, 0, 0, 1);
        public static readonly ColorHSVA gray = new ColorHSVA(0, 0, 0.5f, 1);

        #region Equality members

        public bool Equals(ColorHSVA other)
        {
            return h.Equals(other.h) && s.Equals(other.s) && v.Equals(other.v) && a.Equals(other.a);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is ColorHSVA && Equals((ColorHSVA)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = h.GetHashCode();
                hashCode = (hashCode * 397) ^ s.GetHashCode();
                hashCode = (hashCode * 397) ^ v.GetHashCode();
                hashCode = (hashCode * 397) ^ a.GetHashCode();
                return hashCode;
            }
        }

        public static bool operator ==(ColorHSVA left, ColorHSVA right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(ColorHSVA left, ColorHSVA right)
        {
            return !left.Equals(right);
        }

        #endregion
    }
}