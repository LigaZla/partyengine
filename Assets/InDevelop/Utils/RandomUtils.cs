﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using Random = UnityEngine.Random;

namespace TalkingHeads.CameraApp
{
    public static class RandomUtils
    {
        public static T Sample<T>(this IList<T> arr)
        {
            return arr[Random.Range(0, arr.Count)];
        }

        public static T Sample<T>(this T[] arr, float[] priorities)
        {
            Assert.AreEqual(arr.Length, priorities.Length);

            float totalPrioritySum = 0;
            for (int i = 0; i < priorities.Length; i++)
                totalPrioritySum += priorities[i];

            float point = Random.Range(0, totalPrioritySum);
            float curSum = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                curSum += priorities[i];
                if (curSum > point)
                    return arr[i];
            }

            return arr[arr.Length - 1];
        }

        public static T Sample<T>(this T[] arr, Func<T, float> priorities)
        {
            float totalPrioritySum = 0;
            for (int i = 0; i < arr.Length; i++)
                totalPrioritySum += priorities(arr[i]);

            float point = Random.Range(0, totalPrioritySum);
            float curSum = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                curSum += priorities(arr[i]);
                if (curSum > point)
                    return arr[i];
            }

            return arr[arr.Length - 1];
        }

        public static float RandomNormal()
        {
            return Mathf.Cos(2 * Mathf.PI * Random.value) * Mathf.Sqrt(-2 * Mathf.Log(Random.value));
        }
    }
}