﻿using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;

namespace TalkingHeads.CameraApp
{
    public static class AsyncUtils
    {
        private static readonly HashSet<object> _guards = new HashSet<object>();

        public static async UniTask DiscardSimultaneousCalls(object guard, Func<UniTask> fn)
        {
            if (_guards.Add(guard))
            {
                try
                {
                    await fn();
                }
                finally
                {
                    _guards.Remove(guard);
                }
            }
        }

        public static void DiscardSimultaneousCalls(object guard, Action fn)
        {
            if (_guards.Add(guard))
            {
                try
                {
                    fn();
                }
                finally
                {
                    _guards.Remove(guard);
                }
            }
        }
    }
}