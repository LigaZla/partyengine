﻿using UnityEngine;

namespace TalkingHeads.CameraApp
{
    public struct Vector3Wrapper
    {
        public Vector3 Vector { get; private set; }

        public Vector3Wrapper(Vector3 v)
        {
            Vector = v;
        }

        public static implicit operator Vector3Wrapper(Vector3 v)
        {
            return new Vector3Wrapper(v);
        }

        public static implicit operator Vector3(Vector3Wrapper v)
        {
            return new Vector3(v.Vector.x, v.Vector.y, v.Vector.z);
        }

        public static Vector3Wrapper operator *(Vector3Wrapper aw, Vector3 b)
        {
            Vector3 a = aw;
            return new Vector3(a.x * b.x, a.y * b.y, a.z * b.z);
        }

        public static Vector3Wrapper operator *(Vector3 a, Vector3Wrapper bw)
        {
            Vector3 b = bw;
            return new Vector3(a.x * b.x, a.y * b.y, a.z * b.z);
        }

        public static Vector3Wrapper operator *(Vector3Wrapper aw, Vector3Wrapper bw)
        {
            Vector3 a = aw;
            Vector3 b = bw;
            return new Vector3(a.x * b.x, a.y * b.y, a.z * b.z);
        }

        public static Vector3Wrapper operator /(Vector3Wrapper aw, Vector3 b)
        {
            Vector3 a = aw;
            return new Vector3(a.x / b.x, a.y / b.y, a.z / b.z);
        }

        public static Vector3Wrapper operator /(Vector3 a, Vector3Wrapper bw)
        {
            Vector3 b = bw;
            return new Vector3(a.x / b.x, a.y / b.y, a.z / b.z);
        }

        public static Vector3Wrapper operator /(Vector3Wrapper aw, Vector3Wrapper bw)
        {
            Vector3 a = aw;
            Vector3 b = bw;
            return new Vector3(a.x / b.x, a.y / b.y, a.z / b.z);
        }
    }
}