﻿namespace TalkingHeads.CameraApp
{
    public static class MathUtils
    {
        // ease squad - derivate
        public static float EaseInOutQuadDerivate(float start, float end, float value)
        {
            value /= .5f;
            end -= start;

            if (value < 1)
                return end * value;

            value--;

            return end * (1 - value);
        }

        // ease squad - value
        public static float EaseInOutQuad(float start, float end, float value)
        {
            value /= .5f;
            end -= start;
            if (value < 1) return end * 0.5f * value * value + start;
            value--;
            return -end * 0.5f * (value * (value - 2) - 1) + start;
        }
    }
}