﻿using System;
using UnityEngine;

namespace TalkingHeads.CameraApp
{
    public static class SpriteUtils
    {
        public static Sprite CreateSprite(this Texture2D texture)
        {
            return Sprite.Create(
                texture,
                new Rect(0, 0, texture.width, texture.height),
                new Vector2(texture.width / 2f, texture.height / 2f),
                100);
        }
    }
}