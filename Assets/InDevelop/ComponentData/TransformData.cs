﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PartyEngine.ComponentData
{
	[System.Serializable]
	public class TransformData : BaseData<Transform, TransformData>
	{
		[SerializeField] Vector3 position;
		[SerializeField] Vector3 rotation;
		[SerializeField] Vector3 scale;

		public override void SetData(Transform target)
		{
			target.position = position;
			target.eulerAngles = rotation;
			target.localScale = scale;
		}
		
		public override bool Equals(Transform target)
		{
			if(target.position != position) return false;
			if(target.eulerAngles != rotation) return false;
			if(target.localScale != scale) return false;
			
			return true;
		}
	}
}