﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PartyEngine.ComponentData
{
	[System.Serializable]
	public class RectData : BaseData<RectTransform, RectData>
	{
		[SerializeField] Vector3 anchoredPosition;
		[SerializeField] Vector2 sizeDelta;
		[SerializeField] Vector2 anchorMin = new Vector2(0.5f, 0.5f);
		[SerializeField] Vector2 anchorMax = new Vector2(0.5f, 0.5f);
		[SerializeField] Vector2 pivot = new Vector2(0.5f, 0.5f);

		public override void SetData(RectTransform target)
		{
			target.anchorMax = anchorMax;
			target.anchorMin = anchorMin;
			target.pivot = pivot;
			target.anchoredPosition3D = anchoredPosition;
			target.sizeDelta = sizeDelta;
		}
		
		public override bool Equals(RectTransform target)
		{
			if(target.anchorMax != anchorMax) return false;
			if(target.anchorMin != anchorMin) return false;
			if(target.pivot != pivot) return false;
			if(target.anchoredPosition3D != anchoredPosition) return false;
			if(target.sizeDelta != sizeDelta) return false;
			
			return true;
		}
	}
}