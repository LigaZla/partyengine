﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PartyEngine.ComponentData 
{
	[System.Serializable]
	public abstract class Data
	{
		//TODO : add parse
		/// <summary>/// Return JSON representation of this component/// </summary>
		public override string ToString()
		{
			return JsonUtility.ToJson(this);
		}
	}
		
	[System.Serializable]
	public abstract class BaseData<COMPONENT, DATA> : Data
	{
		/// <summary>
		/// Set the parametres to target
		/// </summary>
		public abstract void SetData(COMPONENT target);
	
		/// <summary>
		/// Target parametres equals current?
		/// </summary>
		public abstract bool Equals(COMPONENT target);

	}
}
