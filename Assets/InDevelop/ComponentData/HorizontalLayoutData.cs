﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PartyEngine.ComponentData 
{
	[System.Serializable]
	public class HorizontalLayoutData : BaseData<HorizontalLayoutGroup, HorizontalLayoutData>
	{
		[SerializeField] RectOffset padding;
		[SerializeField] float spacing;
		[SerializeField] TextAnchor childAlignment;
		[SerializeField] bool childControlWidth;
		[SerializeField] bool childControlHeight;
		[SerializeField] bool childForceExpandWidth;
		[SerializeField] bool childForceExpandHeight;

		public override void SetData(HorizontalLayoutGroup target)
		{
			target.padding = padding;
			target.spacing = spacing;
			
			target.childAlignment = childAlignment;
			
			target.childControlWidth = childControlWidth;
			target.childControlHeight = childControlHeight;
			target.childForceExpandWidth = childForceExpandWidth;
			target.childForceExpandHeight = childForceExpandHeight;
		}
		
		public override bool Equals(HorizontalLayoutGroup target)
		{
			//TODO : add comparsions
			return false;
		}
	}
}