﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PartyEngine {
public class Raycasting  {
	public static Raycasting Instance { get { if(instance == null) instance = new Raycasting(); return instance;}}
	static Raycasting instance;
	
	Transform rayCaster;
	
	public Raycasting()
	{
		rayCaster = new GameObject("Raycaster").transform;
	}
	
	public bool Raycast(Vector3 _from, Vector3 _to, LayerMask _mask)
	{
		RaycastHit hit;
		return Raycast(_from, _to, _mask, out hit);
	}
	
	public bool Raycast(Vector3 _from, Vector3 _to, LayerMask _mask, out RaycastHit _hit)
	{
		rayCaster.transform.position = _from;
		rayCaster.LookAt(_to);

		return Physics.Raycast(rayCaster.position, rayCaster.forward, out _hit, Vector3.Distance(_from, _to), _mask);
	}
	
}}