﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace PartyEngine.Blueprints {
public class Joystic : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

	[SerializeField] RectTransform toggle;
	[SerializeField] float distance;
	[SerializeField] Joystic another;
	
	Vector2 startPosition;
	Vector2 angelTarget;
	
	float angle;
	float dist;
	
#if UNITY_ANDROID || UNITY_IOS
	int index;
#endif
	Vector2 startTouch;
	
	public bool Moved {get; private set;}
	public float Angle { get { return toggle.anchoredPosition.x > 0 ? -angle : angle; } }
	public float Power { get { return dist / distance; } }
	
	void Awake()
	{
		startPosition = toggle.anchoredPosition;
		angelTarget = new Vector2(startPosition.x, startPosition.y+distance);
	}
	
	void Update()
	{
		Move();
	
		angle = Vector2.Angle(angelTarget, toggle.anchoredPosition);
		dist = Vector2.Distance(startPosition, toggle.anchoredPosition);
	}
	
	public void OnPointerDown(PointerEventData e) 
	{
		StartMove();
	}
	
	public void OnPointerUp(PointerEventData e) 
	{
		EndMove();
	}
	
	void StartMove()
	{
	#if UNITY_ANDROID || UNITY_IOS
		index = Input.touchCount-1;
	#endif
		startTouch = TouchPosition();
		Moved = true;
	}
	void Move()
	{
		if(!Moved) return;
	
		toggle.anchoredPosition = Vector2.ClampMagnitude((startPosition + (TouchPosition() - startTouch) / Screen.height * 720f), distance);
	}
	void EndMove()
	{
		toggle.anchoredPosition = startPosition;
	#if UNITY_ANDROID || UNITY_IOS
		another.index = 0;
	#endif
		Moved = false;
	}
	Vector2 TouchPosition()
	{
	#if UNITY_ANDROID || UNITY_IOS
		return Input.GetTouch(index).position;
	#else
		return Input.mousePosition;
	#endif
	}

}}