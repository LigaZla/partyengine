﻿using UnityEngine;

namespace PartyEngine {
public static class ColorINT {

	const int RShift = 16; 
	const int GShift = 8;
	const int BShift = 0;
	const int AShift = 24;
	
	public static Color ConvertToColor(long value)
	{
		return (Color)ConvertToColor32(value);
	}
	public static Color32 ConvertToColor32(long value)
	{
		long newColor = value & 0xffffffff;
		return new Color32(
	        (byte)((newColor >> RShift) & 0xFF),
	        (byte)((newColor >> GShift) & 0xFF),
	       	(byte)((newColor >> BShift) & 0xFF),
			(byte)((newColor >> AShift) & 0xFF)
		);
	}
	
	public static int ConvertToInt32(Color color) 
	{
		return ConvertToInt32((Color32)color);
	}
	public static int ConvertToInt32(Color32 rgba) 
	{
		return unchecked((int) MakeLong(rgba.r, rgba.b, rgba.b, rgba.a));
	}
	public static int ConvertToInt32(byte r, byte g, byte b, byte a) 
	{
		return unchecked((int) MakeLong(r, g, b, a));
	}
	static long MakeLong(byte r, byte g, byte b, byte a)
	{
		uint uValue = unchecked((uint)(
			r << RShift | 
			g << GShift | 
			b << BShift | 
			a << AShift
		));
		return (long)(uValue & 0xffffffff);
	}

}}