﻿using UnityEngine;

namespace PartyEngine {
public class Speedometr 
{
	public float Speed { get { return speed * (1/Time.fixedDeltaTime);}}
	public float Distance { get { return distance;}}
		
	Transform movingObject;
	float speed;
	float distance;
	
	Vector3 lastPos;
	
	public Speedometr(Transform movingObject)
	{
		this.movingObject = movingObject;
		lastPos = movingObject.position;
		GlobalEvents.OnFixedUpdate += FixedUpdate;
	}
	
	void FixedUpdate()
	{
		speed = Vector3.Distance(lastPos, movingObject.position);
		distance += speed;
		
		lastPos = movingObject.position;
	}
	
	public void ResetDistance()
	{
		distance = 0;
	}
	
	public void Close()
	{
		GlobalEvents.OnFixedUpdate -= FixedUpdate;
	}
}}