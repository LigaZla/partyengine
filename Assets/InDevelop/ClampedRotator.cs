﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PartyEngine {
public class ClampedRotator {

	Transform target;
	bool clamped;
	Vector3 min;
	Vector3 max;
	Vector3 v;
	
	public ClampedRotator(Transform _target)
	{
		target = _target;
	}
	public ClampedRotator(Transform _target, Vector3 _min, Vector3 _max, bool _clamped):this(_target)
	{
		min = _min;
		max = _max;
		clamped = _clamped;
	}
	
	public void SetClamped(bool _clamped)
	{
		clamped = _clamped;
	}
	
	public void Rotate(Vector3 speed)
	{
		v += speed;
		
		if(clamped)
		{
			v.x = Mathf.Clamp(v.x, min.x, max.x);
			v.y = Mathf.Clamp(v.y, min.y, max.y);
			v.z = Mathf.Clamp(v.z, min.z, max.z);
		}
		
		target.localEulerAngles = v;
	}
}}