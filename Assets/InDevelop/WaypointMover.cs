﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PartyEngine {
public class WaypointMover {

	Transform transform;
	Vector3[] points;
	float speed;
	int currWaypoint = -1;
	
	Vector3 target;
	
	public WaypointMover(Transform transform, Vector3[] points, float speed)
	{
		this.transform = transform;
		this.points = points;
		this.speed = speed;
		NextWayPoint();
	}

	public void Update()
	{
		if(transform.position != target)
			transform.position = Vector3.MoveTowards(transform.position, target, speed*Time.deltaTime);
		else
			NextWayPoint();
	}
	
	void NextWayPoint()
	{
		currWaypoint++;
		if(currWaypoint >= points.Length)
			currWaypoint = 0;
		
		target = points[currWaypoint];
	}	
}}