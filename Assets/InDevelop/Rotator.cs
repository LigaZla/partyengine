﻿using UnityEngine;

namespace PartyEngine {
public class Rotator {

Transform transform;
float speed;
	
	public Rotator(Transform transform, float speed)
	{
		this.transform = transform;
		this.speed = speed;
	}
	public Rotator(float speed)
	{
		this.speed = speed;
	}
	
	public void SetRotation(Transform target)
	{
		SetRotation(target.rotation);
	}
	public void SetRotation(Vector3 target)
	{
		SetRotation(Quaternion.Euler(target));
	}
	public void SetRotation(Quaternion target)
	{
		transform.rotation = target;
	}
	
	public void SetLocalRotation(Transform target)
	{
		SetLocalRotation(target.localEulerAngles);
	}
	public void SetLocalRotation(Vector3 target)
	{
		transform.localEulerAngles = target;
	}
	public void SetLocalRotation(Quaternion target)
	{
		transform.localRotation = target;
	}
	
	
	public void RotateToLookAtTarget(Transform target)
	{
		RotateToLookAtTarget(target.position, speed);
	}
	public void RotateToLookAtTarget(Transform target, float speed)
	{
		RotateToLookAtTarget(target.position, speed);
	}
	
	public void RotateToLookAtTarget(Vector3 target)
	{
		RotateToLookAtTarget(target, speed);
	}
	public void RotateToLookAtTarget(Vector3 target, float speed)
	{
		RotateTowards(Quaternion.LookRotation(target - transform.position), speed);
	}
	
	public void RotateTowards(Vector3 target)
	{
		RotateTowards(Quaternion.Euler(target), speed);
	}
	public void RotateTowards(Vector3 target, float speed)
	{
		RotateTowards(Quaternion.Euler(target), speed);
	}
	public void RotateTowards(Quaternion target)
	{
		RotateTowards(target, speed);
	}
	public void RotateTowards(Quaternion target, float speed)
	{
		transform.rotation = Quaternion.RotateTowards(
			transform.rotation,
			target,
			speed*Time.deltaTime
		);
	}
	
	
	public bool LookedAt(Transform target)
	{
		return LookedAt(target.position);
	}
	public bool LookedAt(Vector3 target)
	{
		return transform.rotation == Quaternion.LookRotation(target - transform.position);
	}
	
	public bool RotationEquals(Quaternion target)
	{
		return transform.rotation == target;
	}
	public bool RotationEquals(Vector3 euler)
	{
		return RotationEquals(Quaternion.Euler(euler));
	}
	public bool RotationEquals(Transform target)
	{
		return transform.rotation == target.rotation;
	}
	
	public bool LocalRotationEquals(Quaternion target)
	{
		return transform.localRotation == target;
	}
	public bool LocalRotationEquals(Vector3 euler)
	{
		return transform.localEulerAngles == euler;
	}
	public bool LocalRotationEquals(Transform target)
	{
		return transform.localRotation == target.localRotation;
	}
	
	
	public static bool LookedAt(Transform current, Transform target)
	{
		return LookedAt(current, target.position);
	}
	public static bool LookedAt(Transform current, Vector3 target)
	{
		return current.rotation == Quaternion.LookRotation(target - current.position);
	}

}}