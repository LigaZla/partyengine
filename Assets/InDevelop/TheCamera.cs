﻿using UnityEngine;

namespace PartyEngine {

public class TheCamera : MonoBehaviour {
public static TheCamera gate;

[SerializeField] Camera _camera;
[SerializeField] float distance;

void Awake(){
	gate = this;
}

static Vector3 camData;
public Vector3 MouseToWorld(){
	camData.x = Input.mousePosition.x;
	camData.y = Input.mousePosition.y;
	camData.z = distance;
	return _camera.ScreenToWorldPoint(camData);
	//%width %height
}

}}