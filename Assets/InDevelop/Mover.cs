﻿using UnityEngine;

namespace PartyEngine {
public class Mover {
	
	Transform transform;
	Transform target;
	float speed;
	// TODO : move from trasform data and time
	public Mover(Transform transform)
	{
		this.transform = transform;
	}
	public Mover(float speed)
	{
		this.speed = speed;
	}
	public Mover(Transform transform, float speed):this(speed)
	{
		this.transform = transform;
	}
	public Mover(Transform transform, Transform target, float speed):this(transform, speed)
	{
		this.target = target;
	}
	
	public void MoveTo()
	{
		MoveTo(speed);
	}
	public void MoveTo(Transform target)
	{
		MoveTo(target.position);
	}
	public void MoveTo(Transform target, float speed)
	{
		MoveTo(target.position, speed);
	}
	public void MoveTo(Vector3 target)
	{
		MoveTo(target, speed);
	}
	public void MoveTo(float speed)
	{
		MoveTo(target.position, speed);
	}
	public void MoveTo(Vector3 target, float speed)
	{
		transform.position = Vector3.MoveTowards(transform.position, target, speed);
	}
	
	
	public bool InPosition()
	{
		return InPosition(target.position);
	}
	public bool InPosition(Transform target)
	{
		return InPosition(target.position);
	}
	public bool InPosition(Vector3 target)
	{
		return InPosition(transform.position, target);
	}
	
	public static bool InPosition(Transform current, Transform target)
	{
		return InPosition(current.position, target.position);
	}
	public static bool InPosition(Vector3 current, Vector3 target)
	{
		return current == target;
	}
	
	
	public bool InPosition(float dispersion)
	{
		return InPosition(target.position, dispersion);
	}
	public bool InPosition(Transform target, float dispersion)
	{
		return InPosition(target.position, dispersion);
	}
	public bool InPosition(Vector3 target, float dispersion)
	{
		return InPosition(transform.position, target, dispersion);
	}
	
	public static bool InPosition(Transform current, Transform target, float dispersion)
	{
		return InPosition(current.position, target.position, dispersion);
	}
	public static bool InPosition(Vector3 current, Vector3 target, float dispersion)
	{
		return Vector3.Distance(current, target) <= dispersion;
	}

	
	
}}