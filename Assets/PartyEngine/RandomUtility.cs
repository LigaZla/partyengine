﻿using UnityEngine;

namespace PartyEngine {
public static class RandomUtility
{
	/// <summary>Example: if percent = 0.25f, 25% chance to call 'a' action and 75% chance to call 'b' action</summary>
	/// <param name="percent">Percent between 0 and 1</param>
	public static void RandomAction(float percent, System.Action a, System.Action b)
	{
		if(GetRandomBool(percent)) a.Invoke();
		else b.Invoke();
	}
	
	/// <summary>Return value between 0 and 359</summary>
	public static int GetRandomAngle()
	{
		return Random.Range(0, 360);
	}
	
	/// <summary>Return 1 or -1</summary>
	public static int GetRandomOne()
	{
		return GetRandomBool() ? 1 : -1;
	}
		
	/// <summary>In currcent chance return true</summary>
	public static bool GetRandomBool(float percent = 0.5f)
	{
		return Random.Range(0, 1f) <= percent;
	}
	
	/// <summary>Example: if percent = 0.25f, 25% chance to get 'a' elemnt and 75% chance to get 'b' element</summary>
	/// <param name="percent">Percent between 0 and 1</param>
	public static T GetRandom<T>(float percent, T a, T b)
	{
		return GetRandomBool(percent) ? a : b;
	}
}}