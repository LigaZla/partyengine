﻿using System;
using PartyEngine;
using PartyEngine.Ext;

namespace PartyEngine.Actions {
public class EveryFrameAction 
{
	public bool IsDone { get; private set;}
	
	Action<float> action;
	System.Func<bool> endAction;
	Action whenDoneAction;
	float time = float.MaxValue;
	TimeThread layer;
		
	public EveryFrameAction(Action<float> action, System.Func<bool> endAction, Action whenDone, float time, TimeThread layer)
	{
		this.action = action;
		this.endAction = endAction ?? WhenTimeEnd;
		this.whenDoneAction = whenDone;
		this.time = time;
		this.layer = layer;
		
		IsDone = false;
		
		layer.Subscribe(Update);
	}
	public EveryFrameAction(Action<float> action, TimeThread layer)
	{
		this.action = action;
		this.layer = layer;
		
		IsDone = false;
		
		layer.Subscribe(UpdateUnlimited);
	}
	
	bool WhenTimeEnd()
	{
		return time <= 0;
	}
	
	void Update()
	{
		action.Invoke(layer.DeltaTime);
		time -= layer.DeltaTime;
		
		if(endAction.Invoke())
		{
			whenDoneAction.SafetyInvoke();
			Close();
		}
	}
	void UpdateUnlimited()
	{
		action.Invoke(layer.DeltaTime);
	}
	
	public void Close()
	{
		layer.Unsubscribe(Update);
		layer.Unsubscribe(UpdateUnlimited);

		action = null;
		endAction = null;
		whenDoneAction = null;
		IsDone = true;
	}
	
	public static EveryFrameAction StartActionUnlimited(Action<float> action, TimeThread layer)
	{
		return AddAction(new EveryFrameAction(action, layer));
	}
	
	public static EveryFrameAction StartAction(Action<float> action, System.Func<bool> endAction, Action whenDone, TimeThread layer)
	{
		return AddAction(new EveryFrameAction(action, endAction, whenDone, 0, layer));
	}
	
	public static EveryFrameAction StartAction(Action<float> action, float time, Action whenDone, TimeThread layer)
	{
		return AddAction(new EveryFrameAction(action, null, whenDone, time, layer));
	}

	const int ARRAY_SIZE = 64;
	readonly static EveryFrameAction[] actions = new EveryFrameAction[ARRAY_SIZE];
	static int currActionIndex = -1;
	static EveryFrameAction AddAction(EveryFrameAction action)
	{
		for (int i = 0; i < ARRAY_SIZE; i++)
		{
			if(++currActionIndex >= actions.Length) 
				currActionIndex = 0;
			
			if(actions[currActionIndex] == null || actions[currActionIndex].IsDone)
			{
				actions[currActionIndex] = action;
				goto END;
			}
		}
		
		#if UNITY_EDITOR || DEVELOPMENT_BUILD
		Debugger.LogError("EveryFrameAction array need more size");
		#endif
		
		actions[currActionIndex] = action;
		
		END:
		return action;
	}
}}