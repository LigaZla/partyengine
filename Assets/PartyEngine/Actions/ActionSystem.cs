﻿using PartyEngine;

namespace PartyEngine.Actions {
public static class ActionSystem
{
	#region EXT
	public static bool IsNullOrDone(this BaseAction action)
	{
		return action == null || action.IsDone;
	}
	
	public static void SafetyTerminate(this BaseAction action)
	{
		if(!action.IsNullOrDone())
			action.Terminate();
	}
	#endregion
	
	#region CLASSES
	public abstract class BaseAction
	{
		protected CallTimer timer;
		protected System.Action action;
		
		public abstract bool IsDone {get; }
		public float Time { get { return timer!=null ? timer.Time : 1;} }

		#if UNITY_EDITOR
		protected string id;
		#endif
		public virtual void Terminate()
		{
			if(timer != null) timer.Close();
			#if UNITY_EDITOR
			else 
			{
				Debugger.LogError("Action was already terminated! Id ='"+id+"'");
			}
			#endif
			timer = null;
			action = null;
		}
		
		public void InvokeForcibly()
		{
			action.Invoke();
		}
		
		public bool EqualsAction(System.Action action)
		{
			return this.action == action;
		}
	}
	
	public class DelayedAction : BaseAction
	{
		int lastCalls;
		System.Action whenDone;
		public override bool IsDone { get { return lastCalls < 1;}}
		
		public DelayedAction(System.Action action, float delay, int calls, TimeThread layer, System.Action whenDone = null)
		{
			this.action = action;
			this.lastCalls = calls;
			this.whenDone = whenDone;
			
			timer = CallTimer.CreateFromDelay(delay);
			timer.Initialize(DoAction, layer, false, true);
			#if UNITY_EDITOR
			id = action.Method.ToString();
			#endif
		}
		void DoAction()
		{
			action.Invoke();
			lastCalls--;
			
			if(IsDone)
			{
				if(whenDone!=null) whenDone.Invoke();
				Terminate();
			}
		}
		
		public override void Terminate()
		{
			base.Terminate();
			lastCalls = 0;
		}
	}
	
	public class DelayedActionUnlimited : BaseAction
	{
		public override bool IsDone { get { return isDone;}}
		bool isDone;
		public DelayedActionUnlimited(System.Action action, float delay, TimeThread layer)
		{
			this.action = action;
			
			timer = CallTimer.CreateFromDelay(delay);
			timer.Initialize(DoAction, layer, false, true);
			#if UNITY_EDITOR
			id = action.Method.ToString();
			#endif
		}

		void DoAction()
		{
			action.Invoke();
		}
		
		public override void Terminate()
		{
			base.Terminate();
			isDone = true;
		}
	}
	#endregion
	
	/// <summary>Called action every "delay" seconds.</summary> 
	public static DelayedActionUnlimited InvokeDelayedUnlimited(System.Action action, float delay)
	{
		return AddAction(new DelayedActionUnlimited(action, delay, TimeThread.Main));
	}
	/// <summary>Called action every "delay" seconds.</summary> 
	public static DelayedActionUnlimited InvokeDelayedUnlimited(System.Action action, float delay, TimeThread layer)
	{
		return AddAction(new DelayedActionUnlimited(action, delay, layer));
	}	
	
	/// <summary>Called action with delay</summary> 
	public static DelayedAction InvokeDelayed(System.Action action, float delay)
	{
		return InvokeSeveralDelayed(action, delay, 1, TimeThread.Main);
	}
	/// <summary>Called action with delay</summary> 
	public static DelayedAction InvokeDelayed(System.Action action, float delay, TimeThread layer)
	{
		return InvokeSeveralDelayed(action, delay, 1, layer);
	}
	/// <summary>Called action several times with delay.</summary> 
	public static DelayedAction InvokeSeveralDelayed(System.Action action, float delay, int calls, System.Action whenDone = null)
	{
		return InvokeSeveralDelayed(action, delay, calls, TimeThread.Main, whenDone);
	}
	/// <summary>Called action several times with delay.</summary> 
	public static DelayedAction InvokeSeveralDelayed(System.Action action, float delay, int calls, TimeThread layer, System.Action whenDone = null)
	{
		return AddAction(new DelayedAction(action, delay, calls, layer, whenDone));
	}
	
	const int ARRAY_SIZE = 512;
	readonly static BaseAction[] actions = new BaseAction[ARRAY_SIZE];
	static int currActionIndex = -1;
	static T AddAction<T>(T action) where T : BaseAction
	{	
		for (int i = 0; i < ARRAY_SIZE; i++)
		{
			if(++currActionIndex >= actions.Length) 
				currActionIndex = 0;
			
			if(actions[currActionIndex] is DelayedActionUnlimited) 
				continue;
			else if(actions[currActionIndex].IsNullOrDone())
				goto RETURN;
		}
		
		#if UNITY_EDITOR || DEVELOPMENT_BUILD
		Debugger.LogError("Action array need more size. Index = "+currActionIndex);
		#endif
		
		RETURN:
		actions[currActionIndex] = action;
		
		return action;
	}
	
	#region TERMINATE
	public static void TerminateAction(System.Action action)
	{
		for (int i = 0; i < actions.Length; i++)
		{
			if(actions[i] != null && actions[i].EqualsAction(action))
			{
				actions[i].Terminate();
				actions[i] = null;
				return;
			}
		}
	}
	
	public static void TerminateAll()
	{
		for (int i = 0; i < actions.Length; i++)
		{
			if(!actions[i].IsNullOrDone())
			{
				actions[i].Terminate();
				actions[i] = null;
			}
		}
	}
	#endregion
}}