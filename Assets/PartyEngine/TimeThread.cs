﻿using UnityEngine;
using PartyEngine.Types;

namespace PartyEngine
{
	public class TimeThread
	{
		#region Default time threads
		/// <summary>Default thread. Dont modify him!</summary>
		public static readonly TimeThread Default = new TimeThread(null, false);
		public static readonly TimeThread Main = new TimeThread(Default);
		public static readonly TimeThread Input = new TimeThread(Default);
		#endregion

		readonly CustomEvent<bool> OnPause;
		readonly CustomEvent OnUpdate;
		readonly CustomEvent OnFixedUpdate;
		readonly CustomEvent<float> OnUpdateArg;
		readonly CustomEvent<float> OnFixedUpdateArg;

		public float DeltaTime { get { return Time.deltaTime * speed * parent.speed; } }
		public float FixedDeltaTime { get { return Time.fixedDeltaTime * speed * parent.speed; } }

		public float Speed { get { return speed; } }
		public bool IsPaused { get { return isPaused; } }
		public TimeThread Parent { get { return parent; } }

		TimeThread[] childs = new TimeThread[0];
		TimeThread parent;
		float speed;
		bool isPaused;

		public TimeThread(TimeThread parent, bool needInit = true)
		{
			this.speed = 1;

			if (parent != null)
				SetParent(parent);

			if (!needInit) return;

			OnPause = new CustomEvent<bool>();
			OnUpdate = new CustomEvent();
			OnFixedUpdate = new CustomEvent();
			OnUpdateArg = new CustomEvent<float>();
			OnFixedUpdateArg = new CustomEvent<float>();

			GlobalEvents.OnUpdate += Update;
			GlobalEvents.OnFixedUpdate += FixedUpdate;
		}

		void SetParent(TimeThread parent)
		{
			this.parent = parent;
			this.parent.childs = this.parent.childs.AddItem(this);
		}

		public void SetPause(bool paused)
		{
			if (isPaused == paused) return;

			isPaused = paused;

			OnPause.Invoke(paused);

			childs.DoAction(x => x.SetPause(paused));
		}

		public void SetSpeed(float value)
		{
			speed = Mathf.Max(value, 0);
		}

		void Update()
		{
			if (!isPaused)
			{
				OnUpdate.Invoke();
				OnUpdateArg.Invoke(DeltaTime);
			}
		}

		void FixedUpdate()
		{
			if (!isPaused)
			{
				OnFixedUpdate.Invoke();
				OnFixedUpdateArg.Invoke(FixedDeltaTime);
			}
		}

		#region SUBSCRIBE
		public void Subscribe(System.Action action) { OnUpdate.Add(action); }
		public void Subscribe(System.Action<float> action) { OnUpdateArg.Add(action); }

		public void SubscribeFU(System.Action action) { OnFixedUpdate.Add(action); }
		public void SubscribeFU(System.Action<float> action) { OnFixedUpdateArg.Add(action); }

		public void SubscribePause(System.Action<bool> action) { OnPause.Add(action); }
		#endregion

		#region UNSUBSCRIBE
		public void Unsubscribe(System.Action action) { OnUpdate.RemoveFromEnd(action); }
		public void Unsubscribe(System.Action<float> action) { OnUpdateArg.RemoveFromEnd(action); }

		public void UnsubscribeFU(System.Action action) { OnFixedUpdate.RemoveFromEnd(action); }
		public void UnsubscribeFU(System.Action<float> action) { OnFixedUpdateArg.RemoveFromEnd(action); }

		public void UnsubscribePause(System.Action<bool> action) { OnPause.RemoveFromEnd(action); }
		#endregion

		/// <summary> Unsubscribe all events</summary>
		public void ClearAllSubscribes()
		{
			ClearSubscribes();
			ClearSubscribesFU();
			ClearSubscribesPause();
		}

		/// <summary> Unsubscribe all events OnUpdate</summary>
		public void ClearSubscribes() { OnUpdate.RemoveAll(); OnUpdateArg.RemoveAll(); }
		/// <summary> Unsubscribe all events OnFixedUpdate</summary>
		public void ClearSubscribesFU() { OnFixedUpdate.RemoveAll(); OnUpdateArg.RemoveAll(); }
		/// <summary> Unsubscribe all events OnPause</summary>
		public void ClearSubscribesPause() { OnPause.RemoveAll(); }
	}
}