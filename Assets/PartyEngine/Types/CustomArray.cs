﻿using System;

namespace PartyEngine.Types {
#if UNITY_EDITOR
[System.Serializable]
#endif
public class CustomArray<T> where T : class
{
	#if UNITY_EDITOR
	[UnityEngine.SerializeField] 
	#endif
	protected T[] array;
	
	#if UNITY_EDITOR
	[UnityEngine.SerializeField] 
	#endif
	protected int size;
	
	public int Count { get { return size;}}
	
	public CustomArray():this(4)
	{
		
	}
	public CustomArray(int size)
	{
		array = new T[size];
		this.size = 0;
	}
	public CustomArray(params T[] items)
	{
		this.array = Array.Combine(items, new T[items.Length]);
		size = this.array.Length;
	}
	
	public T this[int index]
	{
		get { return array[index]; }
		set { array[index] = value; }
	}
	
	public void Add(T item)
	{
		array[size++] = item;		
		
		if(size >= array.Length)
			array = array.Expand(array.Length);
	}

	public void Remove(T item)
	{
		if(item == null) return;
		
		for (int i = 0; i < size; i++)
		{
			#if UNITY_EDITOR
			try{
			#endif
				if(array[i].Equals(item))
				{
					if(i+1 < size)
						array = array.ShiftElements(i, i+1, size);
					
					size--;
					array[size] = null;

					return;
				}
			#if UNITY_EDITOR
			}
			catch(System.Exception ex){
				Debugger.LogError(i+"__"+size+"__"+array.Length+"\\n"+ex.Message+"\\n"+ex.StackTrace);
				continue;
			}
			#endif
		}
	}
	public void RemoveFromEnd(T item)
	{
		if(item == null) return;
		
		for (int i = size-1; i >= 0; i--)
		{
			#if UNITY_EDITOR
			try{
			#endif
				if(array[i].Equals(item))
				{
					if(i+1 < size)
						array = array.ShiftElements(i, i+1, size);
					
					size--;
					array[size] = null;

					return;
				}
			#if UNITY_EDITOR
			}
			catch(System.Exception ex){
				Debugger.LogError(i+"__"+size+"__"+array.Length+"\\n"+ex.Message+"\\n"+ex.StackTrace);
				return;
			}
			#endif
		}
	}
	
	public void RemoveAll()
	{
		for (int i = 0; i < array.Length; i++) 
			array[i] = null;
		
		size = 0;
	}
	
	public void Dispose()
	{
		for (int i = 0; i < array.Length; i++)
			array[i] = null;
		
		array = null;
	}
	
	#region CRUTCH ZONE
	/// <summary>If you need use this - you doing something wrong D:</summary>
	public void ClearNullItems()
	{
		int insertIndex = -1;
		int startIndex = -1;
		for (int i = 0; i < size; i++)
		{
			if(insertIndex < 0 && array[i] == null) insertIndex = i;
			if(insertIndex >= 0 && array[i] != null)
			{
				startIndex = i;
				break;
			}
		}
		
		if(insertIndex == 0 && startIndex < 0) // if All items null
		{
			size = 0;
		}
		else if(insertIndex >= 0 && startIndex < 0) // if end items block null's
		{
			size -= size - insertIndex;
		}
		else if(insertIndex >= 0 && startIndex >= 0)
		{
			array = array.ShiftElements(insertIndex, startIndex, size);
			size -= startIndex - insertIndex;
			ClearNullItems();
		}
	}
	#endregion
}}