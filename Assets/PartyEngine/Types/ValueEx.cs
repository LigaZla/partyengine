﻿using System;
using PartyEngine.Ext;

namespace PartyEngine.Types {
[System.Serializable]
public class ValueEx<T> 
{
	T value;
	public T Value 
	{ 
		get { return value;}		
		set
		{
			T oldValue = this.value;
			this.value = value;
			
			if(!oldValue.Equals(value))
			{
				OnChange.SafetyInvoke();
				OnChangeOf.SafetyInvoke(this.value);
			}
			
			OnSet.SafetyInvoke();
			OnSetOf.SafetyInvoke(this.value);
		}
	}
	
	public event Action OnChange;
	public event Action OnSet;
	public event Action<T> OnChangeOf;
	public event Action<T> OnSetOf;
	
	public ValueEx()
	{
		this.value = default(T);
	}
	
	public ValueEx(T value)
	{
		this.value = value;
	}
	
	public void SetOnChangeEvent(Action action)
	{
		OnChange = null;
		OnChange += action;
	}
	public void SetOnChangeEvent(Action<T> action)
	{
		OnChangeOf = null;
		OnChangeOf += action;
	}
	public void SetOnSetEvent(Action action)
	{
		OnSet = null;
		OnSet += action;
	}
	public void SetOnSetEvent(Action<T> action)
	{
		OnSetOf = null;
		OnSetOf += action;
	}
	
	public void ClearOnChangeEvents()
	{
		OnChange = null;
		OnChangeOf = null;
	}
	public void ClearOnSetEvents()
	{
		OnSet = null;
		OnSetOf = null;
	}
	
	public void ClearAllEvents()
	{
		ClearOnChangeEvents();
		ClearOnSetEvents();
	}
	
	public void Clear()
	{
		ClearAllEvents();
		value = default(T);
	}
	
}}