﻿
namespace PartyEngine.Types {
public struct CurrentTarget<T>
{
	public T Current;
	public T Target;
	
	public CurrentTarget(T current, T target)
	{
		this.Current = current;
		this.Target = target;
	}

	public static bool operator==(CurrentTarget<T> v1, CurrentTarget<T> v2)
	{
		return v1.Equals(v2);
	}
	public static bool operator!=(CurrentTarget<T> v1, CurrentTarget<T> v2)
	{
		return !v1.Equals(v2);
	}
	
	public override bool Equals(object obj)
	{
		if (obj is CurrentTarget<T>)
			return Equals((CurrentTarget<T>)obj);
		else
			return false;
	}
	public bool Equals(CurrentTarget<T> v2)
	{
		return Current.Equals(v2.Current) && Target.Equals(v2.Target);
	}
	public bool Equals(T current, T target)
	{
		return Current.Equals(current) && Target.Equals(target);
	}
	
	public override int GetHashCode()
	{
		return 0.GetHashCode();
	}
	
}}