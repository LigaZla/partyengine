﻿using System;
using UnityEngine;

namespace PartyEngine.Types 
{
	[System.Serializable]
	public class MinMaxValue<T> 
	{
		[SerializeField] T min;
		[SerializeField] T max;
		protected T current;

		public T Min => min;
		public T Max => max;
		public virtual T Current { get; set; }

		public MinMaxValue()
		{
			this.max = default(T);
			this.current = default(T);
			this.min = default(T);
		}
		
		public MinMaxValue(T max)
		{
			this.max = max;
			this.current = default(T);
			this.min = default(T);
		}
		
		public MinMaxValue(T max, T min)
		{
			this.max = max;
			this.min = min;
			this.current = default(T);
		}
		
		public MinMaxValue(T max, T min, T value)
		{
			this.max = max;
			this.current = value;
			this.min = min;
		}
		
		public void SetMax(T max)
		{
			this.max = max;
		}
		
		public void SetMin(T min)
		{
			this.min = min;
		}

		public void Maximize()
		{
			this.current = this.max;
		}

		public void Minimize()
		{
			this.current = this.min;
		}

		public void Clear()
		{
			max = default(T);
			current = default(T);
			min = default(T);
		}
	}
	
	[System.Serializable]
	public class MaxMinValueInt : MinMaxValue<int> // for show in inspector
	{
		public override int Current
		{
			get { return current; }
			set
			{
				if(value > Max) current = Max;
				else if(value < Min) current = Min;
				else current = value;
			}
		}
	}
	
	[System.Serializable]
	public class MaxMinValueFloat : MinMaxValue<float> // for show in inspector
	{
		public override float Current
		{
			get { return current; }
			set
			{
				if (value > Max) current = Max;
				else if (value < Min) current = Min;
				else current = value;
			}
		}
	}
}