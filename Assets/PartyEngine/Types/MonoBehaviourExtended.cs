﻿using UnityEngine;
using PartyEngine.Ext;

namespace PartyEngine.Types {
public class MonoBehaviourExtended : MonoBehaviour
{
	public event System.Action<MonoBehaviourExtended, bool> OnSetActive;
	public event System.Action<GameObject, bool> OnSetActiveGO;
	
	public event System.Action<MonoBehaviourExtended> OnDestroyClass;
	public event System.Action<GameObject> OnDestroyGO;

	protected virtual void OnEnable()
	{
		OnSetActive.SafetyInvoke(this, enabled);
	}
	protected virtual void OnDisble()
	{
		OnSetActive.SafetyInvoke(this, enabled);
	}
	
	public virtual void SetActiveGO(bool active)
	{
		gameObject.SetActive(active);
		OnSetActiveGO.SafetyInvoke(gameObject, active);
	}
	public virtual void SetActive(bool active)
	{
		enabled = active;
		OnSetActive.SafetyInvoke(this, enabled);
	}
	
	protected virtual void OnDestroy()
	{
		OnDestroyGO.SafetyInvoke(gameObject);
	}
	public void DestroyClass()
	{
		OnDestroyClass.SafetyInvoke(this);
		Destroy(this);
	}
}}