﻿using System;

namespace PartyEngine.Types
{
	public class CustomEvent : CustomArray<Action>
	{
		public void Invoke()
		{
			for (int i = 0; i < size; i++)
			{
				array[i].Invoke();
			}
		}
	}
	
	public class CustomEvent<T> : CustomArray<Action<T>>
	{
		public void Invoke(T arg)
		{
			for (int i = 0; i < size; i++)
			{
				array[i].Invoke(arg);
			}
		}
	}
	
	public class CustomEvent<T, T1> : CustomArray<Action<T, T1>>
	{
		public void Invoke(T arg, T1 arg1)
		{
			for (int i = 0; i < size; i++)
			{
				array[i].Invoke(arg, arg1);
			}
		}
	}
	
	public class CustomEvent<T, T1, T2> : CustomArray<Action<T, T1, T2>>
	{
		public void Invoke(T arg, T1 arg1, T2 arg2)
		{
			for (int i = 0; i < size; i++)
			{
				array[i].Invoke(arg, arg1, arg2);
			}
		}
	}	
}