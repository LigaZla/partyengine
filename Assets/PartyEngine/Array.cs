﻿using UnityEngine;
using System.Collections.Generic;

namespace PartyEngine {
public static class Array {	
		
#region System

	public static bool IsNullOrEmpty<T>(this T[] array)
	{
		return array == null || array.Length < 1;
	}
	public static bool IsNullOrEmpty<T>(this IList<T> array)
	{
		return array == null || array.Count < 1;
	}
	
	/// <summary>Clamp index between 0 and array.Length-1</summary>
	public static int ClampIndex<T>(this T[] array, int index)
	{
		return Mathf.Clamp(index, 0, array.Length-1);
	}
	/// <summary>Clamp index between 0 and array.Length-1</summary>
	public static int ClampIndex<T>(this IList<T> array, int index)
	{
		return Mathf.Clamp(index, 0, array.Count-1);
	}
	
	public static bool IndexInRange<T>(this T[] array, int index)
	{
		return index >= 0 && index < array.Length;
	}
	public static bool IndexInRange<T>(this IList<T> array, int index)
	{
		return index >= 0 && index < array.Count;
	}

	public static T[] Combine<T>(params T[][] arrays)
	{
		int newCount = 0;
		foreach(T[] array in arrays)
			newCount += array.Length;
		
		T[] newArray = new T[newCount];
		
		int i = 0;
		foreach(T[] array in arrays)
			foreach(T element in array)
				newArray[i++] = element;

		return newArray;
	}
	public static List<T> Combine<T>(params List<T>[] arrays)
	{
		List<T> newList = new List<T>();
		
		foreach(var array in arrays)
			newList.AddRange(array);

		return newList;
	}
	
	public static TO[] Convert<FROM, TO>(this FROM[] array, System.Func<FROM, TO> convertPattern)
	{
		TO[] newArray = new TO[array.Length];
		for (int i = 0; i < array.Length; i++)
		{
			newArray[i] = convertPattern.Invoke(array[i]);
		}
		
		return newArray;
	}
	public static List<TO> Convert<FROM, TO>(this List<FROM> array, System.Func<FROM, TO> convertPattern)
	{
		List<TO> newArray = new List<TO>(array.Count);
		for (int i = 0; i < array.Count; i++)
		{
			newArray.Add(convertPattern.Invoke(array[i]));
		}
		
		return newArray;
	}
	
	public static T[] ToEmptyIfNull<T>(this T[] array)
	{
		array = array ?? new T[0];
		return array;
	}
	public static List<T> ToEmptyIfNull<T>(this List<T> array)
	{
		array = array ?? new List<T>();
		return array;
	}
	
	public static T[] Expand<T>(this T[] array, int increaseAmount)
	{
		return array.Resize(array.Length + increaseAmount);
	}
	
	public static T[] Resize<T>(this T[] array, int newSize)
	{
		T[] newArray = new T[newSize];
		for(int i = 0; i < newArray.Length; i++)
			if(i < array.Length)
				newArray[i] = array[i];
		
		return newArray;
	}

	public static T[] AddItem<T>(this T[] array, T element)
	{
		T[] arr = new []{element};
		array = Combine(array, arr);
		return array;
	}
	
	public static C[] Fill<C, T>(this C[] current, T[] target, System.Func<T, C> extractPattern)
	{
		current = new C[target.Length];
		for(int i=0; i<current.Length; i++)
			current[i] = extractPattern.Invoke(target[i]);
		
		return current;
	}
	
	public static List<C> Fill<C, T>(this List<C> current, List<T> target, System.Func<T, C> extractPattern)
	{
		current = new List<C>();
		foreach(var targ in target)
			current.Add(extractPattern.Invoke(targ));
		
		return current;
	}
	
	public static T[] ShiftElementsSafety<T>(this T[] array, int insertIndex, int startIndex, int endIndex)
	{
		if(insertIndex < 0) insertIndex = 0;
		if(startIndex < 0) startIndex = 0;
		if(endIndex >= array.Length) endIndex = array.Length-1;
		if(insertIndex > startIndex) insertIndex = startIndex;
		
		return array.ShiftElements(insertIndex, startIndex, endIndex);
	}
	/// <param name="array">array</param>
	/// <param name="insertIndex">The index at which the shifting begins</param>
	/// <param name="startIndex">start index of block of elements which need shift</param>
	/// <param name="endIndex">end index of block of elements which need shift</param>
	/// <returns></returns>
	public static T[] ShiftElements<T>(this T[] array, int insertIndex, int startIndex, int endIndex)
	{
		for (int i = startIndex, j=0; i <= endIndex; i++, j++)
		{
			array[insertIndex+j] = array[i];
		}
		
		return array;
	}
#endregion

#region Random

	public static int[] GetLinearNumbers(int min, int max)
	{
		int[] array = new int[max-min];
		for(int i=0, j=min; i<array.Length; i++, j++)
			array[i] = j;
		
		return array;
	}
	
	public static int[] GetRandomNumbers(int sizeArray, int minValue, int maxValue)
	{
		int[] array = new int[sizeArray];
		for(int i=0; i<array.Length; i++)
			array[i] = Random.Range(minValue, maxValue);
		
		return array;
	}

	public static T[] SortRandom<T>(this T[] array)
	{
		return array.SortRandom(false, 0);
	}
	public static T[] SortRandom<T>(this T[] array, int seed)
	{
		return array.SortRandom(true, seed);
	}
	static T[] SortRandom<T>(this T[] array, bool seedable, int seed)
	{
		System.Random rand = seedable ? new System.Random(seed) : new System.Random();
	    T temp;
		for(int currElem=array.Length-1, replacedElem=0; currElem>0; currElem--) 
		{
			replacedElem = rand.Next(0, currElem+1);
			temp = array[currElem];
			array[currElem] = array[replacedElem];
			array[replacedElem] = temp;
	    }
		
		return array;
	}
	public static IList<T> SortRandom<T>(this IList<T> array)
	{
		return array.SortRandom();
	}
	public static IList<T> SortRandom<T>(this IList<T> array, int seed)
	{
		return array.SortRandom(true, seed);
	}
	static IList<T> SortRandom<T>(this IList<T> array, bool seedable, int seed)
	{
		System.Random rand = seedable ? new System.Random(seed) : new System.Random();
	    T temp;
		for(int currElem=array.Count-1, replacedElem=0; currElem>0; currElem--) 
		{
	        replacedElem = rand.Next(0, currElem+1);
	        temp = array[currElem];
	        array[currElem] = array[replacedElem];
	        array[replacedElem] = temp;
	    }
		
		return array;
	}	
			
	public static T GetRandomItem<T>(this T[] array)
	{
		return !IsNullOrEmpty(array) ? array[Random.Range(0, array.Length)] : default(T);
	}
	public static T GetRandomItem<T>(this IList<T> array)
	{
		return !IsNullOrEmpty(array) ? array[Random.Range(0, array.Count)] : default(T);
	}
	#endregion
	
#region SORT
	/// <summary>Buble sort array with specified condition</summary>
	/// <param name="array">Array to sort</param>
	/// <param name="compareCondition">Example: "(x,y)=>x>y" soreted min to max</param>
	/// <returns>Sorted array</returns>
	public static T[] BubbleSort<T>(this T[] array, System.Func<T, T, bool> compareCondition)
	{
		T temp;
		for(int i=0; i < array.Length; i++)
		{
			for(int j=0; j<array.Length-1; j++)
			{
				if(compareCondition(array[j], array[j+1]))
				{
					temp = array[j];
					array[j] = array[j+1];
					array[j+1] = temp;
				}
			}
		}
		
		return array;
	}
	/// <summary>Buble sort array with specified condition</summary>
	/// <param name="array">Array to sort</param>
	/// <param name="compareCondition">Example: "(x,y)=>x>y" soreted min to max</param>
	/// <returns>Sorted array</returns>
	public static List<T> BubbleSort<T>(this List<T> array, System.Func<T, T, bool> compareCondition)
	{
		T temp;
		for(int i=0; i < array.Count; i++)
		{
			for(int j=0; j<array.Count-1; j++)
			{
				if(compareCondition(array[j], array[j+1]))
				{
					temp = array[j];
					array[j] = array[j+1];
					array[j+1] = temp;
				}
			}
		}
		
		return array;
	}
#endregion
	
#region GET
	/// <summary>Return the first founded element which true with compareCondition</summary>
	public static T GetItem<T>(this T[] array, System.Func<T, bool> compareCondition, T nullItem = default(T))
	{
		foreach(var element in array)
			if(compareCondition(element))
			   return element;
				
		return nullItem;
	}
	/// <summary>Return the first founded element which true with compareCondition</summary>
	public static T GetItem<T>(this IList<T> array, System.Func<T, bool> compareCondition, T nullItem = default(T))
	{
		foreach(var element in array)
			if(compareCondition(element))
			   return element;
				
		return nullItem;
	}
	
	public static T[] GetAllItems<T>(this T[] array, System.Func<T, bool> compareCondition)
	{
		List<T> list = new List<T>();
		array.DoAction(x=> { if(compareCondition.Invoke(x)) list.Add(x); });
		
		return list.ToArray();
	}
	public static T[] GetAllItems<T>(this IList<T> array, System.Func<T, bool> compareCondition)
	{
		List<T> list = new List<T>();
		array.DoAction(x=> { if(compareCondition.Invoke(x)) list.Add(x); });
		
		return list.ToArray();
	}
	
	/// <summary>Return element with index between 0 and array.Length-1 </summary>
	public static T SafetyGetElement<T>(this T[] array, int index)
	{
		index = array.ClampIndex(index);
		return array[index];
	}
	/// <summary>Return element with index between 0 and array.Length-1 </summary>
	public static T SafetyGetElement<T>(this IList<T> array, int index)
	{
		index = array.ClampIndex(index);
		return array[index];
	}
	
	public static T GetLastElement<T>(this T[] array)
	{
		return array[array.Length-1];
	}
	public static T GetLastElement<T>(this IList<T> array)
	{
		return array[array.Count-1];
	}
	
	/// <summary> Count, how much elements equals condition </summary>
	public static int GetCount<T>(this T[] array, System.Func<T, bool> compareCondition)
	{
		int count = 0;
		foreach (var element in array)
			if(compareCondition.Invoke(element))
				count++;
		
		return count;
	}
	/// <summary> Count, how much elements equals condition </summary>
	public static int GetCount<T>(this List<T> array, System.Func<T, bool> compareCondition)
	{
		int count = 0;
		foreach (var element in array)
			if(compareCondition.Invoke(element))
				count++;
		
		return count;
	}
	
	public static T[] Filter<T>(this T[] array, System.Func<T, bool> condition)
	{
		List<T> newArray = new List<T>();
		for (int i = 0; i < array.Length; i++)
			if(condition.Invoke(array[i]))
				newArray.Add(array[i]);
		
		return newArray.ToArray();
	}
	public static List<T> Filter<T>(this IList<T> array, System.Func<T, bool> condition)
	{
		List<T> newArray = new List<T>();
		for (int i = 0; i < array.Count; i++)
			if(condition.Invoke(array[i]))
				newArray.Add(array[i]);
		
		return newArray;
	}
#endregion

#region REMOVE
	public static T[] RemoveFirstElement<T>(this T[] array)
	{
		T[] newArray = new T[array.Length-1];
		for(int i = 0; i < newArray.Length; i++)
			if(i < array.Length)
				newArray[i] = array[i+1];
		
		return newArray;
	}
	public static void RemoveFirstElement<T>(this IList<T> array)
	{
		array.RemoveAt(0);
	}
	
	public static T[] RemoveLastElement<T>(this T[] array)
	{
		return array.Resize(array.Length-1);
	}
	public static void RemoveLastElement<T>(this IList<T> array)
	{
		array.RemoveAt(array.Count-1);
	}

	public static bool RemoveItem<T>(ref T[] array, System.Func<T, bool> compareCondition)
	{
		// TODO : REFACTOR
		bool itemFounded = false;
		
		List<T> newArray = new List<T>();
		newArray.AddRange(array);
		
		T item = newArray.GetItem(compareCondition);
		itemFounded = !item.Equals(default(T));
		if(itemFounded)
			newArray.Remove(item);
		
		array = newArray.ToArray();

		return itemFounded;
	}
	public static bool RemoveItem<T>(this IList<T> array, System.Func<T, bool> compareCondition)
	{
		foreach(var item in array)
		{
			if(compareCondition(item))
			{
				array.Remove(item);
				return true;
			}
		}
		
		return false;
	}
	
	public static T[] RemoveItems<T>(this T[] array, System.Func<T, bool> compareCondition)
	{
		List<T> newArray = new List<T>();
		foreach (var element in array)
			if(!compareCondition(element)) 
				newArray.Add(element);
		
		return newArray.ToArray();
	}
	public static List<T> RemoveItems<T>(this List<T> array, System.Func<T, bool> compareCondition)
	{
		for (int i = array.Count-1; i >= 0; i++)
		{
			if(compareCondition(array[i]))
			{
				array.RemoveAt(i);
			}
		}
		
		return array;
	}
#endregion

	public static bool HasElement<T>(this T[] array, System.Func<T, bool> compareCondition)
	{
		foreach(var item in array)
			if(compareCondition(item))
				return true;
		
		return false;
	}
	public static bool HasElement<T>(this IList<T> array, System.Func<T, bool> compareCondition)
	{
		foreach(var item in array)
			if(compareCondition(item))
				return true;
		
		return false;
	}

	/// <summary>Do action to every element in array. Start with first index</summary>
	public static void DoAction<T>(this T[] array, System.Action<T> action)
	{
		for(int i = 0; i < array.Length; i++)
			action.Invoke(array[i]);
	}
	/// <summary>Do action to every element in array. Start with first index</summary>
	public static void DoAction<T>(this IList<T> array, System.Action<T> action)
	{
		for(int i = 0; i < array.Count; i++)
			action.Invoke(array[i]);
	}
	
	/// <summary>Do action to every element in array with condition. Start with first index</summary>
	public static void DoAction<T>(this T[] array, System.Action<T> action, System.Func<T, bool> condition)
	{
		for(int i = 0; i < array.Length; i++)
			if(condition.Invoke(array[i]))
				action.Invoke(array[i]);
	}
	/// <summary>Do action to every element in array with condition. Start with first index</summary>
	public static void DoAction<T>(this IList<T> array, System.Action<T> action, System.Func<T, bool> condition)
	{
		for(int i = 0; i < array.Count; i++)
			if(condition.Invoke(array[i]))
				action.Invoke(array[i]);
	}
	
	/// <summary>Do action to every element in array. Start with last index</summary>
	public static void DoActionFromEnd<T>(this T[] array, System.Action<T> action)
	{
		for(int i = array.Length-1; i >= 0; i--)
			action.Invoke(array[i]);
	}
	/// <summary>Do action to every element in array. Start with last index</summary>
	public static void DoActionFromEnd<T>(this IList<T> array, System.Action<T> action)
	{
		for(int i = array.Count-1; i >= 0; i--)
			action.Invoke(array[i]);
	}
	
	/// <summary>Do action to every element in array with iteration index. Like as for. Start with first index</summary>
	public static void DoActionIterable<T>(this T[] array, System.Action<int, T> action)
	{
		for(int i = 0; i < array.Length; i++)
			action.Invoke(i, array[i]);
	}
	/// <summary>Do action to every element in array with iteration index. Like as for. Start with first index</summary>
	public static void DoActionIterable<T>(this IList<T> array, System.Action<int, T> action)
	{
		for(int i = 0; i < array.Count; i++)
			action.Invoke(i, array[i]);
	}
}}