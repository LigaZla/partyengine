﻿using UnityEngine;
using PartyEngine.Ext;

namespace PartyEngine {
public class Raycaster 
{
	Transform rayLine;
	LayerMask mask;
	float distance;
	RaycastHit hit;
	
	public Raycaster(Transform container, LayerMask mask, float distance)
	{
		this.distance = distance;
		this.mask = mask;
		rayLine = ObjectExt.CreateGameObject("Raycaster", container).transform;
	}
	
	public void SetPosition(Vector3 position)
	{
		rayLine.position = position;
	}
	
	public void SetRotation(Vector3 rotation)
	{
		rayLine.localEulerAngles = rotation;
	}
	
	public bool Touched()
	{
		return Touched(distance);
	}
	public bool Touched(float distance)
	{
		return Physics.Raycast(rayLine.position, rayLine.forward, out hit, distance, mask);
	}	
	
	public bool Touched(Vector3 target)
	{
		return Touched(target, distance);
	}
	public bool Touched(Vector3 target, float distance)
	{
		rayLine.LookAt(target);
		return Touched(distance);
	}
	
	public RaycastHit GetLastHit()
	{
		return hit;
	}
	
	public RaycastHit[] GetTouchedAll(float distance)
	{
		return Physics.RaycastAll(rayLine.position, rayLine.up, distance, mask);
	}	
	
	public void Close()
	{
		rayLine.Destroy();
	}
}}