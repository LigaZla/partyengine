﻿using UnityEngine;
using PartyEngine.Ext;

namespace PartyEngine {
public static class Debugger 
{
	static int meow;
	public static void MEOW()
	{
		LogWarning("MEOW = "+meow++);
	}
	
	public static void Log(object message, Object context = null)
	{
		Log(message.ToString(), context);
	}
	public static void Log(string message, Object context = null)
	{
		if(context != null)
			Debug.LogFormat(context, FormatMessage("Log.", message, context));
		else
			Debug.Log(FormatMessage("Log.", message, context));
	}
	public static void Log(this Object context, string message)
	{
		Debug.LogFormat(context, FormatMessage("Log.", message, context));
	}
	
	
	public static void LogWarning(object message, Object context = null)
	{
		LogWarning(message.ToString(), context);
	}
	public static void LogWarning(string message, Object context = null)
	{
		if(context != null) 
			Debug.LogWarningFormat(context, FormatMessage("WARNING!", message, context));
		else 
			Debug.LogWarning(FormatMessage("WARNING!", message, context));
	}
	public static void LogWarning(this Object context, string message)
	{
		Debug.LogWarningFormat(context, FormatMessage("WARNING!", message, context));
	}
	
	public static void LogError(object message, Object context = null)
	{
		LogError(message.ToString(), context);
	}
	public static void LogError(string message, Object context = null)
	{
		if(context != null) 
			Debug.LogErrorFormat(context, FormatMessage("ERROR!", message, context));
		else 
			Debug.LogError(FormatMessage("ERROR!", message, context));
	}
	public static void LogError(this Object context, string message)
	{
		Debug.LogErrorFormat(context, FormatMessage("ERROR!", message, context));
	}
	
	static string FormatMessage(string type, string message, Object context)
	{
		string prefix = type+" Frame: "+Time.frameCount+" Time: "+System.DateTime.Now.ToStringFullDateAndTime();
		
		if(context != null)
			return prefix+". "+context.name+" say: "+message;
		else
			return prefix+". Message: "+message;
	}
	
	public static void LogException(System.Exception ex)
	{
		Debug.LogException(ex);
	}
	
#region WATCH
	static System.Diagnostics.Stopwatch watch;
	public static void StartWatch()
	{
		if(watch == null)watch = new System.Diagnostics.Stopwatch();
		else watch.Reset();
		
		watch.Start();
	}
	
	public static void StopAndLogWatch()
	{
		watch.Stop();
		LogWatch();
	}
	public static void LogWatch()
	{
		Debugger.Log(watch.Elapsed);
	}
#endregion
}}