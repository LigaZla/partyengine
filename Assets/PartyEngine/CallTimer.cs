﻿using UnityEngine;
using PartyEngine.Ext;

namespace PartyEngine {
public class CallTimer 
{	
	const float MAX_TIME = 1f;
	const float SECONDS_IN_MINUTE = 60f;	
		
	public bool IsFull { get { return time >= MAX_TIME;}}
	public bool IsPaused { get { return paused;}}
	public float Time { get{ return time;}}
	public float RemainingSeconds { get{ return (MAX_TIME - time) / callPerSecond;}}
	
	TimeThread timeLayer;
	float callPerSecond;
	float time;
	bool paused;

	event System.Action onFull;
	
	/// <summary>Call every 'delay' seconds;</summary>
	public static CallTimer CreateFromDelay(float delay)
	{
		return new CallTimer(SECONDS_IN_MINUTE / delay);
	}
	public static CallTimer CreateFromSeconds(float callPerSecond)
	{
		return new CallTimer(SECONDS_IN_MINUTE * callPerSecond);
	}
	public CallTimer(float callPerMinute)
	{
		callPerSecond = callPerMinute / SECONDS_IN_MINUTE;
		time = 0;
		onFull = null;
		SetTimeLayer(null);
	}
	
	void SetTimeLayer(TimeThread timeLayer)
	{
		this.timeLayer = timeLayer ?? TimeThread.Main;
	}

	public CallTimer Initialize(System.Action action)
	{
		SetAction(action);
		
		return this;
	}
	public CallTimer Initialize(System.Action action, TimeThread timeLayer, bool isFull = false, bool registerToGlobalEvents = false)
	{
		time = isFull ? MAX_TIME : 0;
		SetTimeLayer(timeLayer);
		
		if(action != null) 
			SetAction(action);
		
		if(registerToGlobalEvents) 
			timeLayer.Subscribe(Update);
		
		return this;
	}
	
	public void SetAction(System.Action action)
	{
		onFull = action;
	}
	public void RegisterAction(System.Action action)
	{
		onFull += action;
	}
	public void UnregisterAction(System.Action action)
	{
		onFull -= action;
	}
	public void UnregisterAllActions()
	{
		onFull = null;
	}

	public void Update()
	{
		Increment();
		for(; IsFull && !paused; Decrement())
			onFull.SafetyInvoke();
	}
	
	public void Increment()
	{
		if(!paused && time < MAX_TIME) 
			time += callPerSecond * timeLayer.DeltaTime;
	}
	
	public void Decrement()
	{
		time--;
	}
	
	public void SetPause(bool pause)
	{
		paused = pause;
	}
	
	public void SetFull()
	{
		time = MAX_TIME;
	}
	public void SetZero()
	{
		time = 0;
	}
	public void SetSeconds(float seconds)
	{
		time = callPerSecond * seconds;
	}
	public void SetTime(float time)
	{
		this.time = time;
	}
	public void SetPercent(float percent)
	{
		time = MAX_TIME * Mathf.Clamp(percent, 0, 1f);
	}
	
	public void SetCallPerMinute(float callPerMinute)
	{
		callPerSecond = callPerMinute / SECONDS_IN_MINUTE;
	}
	
	/// <summary>Prepare to delete</summary>
	public void Close()
	{
		onFull = null;
		callPerSecond = 0;
		time = 0;
		paused = false;
		
		timeLayer.Unsubscribe(Update);
	}
}}