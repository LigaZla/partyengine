﻿using UnityEngine;
using System.Collections.Generic;

namespace PartyEngine.Ext {
public static class VectorExt 
{
	/// <summary>Create vector which properties == value </summary>
	public static Vector4 CreateVector(float value)
	{
		return new Vector4(value, value, value, value);
	}

	public static bool InSquare(this Vector4 point, Vector4 min, Vector4 max)
	{
		return InSquare((Vector2)point, (Vector2)min, (Vector2)max);
	}
	public static bool InSquare(this Vector3 point, Vector3 min, Vector3 max)
	{
		return InSquare((Vector2)point, (Vector2)min, (Vector2)max);
	}
	public static bool InSquare(this Vector2 point, Vector2 min, Vector2 max)
	{
		return point.x > min.x && point.x < max.x && point.y > min.y && point.y < max.y;
	}
	
	public static bool InCube(this Vector4 point, Vector4 min, Vector4 max)
	{
		return InCube((Vector3)point, (Vector3)min, (Vector3)max);
	}
	public static bool InCube(this Vector3 point, Vector3 min, Vector3 max)
	{
		return point.InSquare(min, max) && point.z > min.z && point.z < max.z;
	}
	
	public static bool InMatrix(this Vector4 point, Vector4 min, Vector4 max)
	{
		return point.InCube(min, max) && point.w > min.w && point.w < max.w;
	}
	
	public static bool InDistance(Vector4 from, Vector4 to, float distance)
	{
		return Vector4.Distance(from, to) < distance;
	}
	
	public static Vector2 GetPointToTargetLinear(Vector2 current, Vector2 target, float shift)
	{
		Vector2 dir = target - current;
		
		float dirX = Mathf.Abs(dir.x);
		float dirY = Mathf.Abs(dir.y);
		
		float xScale = dirX / dirY;
		float yScale = dirY / dirX;

		float x = (dirX < dirY ? xScale : 1) * shift * (dir.x >= 0 ? 1 : -1);
		float y = (dirX > dirY ? yScale : 1) * shift * (dir.y >= 0 ? 1 : -1);
		
		return new Vector2(x, y) + target;
	}
	
	public static Vector2 GetPointToTarget(Vector2 current, Vector2 target, float percent)
	{
		return current + ((target - current) * percent);
	}
	public static Vector3 GetPointToTarget(Vector3 current, Vector3 target, float percent)
	{
		return current + ((target - current) * percent);
	}
	
	
	public static Vector4 Clamp(this Vector4 value, Vector4 min, Vector4 max)
	{
		return 
			new Vector4
			(
				Mathf.Clamp(value.x, min.x, max.x),
				Mathf.Clamp(value.y, min.y, max.y),
				Mathf.Clamp(value.z, min.z, max.z),
				Mathf.Clamp(value.w, min.w, max.w)
			);
	}
	
	public static float GetMax(this Vector2 vector)
	{
		return vector.x > vector.y ? vector.x : vector.y;
	}
	public static float GetMax(this Vector3 vector)
	{
		float value = vector.x;
		if(value > vector.y) value = vector.y;
		if(value > vector.z) value = vector.z;
		
		return value;
	}
	public static float GetMax(this Vector4 vector)
	{
		float value = vector.x;
		if(value > vector.y) value = vector.y;
		if(value > vector.z) value = vector.z;
		if(value > vector.w) value = vector.w;
		
		return value;
	}
	
	public static float GetMin(this Vector4 vector)
	{
		float value = vector.x;
		if(value < vector.y) value = vector.y;
		if(value < vector.z) value = vector.z;
		if(value < vector.w) value = vector.w;
		
		return value;
	}
	public static float GetMin(this Vector3 vector)
	{
		float value = vector.x;
		if(value < vector.y) value = vector.y;
		if(value < vector.z) value = vector.z;
		
		return value;
	}
	public static float GetMin(this Vector2 vector)
	{
		return vector.x < vector.y ? vector.x : vector.y;
	}
	
	/// <summary>Return new vector with absolute values</summary>
	public static Vector4 Abs(this Vector4 vector)
	{
		return 
			new Vector4
			(
				System.Math.Abs(vector.x),
				System.Math.Abs(vector.y),
				System.Math.Abs(vector.z),
				System.Math.Abs(vector.w)
			);
	}
	
	/// <summary>Clamp vector between 0 and vector</summary>
	public static Vector4 Positive(this Vector4 vector)
	{
		return 
			new Vector4
			(
				Mathf.Max(0, vector.x),
				Mathf.Max(0, vector.y),
				Mathf.Max(0, vector.z),
				Mathf.Max(0, vector.w)
			);
	}
	
	/// <summary>Get max value and set him to all</summary>
	public static Vector4 Maximize(this Vector4 vector)
	{
		float value = GetMax(vector);
		float value2 = GetMin(vector);
		return CreateVector(value > Mathf.Abs(value2) ? value : value2);
	}
	
	public static Vector4 Random(Vector4 min, Vector4 max)
	{
		return 
			new Vector4
			(
				UnityEngine.Random.Range(min.x, max.x),
				UnityEngine.Random.Range(min.y, max.y),
				UnityEngine.Random.Range(min.z, max.z),
				UnityEngine.Random.Range(min.w, max.w)
			);
	}
	
	#region GET NEAREST
	public static T GetNearest<T>(this T[] array, Vector3 fromPos) where T : Component
	{
		T obj = null;
		float lastDist = float.MaxValue;
		foreach (var element in array)
		{
			float dist = Vector3.Distance(fromPos, element.transform.position);
			if(dist < lastDist)
			{
				lastDist = dist;
				obj = element;
			}
		}
		
		return obj;
	}
	public static T GetNearest<T>(this IList<T> array, Vector3 fromPos) where T : Component
	{
		T obj = null;
		float lastDist = float.MaxValue;
		foreach (var element in array)
		{
			float dist = Vector3.Distance(fromPos, element.transform.position);
			if(dist < lastDist)
			{
				lastDist = dist;
				obj = element;
			}
		}
		
		return obj;
	}
	
	public static T GetNearest<T>(this T[] array, Vector3 fromPos, System.Func<T, bool> customCompare) where T : Component
	{
		T obj = null;
		float lastDist = float.MaxValue;
		foreach (var element in array)
		{
			float dist = Vector3.Distance(fromPos, element.transform.position);
			if(dist < lastDist && customCompare.Invoke(element))
			{
				lastDist = dist;
				obj = element;
			}
		}
		
		return obj;
	}
	public static T GetNearest<T>(this IList<T> array, Vector3 fromPos, System.Func<T, bool> customCompare) where T : Component
	{
		T obj = null;
		float lastDist = float.MaxValue;
		foreach (var element in array)
		{
			float dist = Vector3.Distance(fromPos, element.transform.position);
			if(dist < lastDist && customCompare.Invoke(element))
			{
				lastDist = dist;
				obj = element;
			}
		}
		
		return obj;
	}
	#endregion
	
	#region Math
	public static Vector4 Plus(this Vector2 a, Vector4 b)
	{
		return new Vector4(a.x + b.x, a.y + b.y, b.z, b.w);
	}
	public static Vector4 Plus(this Vector3 a, Vector4 b)
	{
		return new Vector4(a.x + b.x, a.y + b.y, a.z + b.z, b.w);
	}
	public static Vector4 Plus(this Vector4 a, Vector4 b)
	{
		return new Vector4(a.x + b.x, a.y + b.y, a.z + b.z, a.w + b.w);
	}
	
	public static Vector4 Minus(this Vector2 a, Vector4 b)
	{
		return new Vector4(a.x - b.x, a.y - b.y, b.z, b.w);
	}
	public static Vector4 Minus(this Vector3 a, Vector4 b)
	{
		return new Vector4(a.x - b.x, a.y - b.y, a.z - b.z, b.w);
	}
	public static Vector4 Minus(this Vector4 a, Vector4 b)
	{
		return new Vector4(a.x - b.x, a.y - b.y, a.z - b.z, a.w - b.w);
	}
	
	public static Vector4 Multiply(this Vector2 a, Vector4 b)
	{
		return new Vector4(a.x * b.x, a.y * b.y, b.z, b.w);
	}
	public static Vector4 Multiply(this Vector3 a, Vector4 b)
	{
		return new Vector4(a.x * b.x, a.y * b.y, a.z * b.z, b.w);
	}
	public static Vector4 Multiply(this Vector4 a, Vector4 b)
	{
		return new Vector4(a.x * b.x, a.y * b.y, a.z * b.z, a.w * b.w);
	}
	
	public static Vector4 Divide(this Vector2 a, Vector4 b)
	{
		return new Vector4(a.x / b.x, a.y / b.y, b.z, b.w);
	}
	public static Vector4 Divide(this Vector3 a, Vector4 b)
	{
		return new Vector4(a.x / b.x, a.y / b.y, a.z / b.z, b.w);
	}
	public static Vector4 Divide(this Vector4 a, Vector4 b)
	{
		return new Vector4(a.x / b.x, a.y / b.y, a.z / b.z, a.w / b.w);
	}
	#endregion
	
	#region	CONVERT
        public static Vector2Int ConvertToVectorInt2(this Vector2 vector)
        {
            return new Vector2Int((int)vector.x, (int)vector.y);
        }

        public static Vector2Int ConvertToVectorInt2(this Vector3 vector)
        {
            return new Vector2Int((int)vector.x, (int)vector.y);
        }

        public static Vector2Int ConvertToVectorInt2(this Vector4 vector)
        {
            return new Vector2Int((int)vector.x, (int)vector.y);
        }

        public static Vector3Int ConvertToVectorInt3(this Vector2 vector)
        {
            return new Vector3Int((int)vector.x, (int)vector.y, 0);
        }

        public static Vector3Int ConvertToVectorInt3(this Vector3 vector)
        {
            return new Vector3Int((int)vector.x, (int)vector.y, (int)vector.z);
        }

        public static Vector3Int ConvertToVectorInt3(this Vector4 vector)
        {
            return new Vector3Int((int)vector.x, (int)vector.y, (int)vector.z);
        }

        public static Vector3 ConvertToVector3(this Vector2Int vector)
        {
            return new Vector3(vector.x, vector.y);
        }

        public static Vector3 ConvertToVector3(this Vector3Int vector)
        {
            return new Vector3(vector.x, vector.y, vector.z);
        }
        #endregion CONVERT
}}