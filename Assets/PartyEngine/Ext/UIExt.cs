﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace PartyEngine.Ext {
public static class UIExt
{
	public static void RegisterSingleAction(this Button btn, System.Action action)
	{
		btn.onClick.RemoveAllListeners();
		btn.onClick.AddListener(action.Invoke);
	}
	public static void RegisterAction(this Button btn, System.Action action)
	{
		btn.onClick.AddListener(action.Invoke);
	}
	
	public static Text GetTextComponent(this Component comp)
	{
		return comp.GetComponent<Text>() ?? comp.transform.GetChild(0).GetComponent<Text>();
	}
	
	#region IMAGE COLOR
	public static void SetColorWithoutAlpha(this Image img, Color color)
	{
		img.color = new Color(color.r, color.g, color.b, img.color.a);
	}
	public static void SetRGB(this Image img, float r, float g, float b)
	{
		img.color = new Color(r, g, b, img.color.a);
	}
	public static void SetR(this Image img, float r)
	{
		var color = img.color;
		color.r = r;
		img.color = color;
	}
	public static void SetG(this Image img, float g)
	{
		var color = img.color;
		color.g = g;
		img.color = color;
	}
	public static void SetB(this Image img, float b)
	{
		var color = img.color;
		color.b = b;
		img.color = color;
	}
	public static void SetAlpha(this Image img, float a)
	{
		var color = img.color;
		color.a = a;
		img.color = color;
	}
	#endregion
}}