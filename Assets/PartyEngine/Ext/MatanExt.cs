﻿using UnityEngine;
using PartyEngine.Types;

namespace PartyEngine.Ext {
public static class MatanExt 
{
	/// <returns>The ratio of width to height (width / height)</returns>
	public static float AspectRatio { get { return (float)Screen.width / (float)Screen.height;}}
		
	//TODO : need refactor
	public static bool Is16_9 { get { return MatanExt.AspectRatio > 1.75f;}}
	public static bool Is16_10 { get { return MatanExt.AspectRatio > 1.55f;}}
	public static bool Is4_3 { get { return MatanExt.AspectRatio < 1.35f;}}
	
	/// <returns>v1 / v2</returns>
	public static float GetFactor(float v1, float v2)
	{
		return v1 / v2;
	}
	
	/// <summary>If min > value || value > max, return middle between min and max. Else return value as is.</summary>
	public static float ClampToAverage(float value, float min, float max)
	{
		if(value < min || value > max) return Average(min, max);
		else return value;
	}
	
	public static float Average(float min, float max)
	{
		return (max + min) / 2;
	}
	public static int Average(int min, int max)
	{
		return (max + min) / 2;
	}
	
	/// <returns>i + 1</returns>
	public static int Incr(this int i)
	{
		return i+1;
	}
	/// <returns>i - 1</returns>
	public static int Decr(this int i)
	{
		return i-1;
	}
	
	/// <returns>True if i != 0</returns>
	public static bool ToBool(this int i)
	{
		return i != 0;
	}	
	
	/// <returns>1 if true. else 0</returns>
	public static int ToInt32(this bool b)
	{
		return b ? 1 : 0;
	}
	
	/// <param name="discardParts">If true, return digit before dot. Else convert to nearest number</param>
	public static int ToInt32(this float value, bool discardParts = false)
	{
		if(discardParts) return (int)value;
		else return System.Convert.ToInt32(value);
	}
	
	/// <summary> Move with positive speed if need increase (target > current). Else with negative </summary>
	public static float MoveTowards(float current, float target, float positiveSpeed, float negativeSpeed)
	{
		if(target > current)
			return Mathf.MoveTowards(current, target, positiveSpeed);
		else
			return Mathf.MoveTowards(current, target, negativeSpeed);
	}

	public static Vector2 GetPositionAroundCricle(float radius)
	{
		return MatanExt.AngleToVector2(Random.Range(0f, 360f)) * radius;
	}
	
	public static Vector2 AngleToVector2(float angle)
	{
		return RadianToVector2(angle * Mathf.Deg2Rad);
	}
	public static Vector2 RadianToVector2(float radian)
	{
		return new Vector2(Mathf.Sin(radian), Mathf.Cos(radian));
	}

	public static float DirectionToAngle(Vector2 direction)
	{
		return Vector2.SignedAngle(Vector2.up, direction);
	}
	public static float DirectionToAngle360(Vector2 direction)
	{
		float angle = Vector2.SignedAngle(Vector2.up, direction);
		if(angle < 0)
			angle = 360 + angle;
		
		return angle;
	}
		
	/// <returns>If value between min and max(inclusive). Example: 5.Between(0, 10) return true; 10.Between(2, 8) return false;</returns>
	public static bool Between(this int value, int min, int max)
	{
		return value > min && value < max;
	}
	/// <returns>If value between min and max(inclusive). Example: 5.Between(0, 10) return true; 10.Between(2, 8) return false;</returns>
	public static bool Between(this float value, float min, float max)
	{
		return value > min && value < max;
	}
	
	/// <summary> Compare two floats with specified precision</summary>
	public static bool EqualsPrecision(this float value, float compare, int precision)
	{
		string format = "0.";
		for (int i = 0; i < precision; i++)
		{
			format += "0";
		}
		
		string v = value.ToString(format);
		string c = compare.ToString(format);

		return v == c;
	}
	
	/// <returns>How much extracted</returns>
	public static float ExtractValue(MinMaxValue<ValueEx<float>> value, float count)
	{
		return ExtractValue(value.Current, count);
	}
	/// <returns>How much extracted</returns>
	public static float ExtractValue(ValueEx<float> value, float count)
	{
		float val = value.Value;
		float extracted = ExtractValue(ref val, count);
		value.Value = val;
		return extracted;
	}
	/// <returns>How much extracted</returns>
	public static float ExtractValue(ref float value, float count)
	{
		if(count <= 0) return 0;
	
		float extracted = (value > count) ? count : value;

		value = Mathf.Max(value - count, 0);
		
		return extracted;
	}
	
	/// <returns>Remainder of count+value - maxValue</returns>
	public static float InsertValue(MinMaxValue<ValueEx<float>> value, float count)
	{
		return InsertValue(value.Current, value.Max.Value, count);
	}
	/// <returns>Remainder of count+value - maxValue</returns>
	public static float InsertValue(ValueEx<float> value, float maxValue, float count)
	{
		float val = value.Value;
		float remainder = InsertValue(ref val, maxValue, count);
		value.Value = val;
		return remainder;
	}
	/// <returns>Remainder of count+value - maxValue</returns>
	public static float InsertValue(ref float value, float maxValue, float count)
	{
		if(count <= 0) return count;
		
		float remainder = Mathf.Min(value+count - maxValue, 0);
		
		value = Mathf.Min(value+count, maxValue);
		
		return remainder;
	}
}}