﻿using System.Collections.Generic;
using UnityEngine;

namespace PartyEngine.Ext {
public static class TransformExt 
{
	/// <summary>Modify only 'x' property of position</summary>
	public static void SetPosX(this Transform transform, float value)
	{
		transform.position = new Vector3(value, transform.position.y, transform.position.z);
	}
	/// <summary>Modify only 'y' property of position</summary>
	public static void SetPosY(this Transform transform, float value)
	{
		transform.position = new Vector3(transform.position.x, value, transform.position.z);
	}
	/// <summary>Modify only 'z' property of position</summary>
	public static void SetPosZ(this Transform transform, float value)
	{
		transform.position = new Vector3(transform.position.x, transform.position.y, value);
	}
	/// <summary>Modify only 'x' and 'y' properties of position</summary>
	public static void SetPosXY(this Transform transform, float x, float y)
	{
		transform.position = new Vector3(x, y, transform.position.z);
	}

	
	/// <summary>Modify only 'x' property of eulerAngles</summary>
	public static void SetEulerX(this Transform transform, float value)
	{
		transform.eulerAngles = new Vector3(value, transform.eulerAngles.y, transform.eulerAngles.z);
	}
	/// <summary>Modify only 'y' property of eulerAngles</summary>
	public static void SetEulerY(this Transform transform, float value)
	{
		transform.eulerAngles = new Vector3(transform.eulerAngles.x, value, transform.eulerAngles.z);
	}
	/// <summary>Modify only 'z' property of eulerAngles</summary>
	public static void SetEulerZ(this Transform transform, float value)
	{
		transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, value);
	}
	
	
	public static RectTransform GetRectTransform(this Component target)
	{
		return target.transform as RectTransform;
	}
	public static RectTransform GetRectTransform(this GameObject target)
	{
		return target.transform as RectTransform;
	}
	
	public static Transform GetParent(this Component target)
	{
		return target.transform.parent;
	}
	
	/// <summary>Return highest parent from hierarchy. Such as transform.parent.parent.parent...</summary>
	/// <param name="target">Transform which need get parent</param>
	/// <returns>Highest parent. If current transform is highest, return him.</returns>
	public static Transform GetGeneralParent(this Transform target)
	{
		while(target.parent != null)
			target = target.parent;
		
		return target;
	}
	
	/// <summary>
	/// Get childs from current Transform. Not recursively.
	/// If hasnt childs - return empty array (Transform[0])
	/// </summary>
	/// <param name="target">Transform with which need get childs</param>
	/// <returns>Childs array</returns>
	public static Transform[] GetChilds(this Transform target)
	{
		Transform[] temp = new Transform[target.childCount];
		for(int i=0; i<temp.Length; i++)
		{
			temp[i] = target.GetChild(i);
		}
		
		return temp;
	}
	
	static List<Transform> tempTransforms = new List<Transform>();
	/// <summary>
	/// Get childs from current Transform and them childs. Recursively. Such as transform.GetChilds().GetChilds().GetChilds()...
	/// If hasnt childs - return empty array (Transform[0])
	/// </summary>
	/// <param name="target">Transform with which need get childs</param>
	/// <returns>Childs array</returns>
	public static Transform[] GetAllChilds(this Transform target)
	{
		tempTransforms.Clear();
		
		GetAllChilds(GetChilds(target));
		
		return tempTransforms.ToArray();
	}
	static void GetAllChilds(Transform[] childs)
	{
		foreach(var child in childs)
		{
			tempTransforms.Add(child);
			GetAllChilds(GetChilds(child));
		}
	}
	
	public static Transform GetFirstChild(this Transform target)
	{
		return target.GetChild(0);
	}

	public static Transform GetLastChild(this Transform target)
	{
		return target.GetChild(target.childCount-1);
	}
		
	/// <summary> Return component of child. Must have child on current index. </summary>
	public static T GetChildComponent<T>(this Transform target, int childIndex) where T : Component
	{
		return target.GetChild(childIndex).GetComponent<T>();
	}
	
	/// <summary>Destroy childs from current Transform.</summary>
	/// <param name="target">Transform from which need destroy childs</param>
	public static void DestroyChilds(this Transform target)
	{
		Transform[] childs = GetChilds(target);
		for(int i=childs.Length-1; i>=0; i--)
			Object.Destroy(childs[i].gameObject);
		
		/*
		  for (int i = transform.childCount - 1; i >= 0; i--)
            {
                var tr = transform.GetChild(i);
                Object.Destroy(tr.gameObject);
                tr.SetParent(null);
            }
		  */
	}
	/// <summary>Destroy childs from current Transform. Editor friendly vesion</summary>
	/// <param name="target">Transform from which need destroy childs</param>
	public static void DestroyChildsImmediate(this Transform target)
	{
		Transform[] childs = GetChilds(target);
		for(int i=childs.Length-1; i>=0; i--)
			Object.DestroyImmediate(childs[i].gameObject);
		
		/*
		 for (int i = transform.childCount - 1; i >= 0; i--)
                Object.DestroyImmediate(transform.GetChild(i).gameObject);
		 */
	}
	
	/// <summary>
	/// Find child who gameObject.name equals 'name' parametr. 
	/// Find on transform childs and them childs. Recursively.
	/// </summary>
	/// <param name="target">Transform from which need find child</param>
	/// <param name="childName">Child name to need find</param>
	/// <returns>Founded transform. Or null if current name not found</returns>
	public static Transform FindChildDeep(this Transform target, string childName)
	{
		Transform[] childs = GetAllChilds(target);
		foreach(var child in childs)
			if(child.gameObject.name == childName)
				return child;
		
		return null;
	}
	
	/// <summary>Set Transform component local params as default. (zero, zero, one)</summary>
	/// <param name="target">Transform which need reset</param>
	public static void ResetLocalParams(this Transform target)
	{
		target.localPosition = Vector3.zero;
		target.localRotation = Quaternion.Euler(Vector3.zero);
		target.localScale = Vector3.one;
	}
	
	/// <summary> Set Transform component params as default. (zero, zero, one)</summary>
	/// <param name="target">Transform which need reset</param>
	public static void ResetGlobalParams(this Transform target)
	{
		target.position = Vector3.zero;
		target.rotation = Quaternion.Euler(Vector3.zero);
		target.localScale = Vector3.one;
	}

	/// <summary> Rotates the transform so the up vector points at /target/'s current position. Rotate z-axis in 2d mode </summary>
	public static void LookAt2D(this Transform current, Transform target)
	{
		current.localEulerAngles += Vector3.forward * GetAngle2D(current, target);
	}
	/// <summary> Rotates the transform so the up vector points at /target/'s current position. Rotate z-axis in 2d mode </summary>
	public static void LookAt2D(this Transform current, Vector3 target)
	{
		current.localEulerAngles += Vector3.forward * GetAngle2D(current, target);
	}

	/// <summary> Get a rotation in 2d mode which need to rotate current transform. From yellow axis to target </summary>
	public static float GetAngle2D(this Transform current, Transform target)
	{
		return GetAngle2D(current, target.position);
	}
	/// <summary> Get a rotation in 2d mode which need to rotate current transform. From yellow axis to target </summary>
	public static float GetAngle2D(this Transform current, Vector3 target)
	{
		int modifer = Vector3.Angle(target - current.position, current.right) > 90 ? 1 : -1;
		float angle = Vector3.Angle(target - current.position, current.up);
		
		return angle * modifer;
	}
	
	public static float GetDistance(this Transform current, Vector3 target){ return GetDistance(current.position, target);}
	public static float GetDistance(this Vector3 current, Transform target){ return GetDistance(current, target.position);}
	public static float GetDistance(this Transform current, Transform target){ return GetDistance(current.position, target.position);}
	public static float GetDistance(this Vector3 current, Vector3 target)
	{
		return Vector3.Distance(current, target);
	}
}}