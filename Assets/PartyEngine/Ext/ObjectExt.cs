﻿using UnityEngine;

namespace PartyEngine.Ext {
public static class ObjectExt 
{	
	public static T Instantiate<T>(this GameObject prefab, Transform parent = null)
	{
		return Object.Instantiate(prefab, parent).GetComponent<T>();
	}
	
	public static void SetActiveGO(this Component comp, bool active)
	{
		comp.gameObject.SetActive(active);
	}
	
	public static void SwitchActiveGO(this Component comp)
	{
		comp.gameObject.SetActive(!comp.gameObject.activeInHierarchy);
	}
	public static void SwitchActive(this GameObject obj)
	{
		obj.SetActive(!obj.activeInHierarchy);
	}
	
	public static void Destroy(this Object obj)
	{
		Object.Destroy(obj);
	}
	
	/// <summary>Destroy elements. Started with last index</summary>
	public static void Destroy(params Object[] array)
	{
		for(int i=array.Length-1; i>=0; i--)
			Object.Destroy(array[i]);
	}
	
	/// <summary>Get component from object. If object hasn't component, method add new to him. </summary>
	/// <param name="obj">GameObject with needed component</param>
	/// <returns>Geted or added component</returns>
	public static T GuarantedGetComponent<T>(this GameObject obj) where T : Component
	{
		T component = obj.GetComponent<T>();
		
		if(component == null) 
			component = obj.AddComponent<T>();
		
		return component;
	}
	/// <summary>Get component from object. If object hasn't component, method add new to him. </summary>
	public static T GuarantedGetComponent<T>(this Component obj) where T : Component
	{
		return obj.gameObject.GuarantedGetComponent<T>();
	}
	
	/// <summary>Add component to object and return him. If componen already attached - get and return his.</summary>
	/// <returns>Attached componet</returns>
	public static T GuarantedAddComponent<T>(this GameObject obj) where T : Component
	{
		T component = obj.AddComponent<T>();
		
		if(component == null) 
			component = obj.GetComponent<T>();
		
		if(component == null)
			Debugger.LogError("Wow! Function can't add and can't get component. Something was wrong! Fix it ASAP!", obj);
		
		return component;
	}
	/// <summary>Add component to object and return him. If componen already attached - get and return his.</summary>
	public static T GuarantedAddComponent<T>(this Component obj) where T : Component
	{
		return obj.gameObject.GuarantedAddComponent<T>();
	}
	
	/// <summary>Create a GameObject with specified name and parent. Set local transforms params as default </summary>
	/// <param name="name">Name of GameObject</param>
	/// <param name="parent">Transform which need set as parent</param>
	/// <returns>Created GameObject</returns>
	public static GameObject CreateGameObject(string name, Transform parent = null)
	{
		GameObject newObj = new GameObject(name);
		newObj.transform.SetParent(parent);
		newObj.transform.localPosition = Vector3.zero;
		newObj.transform.localRotation = Quaternion.Euler(Vector3.zero);
		newObj.transform.localScale = Vector3.one;
		
		return newObj;
	}	

}}