﻿using System;
using UnityEngine;

namespace PartyEngine.Ext {
public static class ActionExt
{
	/// <summary>Invoke actions if they != null </summary>
	/// <param name="actions">One or more acions to call</param>
	public static void SafetyInvoke(params System.Action[] actions)
	{
		foreach(var action in actions)
			if(action != null)
				action.Invoke();
	}//
	
	/// <summary>Invoke action if it != null </summary>
	/// <param name="action">Acions to call</param>
	public static void SafetyInvoke<T>(this System.Func<T> action)
	{
		if(action != null) action.Invoke();
	}
	
	/// <summary>Invoke action if it != null </summary>
	/// <param name="action">Acions to call</param>
	public static void SafetyInvoke(this System.Action action)
	{
		if(action != null) action.Invoke();
	}
	public static void SafetyInvoke<T>(this System.Action<T> action, T arg1)
	{
		if(action != null) action.Invoke(arg1);
	}
	public static void SafetyInvoke<T, T2>(this System.Action<T, T2> action, T arg1, T2 arg2)
	{
		if(action != null) action.Invoke(arg1, arg2);
	}
	public static void SafetyInvoke<T, T2, T3>(this System.Action<T, T2, T3> action, T arg1, T2 arg2, T3 arg3)
	{
		if(action != null) action.Invoke(arg1, arg2, arg3);
	}
	public static void SafetyInvoke<T, T2, T3, T4>(this System.Action<T, T2, T3, T4> action, T arg1, T2 arg2, T3 arg3, T4 arg4)
	{
		if(action != null) action.Invoke(arg1, arg2, arg3, arg4);
	}
	
	public static void InvokeWithTryCatch(this System.Action action)
	{
		try{
			action.SafetyInvoke();
		}catch(System.Exception ex){Debugger.LogError(ex);}
	}
}}