﻿using System.Collections.Generic;
using UnityEngine;

namespace PartyEngine.Ext {
public static class DictionaryExt 
{
	public static T1[] GetKeyArray<T1, T2>(this Dictionary<T1, T2> dick)
	{
		return GetKeyList(dick).ToArray();
	}
	public static List<T1> GetKeyList<T1, T2>(this Dictionary<T1, T2> dick)
	{
		List<T1> keys = new List<T1>();
		foreach(var key in dick.Keys)
			keys.Add(key);
		
		return keys;
	}

	public static T2[] GetValueArray<T1, T2>(this Dictionary<T1, T2> dick)
	{
		return GetValueList(dick).ToArray();
	}
	public static List<T2> GetValueList<T1, T2>(this Dictionary<T1, T2> dick)
	{
		List<T2> values = new List<T2>();
		foreach(var value in dick.Values)
			values.Add(value);
		
		return values;
	}
	
	public static T1 GetKeyByValue<T1, T2>(this Dictionary<T1, T2> dick, T2 value)
	{
		var keys = GetKeyList(dick);
		foreach(var key in keys)
		{
			if(dick[key].Equals(value))
				return key;
		}
		
		Debug.LogWarning("404 Value '"+value+"' not found in dictionary!");
		return default(T1);
	}
	
}}