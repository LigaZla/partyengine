﻿
namespace PartyEngine.Ext {
public static class DateTimeExt
{
	/// <summary> 6/15/2008 9:15 PM </summary>
	public static string ToStringFullDateAndShortTime(this System.DateTime date)
	{
		return date.ToString("g", System.Globalization.DateTimeFormatInfo.InvariantInfo);
	}
	
	/// <summary> 6/15/2008 9:15:07 PM </summary>
	public static string ToStringFullDateAndTime(this System.DateTime date)
	{
		return date.ToString("G", System.Globalization.DateTimeFormatInfo.InvariantInfo);
	}
	
	/// <summary> 6/15/2008 </summary>
	public static string ToStringFullDate(this System.DateTime date)
	{
		return date.ToString("d", System.Globalization.DateTimeFormatInfo.InvariantInfo);
	}
	
	/// <summary> Sunday, June 15, 2008 9:15:07 PM </summary>
	public static string ToStringFullTextedDate(this System.DateTime date)
	{
		return date.ToString("F", System.Globalization.DateTimeFormatInfo.InvariantInfo);
	}
	
	/// <summary> 9:15 PM </summary>
	public static string ToStringShortTime(this System.DateTime date)
	{
		return date.ToString("t", System.Globalization.DateTimeFormatInfo.InvariantInfo);
	}
	
	/// <summary> 9:15:07 PM </summary>
	public static string ToStringFullTime(this System.DateTime date)
	{
		return date.ToString("T", System.Globalization.DateTimeFormatInfo.InvariantInfo);
	}
	
	/// <summary> 2008-06-15 21:15:07Z </summary>
	public static string ToStringUtc(this System.DateTime date)
	{
		return date.ToString("u", System.Globalization.DateTimeFormatInfo.InvariantInfo);
	}
	
	public static string ToStringFileName(this System.DateTime date)
	{
		string fileName = date.ToStringFullDateAndTime();
		
		fileName = fileName.ReplaceSymbols("/", "_");
		fileName = fileName.ReplaceSymbols(":", "-");
		
		return fileName;
	}
	//       d: 6/15/2008
	//       D: Sunday, June 15, 2008
	//       f: Sunday, June 15, 2008 9:15 PM
	//       F: Sunday, June 15, 2008 9:15:07 PM
	//       g: 6/15/2008 9:15 PM
	//       G: 6/15/2008 9:15:07 PM
	//       m: June 15
	//       o: 2008-06-15T21:15:07.0000000
	//       R: Sun, 15 Jun 2008 21:15:07 GMT
	//       s: 2008-06-15T21:15:07
	//       t: 9:15 PM
	//       T: 9:15:07 PM
	//       u: 2008-06-15 21:15:07Z
	//       U: Monday, June 16, 2008 4:15:07 AM
	//       y: June, 2008
}}