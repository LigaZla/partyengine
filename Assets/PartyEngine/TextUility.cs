﻿using System.Security.Cryptography;
using System.Text;

namespace PartyEngine {
public static class TextUility {

	public static string GetMd5Hash(string input)
	{
		MD5 md5Hash = MD5.Create();
		
		byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
		
		StringBuilder sBuilder = new StringBuilder();
		for (int i = 0; i < data.Length; i++)
		{
			sBuilder.Append(data[i].ToString("x2"));
		}
		
		return sBuilder.ToString();
	}
	
	/// <summary>Seconds as minutes:seconds (MM:SS)</summary>
	public static string TimeToString(int seconds)
	{
		const int SECONDS_IN_MINUTE = 60;
		string zero = (seconds%SECONDS_IN_MINUTE > 9) ? ":" : ":0";
		return (seconds / SECONDS_IN_MINUTE) + zero + (seconds % SECONDS_IN_MINUTE);
	}
	
	public static bool IsNullOrEmpty(this string text)
	{
		return string.IsNullOrEmpty(text);
	}
	
	public static string SafetyRemove(this string s, int startIndex)
	{
		if(startIndex >= s.Length) return s;
		if(startIndex < 0) startIndex = 0;
		
		return s.Remove(startIndex);
	}
	
	public static string RemoveCharsFromEnd(this string s, int count)
	{
		return s.Remove(s.Length - count);
	}
	
	public static bool HasPartOrFullOf(this string text, string part)
	{
		if(part.IsNullOrEmpty()) return true;
		return text.DeleteSymbols(part) != text;
	}
	
	/// <summary>Replace all symbols on 'replace' to specified symbol</summary>
	public static string ReplaceSymbols(this string text, string replace, string symbol)
	{
		return System.Text.RegularExpressions.Regex.Replace(text, "("+replace+"\\.?)", symbol);
	}
	
	/// <summary>Delete all symbols in text</summary>
	public static string DeleteSymbols(this string text, params string[] symbols)
	{
		foreach(var element in symbols)
			text = ReplaceSymbols(text, element, "");
		
		return text;
	}
	
	public static char GetLastChar(this string text)
	{
		return text[text.Length-1];
	}
	
	public static string ZeroFormat(this float text, string pattern = "0.0")
	{
		string txt = RemoveZeroFromEnd(text.ToString(pattern));
		return txt[txt.Length-1] == '.' ? txt.Remove(txt.Length-1) : txt;
	}
	static string RemoveZeroFromEnd(string txt)
	{
		if(txt[txt.Length-1] == '0')
			return RemoveZeroFromEnd(txt.Remove(txt.Length-1));
		else 
			return txt;
	}
	
	/*public static void CreateXMLBasedLine(params object[] objects)
	{
		foreach(var element in objects)
	}*/
	
	/// <returns>If text is true or false - return him. Else return false</returns>
	public static bool ToBool(this string text)
	{
		bool value = false;
		return bool.TryParse(text, out value) ? value : false;
	}
	
	/// <returns>If text is number - return him. Else return 0</returns>
	public static int ToInt32(this string text)
	{
		int value = 0;
		return int.TryParse(text, out value) ? value : 0;
	}
	
	/// <returns>If text is number - return him. Else return 0</returns>
	public static float ToFloat(this string text)
	{
		float value = 0;
		return float.TryParse(text, out value) ? value : 0;
	}
}}