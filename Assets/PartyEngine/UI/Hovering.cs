﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using PartyEngine.Ext;

namespace PartyEngine.UI {

public static class HoveringEx
{
	public static Hovering GetHovering(this Component comp)
	{
		return comp.GetComponent<Hovering>();
	}
	public static Hovering GetHovering(this GameObject obj)
	{
		return obj.GetComponent<Hovering>();
	}
	
	public static void ChangeSelected(Hovering oldSelected, Hovering newSelected)
	{
		if(oldSelected != null)
			oldSelected.SetSelected(false);
		
		newSelected.SetSelected(true);
	}	
}

public class Hovering : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler
{
	class Machine : SimpleStateMachine
	{
		public const uint NORMAL = 1;
		public const uint HOVERED = 2;
		public const uint PRESSED = 4;
		public const uint SELECTED = 8;
		public const uint DISABLED = 16;
	}
		
	public bool IsHovered { get { return stateMachine.InState(Machine.HOVERED);}}
	public bool IsPressed { get { return stateMachine.InState(Machine.PRESSED);}}
	public bool IsSelected { get { return stateMachine.InState(Machine.SELECTED);}}
	public bool IsDisabled { get { return stateMachine.InState(Machine.DISABLED);}}
	
	public System.Action OnSetNormal;
	public System.Action OnSetHovered;
	public System.Action OnSetPressed;
	public System.Action OnSetSelected;
	public System.Action OnSetDisabled;
	public System.Action OnEnabled;
	
	[SerializeField] bool disabledOnStart;
	[SerializeField] bool connectWithBtn = true;
	
	readonly Machine stateMachine = new Machine();
	bool isButtonPressed;
	bool initialized;

	void Awake()
	{
		Initialize();
	}
	public void Initialize()
	{
		if(initialized) return;
		
		stateMachine.ClearTransitions();
		
		stateMachine.AddTransition(Machine.NORMAL, ToNormal);
		stateMachine.AddTransition(Machine.HOVERED, ToHovered);
		stateMachine.AddTransition(Machine.PRESSED, ToPressed);
		stateMachine.AddTransition(Machine.SELECTED, ToSelected);		
		stateMachine.AddTransition(Machine.DISABLED, ToDisabled);
		
		var comps = GetComponents<HoveringBase>();
		comps.DoAction(x=> x.Initialize());
		
		initialized = true;
		
		if(connectWithBtn)
		{
			Button connectedBtn = GetComponent<Button>() ?? transform.GetChildComponent<Button>(0) ?? transform.GetChildComponent<Button>(1);
			OnSetDisabled += ()=> connectedBtn.enabled = false;
			OnEnabled += ()=> connectedBtn.enabled = true;
		}
		
		if(disabledOnStart) SetDisabled();
		else SetNormal();
	}
	
	public void SetActiveScript(bool active)
	{
		enabled = active;
	}
	public void SetActiveScriptAndSetDisabled(bool active)
	{
		enabled = active;
		SetDisabled(!active);
	}
	
	public void SetDisabled(bool disabled)
	{
		if(disabled) 
			SetDisabled();
		else 
		{
			SetNormal();
			OnEnabled.SafetyInvoke();
		}
	}
	
	public void SetSelected(bool selected)
	{
		if(selected)
			SetSelected();
		else
			SetNormal();
	}
	
	public void OnPointerEnter(PointerEventData eventData)
	{
		if(!stateMachine.InState(Machine.DISABLED | Machine.SELECTED))
		{
			if(!isButtonPressed)
				SetHovered();
			if(isButtonPressed)
				SetPressed();	   	
		}
	}
	
	public void OnPointerExit(PointerEventData eventData)
	{
		if(stateMachine.InState(Machine.HOVERED | Machine.PRESSED))
			SetNormal();
	}
	
	public void OnPointerDown(PointerEventData eventData)
	{
		isButtonPressed = true;
		
		if(!stateMachine.InState(Machine.SELECTED | Machine.DISABLED))
			SetPressed();
	}
	
	public void OnPointerUp(PointerEventData eventData)
	{
		isButtonPressed = false;
		
		if(stateMachine.InState(Machine.PRESSED))
			SetHovered();
	}
	
	void SetNormal(){stateMachine.ChangeState(Machine.NORMAL);}
	void SetHovered(){stateMachine.ChangeState(Machine.HOVERED);}
	void SetPressed(){stateMachine.ChangeState(Machine.PRESSED);}
	void SetDisabled(){stateMachine.ChangeState(Machine.DISABLED);}
	void SetSelected(){stateMachine.ChangeState(Machine.SELECTED);}
	
	void ToNormal(){OnSetNormal.SafetyInvoke();}
	void ToHovered(){OnSetHovered.SafetyInvoke();}
	void ToPressed(){OnSetPressed.SafetyInvoke();}
	void ToDisabled(){OnSetDisabled.SafetyInvoke();}
	void ToSelected(){OnSetSelected.SafetyInvoke();}
	
	public void ApplyAllPresets()
	{
		var comps = GetComponents<HoveringBase>();
		comps.DoAction(x=> x.ApplyPresetsOnEditor(disabledOnStart));
	}
}}