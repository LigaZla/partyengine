﻿using UnityEngine;
using UnityEngine.UI;

namespace PartyEngine.UI {
public class ButtonFields
{
	[SerializeField] Image image;
	[SerializeField] Button button;
	[SerializeField] Text text;
	[SerializeField] RectTransform tform;
	
	public GameObject GameObject { get { return tform.gameObject;}}
	public RectTransform RectTransform { get { return tform;}}
	public Image Image { get { return image;}}
	public Button Button { get { return button;}}
	public Text Text { get { return text;}}
	
}}