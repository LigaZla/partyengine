﻿using UnityEngine;
using UnityEngine.UI;
using PartyEngine.UI.Presets;

namespace PartyEngine.UI {
public class HoveringImageSprite : HoveringBase<HoveringSpritePreset, Sprite>
{
	[SerializeField] Image image;

	protected override void InitializeComponent()
	{
		if(image == null) image = GetComponent<Image>();
	}

	protected override void ApplyPreset(Sprite preset)
	{
		image.sprite = preset;
	}
}}