﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(PartyEngine.UI.Hovering))]
public class HoveringEditor : Editor
{
	SerializedProperty disabledOnStart;
	SerializedProperty connectWithBtn;

	protected void OnEnable()
	{
		disabledOnStart = serializedObject.FindProperty("disabledOnStart");
		connectWithBtn = serializedObject.FindProperty("connectWithBtn");
	}

	public override void OnInspectorGUI()
	{
		serializedObject.Update();

		DrawPosition();

		serializedObject.ApplyModifiedProperties();
	}

	void DrawPosition()
	{
		GUILayout.BeginVertical();

		EditorGUILayout.PropertyField(disabledOnStart);
		EditorGUILayout.PropertyField(connectWithBtn);

		bool clicked = GUILayout.Button("Apply all Presets");
		
		GUILayout.EndVertical();

		if(clicked)
			(serializedObject.targetObject as PartyEngine.UI.Hovering).ApplyAllPresets();
	}
   
}