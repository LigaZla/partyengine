﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PartyEngine.UI.Presets {
[CreateAssetMenu(fileName = "SpritePreset", menuName = "UI.Presets/SpritePreset")]
public class SpritePreset : ScriptableObject
{
	public Sprite sprite;
	public Color color = Color.white;
	public Material material;
	public bool raycastTarget;
	public bool preserveAspect;
	[Header("Image type")]
	public Image.Type type = Image.Type.Simple;
	public Image.FillMethod fillMethod;
	
	public Image.OriginHorizontal originHorizontal;
	public Image.OriginVertical originVertical;
	public Image.Origin90 origin90;
	public Image.Origin180 origin180;
	public Image.Origin360 origin360;
	
	public float fillAmount;
	public bool clockWise;
	public bool fillCenter;
}}