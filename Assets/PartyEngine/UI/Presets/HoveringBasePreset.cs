﻿using UnityEngine;

namespace PartyEngine.UI.Presets {
public abstract class HoveringBasePreset<T> : ScriptableObject
{
	public T normal;
	public T hovered;
	public T pressed;
	public T disabled;
	public T selected;
}}