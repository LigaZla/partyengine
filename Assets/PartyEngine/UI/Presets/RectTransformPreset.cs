﻿using UnityEngine;

namespace PartyEngine.UI.Presets {
[CreateAssetMenu(fileName = "RectTransformPreset", menuName = "UI.Presets/RectTransformPreset")]
public class RectTransformPreset : TransformPreset
{
	public Vector2 sizeDelta = new Vector2(100, 100);
	[Header("Anchor")]
	public Vector2 anchorMin = new Vector2(0.5f, 0.5f);
	public Vector2 anchorMax = new Vector2(0.5f, 0.5f);
	public Vector2 pivot = new Vector2(0.5f, 0.5f);
}}