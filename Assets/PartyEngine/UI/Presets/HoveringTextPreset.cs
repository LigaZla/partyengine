﻿using UnityEngine;

namespace PartyEngine.UI.Presets {
[CreateAssetMenu(fileName = "HoveringTextPreset", menuName = "UI.Presets/HoveringTextPreset")]
public class HoveringTextPreset : HoveringBasePreset<HoveringTextPreset.TextPreset>
{
	[System.Serializable]
	public class TextPreset
	{
		public FontStyle fontStyle = FontStyle.Normal;
		public int fontSize = 20;
		public Color color = Color.black;
	}
}}