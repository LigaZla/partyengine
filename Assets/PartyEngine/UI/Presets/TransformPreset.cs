﻿using UnityEngine;

namespace PartyEngine.UI.Presets {
[CreateAssetMenu(fileName = "TransformPreset", menuName = "UI.Presets/TransformPreset")]
public class TransformPreset : ScriptableObject
{
	public Vector3 position;
	public Vector3 rotation;
	public Vector3 localScale = new Vector3(1, 1, 1);
}}