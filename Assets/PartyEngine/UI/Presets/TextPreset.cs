﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PartyEngine.UI.Presets {
[CreateAssetMenu(fileName = "TextPreset", menuName = "UI.Presets/TextPreset")]
public class TextPreset : ScriptableObject
{
	public enum TextStyle
	{
		NONE,
		LOWERCASE,
		UPPERCASE
	} 
	
	[Header("Character")]
	public Font font;
	public FontStyle fontStyle = FontStyle.Normal;
	public TextStyle textStyle = TextStyle.NONE;
	public int fontSize = 20;
	public float lineSpacing = 1;
	public bool richText = true;
	[Header("Paragraf")]
	public TextAnchor alignment = TextAnchor.MiddleCenter;
	public bool alignByGeometry;
	public HorizontalWrapMode horizontalOverflow;
	public VerticalWrapMode verticalOverflow;
	[Header("BestFit")]
	public bool bestFit;
	public int minSize = 4;
	public int maxSize = 20;
	[Header("Other")]
	public Color color = Color.black;
	public Material material;
	public bool raycastTarget;
	[Header("Outline")]
	public bool outline;
	public Color outlineColor = Color.black;
	public Vector2 outlineSize = new Vector2(2, -2);
	public bool outlineUseGraphicAlpha = true;
}}