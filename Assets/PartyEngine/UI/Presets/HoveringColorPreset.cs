﻿using UnityEngine;

namespace PartyEngine.UI.Presets {
[CreateAssetMenu(fileName = "HoveringColorPreset", menuName = "UI.Presets/HoveringColorPreset")]
public class HoveringColorPreset : HoveringBasePreset<Color>
{

}}