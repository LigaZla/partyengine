﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PartyEngine.UI.Presets {
[CreateAssetMenu(fileName = "HoveringSpritePreset", menuName = "UI.Presets/HoveringSpritePreset")]
public class HoveringSpritePreset : HoveringBasePreset<Sprite>
{

}}