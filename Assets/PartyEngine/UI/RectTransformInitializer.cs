﻿using UnityEngine;
using PartyEngine.UI.Presets;

namespace PartyEngine.UI {
public class RectTransformInitializer : MonoBehaviour
{
	[SerializeField] RectTransformPreset preset;
	[SerializeField] bool applyTransform = true;
	[SerializeField] bool applySizeDelta = true;
	[SerializeField] bool applyAnchors = true;
	[SerializeField] bool applyPivot = true;

	void Awake()
	{
		var rect = GetComponent<RectTransform>();
		
		if(applyTransform)
		{
			rect.anchoredPosition = preset.position;
			rect.eulerAngles = preset.rotation;
			rect.localScale = preset.localScale;
		}
		
		if(applySizeDelta)
			rect.sizeDelta = preset.sizeDelta;
		
		if(applyAnchors)
		{
			rect.anchorMin = preset.anchorMin;
			rect.anchorMax = preset.anchorMax;
		}
		
		if(applyPivot)
			rect.pivot = preset.pivot;
		
		Destroy(this);
	}

}}