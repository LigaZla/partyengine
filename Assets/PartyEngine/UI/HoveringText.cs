﻿using UnityEngine;
using UnityEngine.UI;
using PartyEngine.UI.Presets;
using PartyEngine.Ext;

namespace PartyEngine.UI {
[RequireComponent(typeof(Hovering))]
public class HoveringText : HoveringBase<HoveringTextPreset, HoveringTextPreset.TextPreset>
{
	[SerializeField] Text text;
	[SerializeField] bool colorOnly = true;

	protected override void InitializeComponent()
	{
		if(text == null) text = GetComponent<Text>() ?? transform.GetFirstChild().GetComponent<Text>();
	}

	protected override void ApplyPreset(HoveringTextPreset.TextPreset preset)
	{
		text.color = preset.color;
		
		if(colorOnly) return;
		
		text.fontStyle = preset.fontStyle;
		text.fontSize = preset.fontSize;
	}		

}}