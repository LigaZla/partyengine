﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PartyEngine.UI.Presets;

namespace PartyEngine.UI {
[RequireComponent(typeof(Image))]
public class SpriteInitializer : MonoBehaviour
{
	[SerializeField] SpritePreset preset;

	void Awake()
	{
		var img = GetComponent<Image>();

		img.sprite = preset.sprite;
		img.color = preset.color;
		img.material = preset.material;
		img.raycastTarget = preset.raycastTarget;
		img.preserveAspect = preset.preserveAspect;
		
		img.type = preset.type;
		img.fillMethod = preset.fillMethod;
		
		if(img.type == Image.Type.Filled)
		{
			img.fillOrigin = GetFillOrigin(preset.fillMethod, preset);
		}
		
		img.fillAmount = preset.fillAmount;
		img.fillClockwise = preset.clockWise;
		img.fillCenter = preset.fillCenter;

		Destroy(this);
	}
	int GetFillOrigin(Image.FillMethod method, SpritePreset preset)
	{
		switch(method)
		{
			case Image.FillMethod.Horizontal : return (int)preset.originHorizontal;
			case Image.FillMethod.Vertical : return (int)preset.originVertical;
			case Image.FillMethod.Radial90 : return (int)preset.origin90;
			case Image.FillMethod.Radial180 : return (int)preset.origin180;
			case Image.FillMethod.Radial360 : return (int)preset.origin360;
		}
		
		Debugger.LogError("Fail to get FillOrigin");
		return 0;
	}
}}