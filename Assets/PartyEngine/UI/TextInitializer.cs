﻿using UnityEngine;
using UnityEngine.UI;
using PartyEngine.UI.Presets;
using PartyEngine.Ext;
using PartyEngine;

namespace PartyEngine.UI {
public class TextInitializer : MonoBehaviour
{
	[SerializeField] TextPreset preset;

	void Awake()
	{
		var text = GetComponent<Text>() ?? transform.GetFirstChild().GetComponent<Text>();
		
		text.font = preset.font;
		text.fontStyle = preset.fontStyle;
		text.fontSize = preset.fontSize;
		text.lineSpacing = preset.lineSpacing;
		text.supportRichText = preset.richText;
		
		text.alignment = preset.alignment;
		text.alignByGeometry = preset.alignByGeometry;
		text.horizontalOverflow = preset.horizontalOverflow;
		text.verticalOverflow = preset.verticalOverflow;
		
		text.resizeTextForBestFit = preset.bestFit;
		text.resizeTextMinSize = preset.minSize;
		text.resizeTextMaxSize = preset.maxSize;
		
		text.color = preset.color;
		text.material = preset.material;
		text.raycastTarget = preset.raycastTarget;
		
		if(preset.textStyle == TextPreset.TextStyle.LOWERCASE)
			text.text = text.text.ToLower();
		else if(preset.textStyle == TextPreset.TextStyle.UPPERCASE)
			text.text = text.text.ToUpper();
		
		Outline outline = null;
		if(preset.outline)
		{
			outline = text.GuarantedGetComponent<Outline>();
			outline.effectColor = preset.outlineColor;
			outline.effectDistance = preset.outlineSize;
			outline.useGraphicAlpha = preset.outlineUseGraphicAlpha;
		}
		else 
		{
			outline = text.GetComponent<Outline>();
			if(outline != null) Destroy(outline);
		}
		
		Destroy(this);
	}

}}