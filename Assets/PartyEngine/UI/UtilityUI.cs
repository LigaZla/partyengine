﻿using UnityEngine;
using UnityEngine.UI;

namespace PartyEngine.UI {
public static class UtilityUI
{
	public static readonly Vector2Int CANVAS_SIZE = new Vector2Int(1920, 1080);
	public static Vector2 MousePointToCanvasPoint()
	{
		return new Vector2(Input.mousePosition.x/Screen.width * CANVAS_SIZE.x, Input.mousePosition.y/Screen.height * CANVAS_SIZE.y);
	}
	
	public static Vector2 MousePointToCanvasPoint(RectTransform rect)
	{
		return new Vector2(Input.mousePosition.x/Screen.width * rect.sizeDelta.x, Input.mousePosition.y/Screen.height * rect.sizeDelta.y);
	}
	
	public static Vector2 MousePointToCanvasPoint(Vector2 mousePos, RectTransform rect)
	{
		return new Vector2(mousePos.x/Screen.width * rect.sizeDelta.x, mousePos.y/Screen.height * rect.sizeDelta.y);
	}
	
	public static void SpawnItemsToContainer<T, U>(this T[] items, GameObject prefab, Transform container, System.Action<T, U> initAction)
	{
		foreach (T item in items)
		{
			U obj = Object.Instantiate(prefab, container).GetComponent<U>();
			if(initAction!=null)initAction.Invoke(item, obj);
		}
	}
	
	#region UNITY BUGS
	/// <summary>
	///  Unity 2017.3.0: Dropdown canvas doesn't copy parent sortingLayer.
	/// </summary>
	public static void FixBug_DropdownList(UnityEngine.UI.Dropdown dropdown, string sortingLayerName)
	{
		var canvas = dropdown.GetComponentInChildren<Canvas>();
		if(canvas != null && canvas.sortingLayerName != sortingLayerName)
			canvas.sortingLayerName = sortingLayerName;
	}
	#endregion
}}