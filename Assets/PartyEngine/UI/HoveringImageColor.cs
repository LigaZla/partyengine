﻿using UnityEngine;
using UnityEngine.UI;
using PartyEngine.UI.Presets;

namespace PartyEngine.UI {
public class HoveringImageColor : HoveringBase<HoveringColorPreset, Color>
{
	[SerializeField] Image image;
	
	protected override void InitializeComponent()
	{
		if(image == null) image = GetComponent<Image>();
	}

	protected override void ApplyPreset(Color preset)
	{
		image.color = preset;
	}
	
}}