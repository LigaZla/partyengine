﻿using UnityEngine;
using PartyEngine.UI.Presets;

namespace PartyEngine.UI {
public abstract class HoveringBase : MonoBehaviour
{
	public abstract void Initialize();
	public abstract void ApplyPresetsOnEditor(bool disabledOnStart);
}
[RequireComponent(typeof(Hovering))]
public abstract class HoveringBase<P, T> : HoveringBase where P : HoveringBasePreset<T>
{
	[SerializeField] P preset;
	Hovering hovering;
	
	public override void Initialize()
	{
		InitializeComponent();
		hovering = this.GetHovering();
		
		hovering.OnSetNormal += ToNormal;
		hovering.OnSetHovered += ToHovered;
		hovering.OnSetPressed += ToPressed;
		hovering.OnSetDisabled += ToDisabled;
		hovering.OnSetSelected += ToSelected;
	}
	protected abstract void InitializeComponent();
	
	protected void ToNormal(){ ApplyPreset(preset.normal); }
	protected void ToHovered(){ ApplyPreset(preset.hovered); }
	protected void ToPressed(){ ApplyPreset(preset.pressed); }
	protected void ToDisabled(){ ApplyPreset(preset.disabled); }
	protected void ToSelected(){ ApplyPreset(preset.selected); }

	protected abstract void ApplyPreset(T preset);
	
	public override void ApplyPresetsOnEditor(bool disabledOnStart)
	{
		InitializeComponent();
		
		if(disabledOnStart) ToDisabled();
		else ToNormal();
	}
	
	void OnDestroy()
	{
		hovering.OnSetNormal -= ToNormal;
		hovering.OnSetHovered -= ToHovered;
		hovering.OnSetPressed -= ToPressed;
		hovering.OnSetDisabled -= ToDisabled;
		hovering.OnSetSelected -= ToSelected;
	}
}}