﻿using UnityEngine;

namespace PartyEngine {
public static class PartyInput 
{
	public static bool LeftMouseDown {get { return Input.GetKeyDown(KeyCode.Mouse0);}}
	public static bool RightMouseDown {get { return Input.GetKeyDown(KeyCode.Mouse1);}}
	public static bool MiddleMouseDown {get { return Input.GetKeyDown(KeyCode.Mouse2);}}
	
	public static bool LeftMouseUp {get { return Input.GetKeyUp(KeyCode.Mouse0);}}
	public static bool RightMouseUp {get { return Input.GetKeyUp(KeyCode.Mouse1);}}
	public static bool MiddleMouseUp {get { return Input.GetKeyUp(KeyCode.Mouse2);}}	
	
	public static bool LeftMouse {get { return Input.GetKey(KeyCode.Mouse0);}}
	public static bool RightMouse {get { return Input.GetKey(KeyCode.Mouse1);}}
	public static bool MiddleMouse {get { return Input.GetKey(KeyCode.Mouse2);}}
	
	public static float MouseScroll {get { return Input.GetAxis("Mouse ScrollWheel");}}
	
	
	public static bool LeftCtrlDown {get { return Input.GetKeyDown(KeyCode.LeftControl);}}
	public static bool LeftAltDown {get { return Input.GetKeyDown(KeyCode.LeftAlt);}}
	public static bool LeftShiftDown {get { return Input.GetKeyDown(KeyCode.LeftShift);}}
	
	public static bool LeftCtrlUp {get { return Input.GetKeyUp(KeyCode.LeftControl);}}
	public static bool LeftAltUp {get { return Input.GetKeyUp(KeyCode.LeftAlt);}}
	public static bool LeftShiftUp {get { return Input.GetKeyUp(KeyCode.LeftShift);}}
	
	public static bool LeftCtrl {get { return Input.GetKey(KeyCode.LeftControl);}}
	public static bool LeftAlt {get { return Input.GetKey(KeyCode.LeftAlt);}}
	public static bool LeftShift {get { return Input.GetKey(KeyCode.LeftShift);}}

	
	public static bool RightCtrlDown {get { return Input.GetKeyDown(KeyCode.RightControl);}}
	public static bool RightAltDown {get { return Input.GetKeyDown(KeyCode.RightAlt);}}
	public static bool RightShiftDown {get { return Input.GetKeyDown(KeyCode.RightShift);}}
	
	public static bool RightCtrlUp {get { return Input.GetKeyUp(KeyCode.RightControl);}}
	public static bool RightAltUp {get { return Input.GetKeyUp(KeyCode.RightAlt);}}
	public static bool RightShiftUp {get { return Input.GetKeyUp(KeyCode.RightShift);}}
	
	public static bool RightCtrl {get { return Input.GetKey(KeyCode.RightControl);}}
	public static bool RightAlt {get { return Input.GetKey(KeyCode.RightAlt);}}
	public static bool RightShift {get { return Input.GetKey(KeyCode.RightShift);}}
	
	
	public static bool F1Down {get { return Input.GetKeyDown(KeyCode.F1);}}
	public static bool F2Down {get { return Input.GetKeyDown(KeyCode.F2);}}
	public static bool F3Down {get { return Input.GetKeyDown(KeyCode.F3);}}
	public static bool F4Down {get { return Input.GetKeyDown(KeyCode.F4);}}
	public static bool F5Down {get { return Input.GetKeyDown(KeyCode.F5);}}
	public static bool F6Down {get { return Input.GetKeyDown(KeyCode.F6);}}
	public static bool F7Down {get { return Input.GetKeyDown(KeyCode.F7);}}
	public static bool F8Down {get { return Input.GetKeyDown(KeyCode.F8);}}
	public static bool F9Down {get { return Input.GetKeyDown(KeyCode.F9);}}
	public static bool F10Down {get { return Input.GetKeyDown(KeyCode.F10);}}
	public static bool F11Down {get { return Input.GetKeyDown(KeyCode.F11);}}
	public static bool F12Down {get { return Input.GetKeyDown(KeyCode.F12);}}
	
	public static bool Ctrl_A {get { return LeftCtrl && Input.GetKeyDown(KeyCode.A);}}
	public static bool Ctrl_C {get { return LeftCtrl && Input.GetKeyDown(KeyCode.C);}}
	public static bool Ctrl_V {get { return LeftCtrl && Input.GetKeyDown(KeyCode.V);}}
	public static bool Ctrl_D {get { return LeftCtrl && Input.GetKeyDown(KeyCode.D);}}
	public static bool Ctrl_X {get { return LeftCtrl && Input.GetKeyDown(KeyCode.X);}}
	public static bool Ctrl_Z {get { return LeftCtrl && Input.GetKeyDown(KeyCode.Z);}}
	public static bool Ctrl_Y {get { return LeftCtrl && Input.GetKeyDown(KeyCode.Y);}}
	
	public static bool DeleteDown {get { return Input.GetKeyDown(KeyCode.Delete);}}
	public static bool DeleteUp {get { return Input.GetKeyUp(KeyCode.Delete);}}
	public static bool Delete {get { return Input.GetKey(KeyCode.Delete);}}	
}}