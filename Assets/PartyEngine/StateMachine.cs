﻿using System;
using System.Collections.Generic;

namespace PartyEngine {
public class StateMachine
{
	Dictionary<ulong, Action> transitions = new Dictionary<ulong, Action>();
	
	public uint CurrentState { get; private set; }
	
	public void ClearTransitions()
	{
		transitions.Clear();
	}
	
	public void AddTransition(uint source, uint target, Action transition)
	{
		ValidateFlags(source, target);
		transitions.Add(Concatenate(source, target), transition);
	}

    public void ChangeState(uint target)
    {
        ValidateFlags(CurrentState, target);
        
        Action transition = null;
        bool allow = transitions.TryGetValue(Concatenate(CurrentState, target), out transition);

        if (allow)
        {
            CurrentState = target;
            transition.Invoke();
        }
        else
        {
            //Debugger.LogError(null, "Transition {0} > {1} ({2}) do not exists", State, target, State | target);
            Debugger.LogError("Transition "+GetStateName(CurrentState)+" => "+GetStateName(target)+" do not exists!");
        }
    }
    
    public bool InState(uint states)
    {
        if(states != 0 && CurrentState == 0) return false;
    	return CurrentState == (states & CurrentState);
    }

    public virtual string GetStateName(uint state)
    {
        return state.ToString();
    }

    void ValidateFlags(uint source, uint target)
    {
        if (!IsFlag(source)) throw new ArgumentException("Source state id must be zero or power of two");
        if (!IsFlag(target)) throw new ArgumentException("Target state id must be zero or power of two");
    }

    ulong Concatenate(uint first, uint second)
    {
        return first | ((ulong)second << 32);
    }

    bool IsFlag(ulong number)
    {
        return (number & (number - 1)) == 0;
    }
}}