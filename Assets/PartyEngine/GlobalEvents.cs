﻿using UnityEngine;
using UnityEngine.SceneManagement;
using PartyEngine.Actions;
using PartyEngine.Ext;

#pragma warning disable 0108
namespace PartyEngine {
public class GlobalEvents : MonoBehaviour 
{
	public static void Initialize()
	{
		if(instance != null)
		{
			Debugger.LogWarning("GlobalEvents already initialized");
			return;
		}
		
		var go = ObjectExt.CreateGameObject("GlobalEvents");
		go.AddComponent<DontDestroyOnLoad>();
		var comp = go.AddComponent<GlobalEvents>();
		
		instance = comp;
	}
	static GlobalEvents instance;
	
	/// <summary>Event called when the game launched. (On 'Start' before first frame)</summary>
	public static event System.Action OnStartGame;
	public static event System.Action OnEndGame;
	
	public static event System.Action<Scene> OnSceneLoaded;
	public static event System.Action<Scene> OnSceneUnLoaded;
	
	public static event System.Action OnUpdate;
	public static event System.Action OnFixedUpdate;
	public static event System.Action OnLateUpdate;
	
	/*
	public static event System.Action<Component, string> OnSendTextMessage;
	public static event System.Action<Component, object> OnSendMessage;
	*/
	
	void Awake()
	{
		if(instance != null)
		{
			Destroy(this);
			return;
		}
		else
			instance = this;

		SceneManager.sceneLoaded += WhenSceneLoaded;
		SceneManager.sceneUnloaded += WhenSceneUnLoaded;
	}
	
	void Start()
	{
		OnStartGame.SafetyInvoke();
	}
	
	void Update() 
	{
		OnUpdate.SafetyInvoke();
	}
	void FixedUpdate() 
	{
		OnFixedUpdate.SafetyInvoke();
	}
	void LateUpdate() 
	{
		OnLateUpdate.SafetyInvoke();
	}
	
	void WhenSceneLoaded(Scene scene, LoadSceneMode mode)
	{
		OnSceneLoaded.SafetyInvoke(scene);
	}
	
	void WhenSceneUnLoaded(Scene scene)
	{
		OnSceneUnLoaded.SafetyInvoke(scene);
	}
	
	void OnApplicaionQuit()
	{
		Exit();
		Destroy(this);
	}
	void Exit()
	{
		SceneManager.sceneLoaded -= WhenSceneLoaded;
		SceneManager.sceneUnloaded -= WhenSceneUnLoaded;
		
		OnEndGame.SafetyInvoke();
		
		OnStartGame = null;
		OnSceneLoaded = null;
		OnUpdate = null;
		OnFixedUpdate = null;
		OnLateUpdate = null;
		OnSceneUnLoaded = null;
		OnEndGame = null;
	}
}}