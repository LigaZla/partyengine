﻿using UnityEngine;

namespace PartyEngine {
public static class ResourcesSystem 
{
	#if !UNITY_EDITOR
	public static string DEFAULT_PATH { get { return Application.dataPath+"/../Resources/";}}
	#else
	public static string DEFAULT_PATH { get { return Application.dataPath+"/Resources/";}}
	#endif
	
	public static System.Xml.XmlDocument LoadDoc(System.Xml.XmlDocument doc, string path)
	{
		string diskPath = DEFAULT_PATH+path+".xml";
		try
		{
			if(System.IO.File.Exists(diskPath))
				doc.Load(diskPath);
			else
			{
				TextAsset textAsset = (TextAsset) Resources.Load(path);
				doc.LoadXml(textAsset.text);
			}
		}
		catch(System.Exception ex) 
		{
			Debug.LogException(ex);
		}
		
		return doc;
	}
	
	/// <summary>Return the text from file in disc or from resource</summary>
	/// <param name="path">Path to file in 'Resources' folder.</param>
	/// <param name="extension">file extension</param>
	/// <returns>text if loaded ok. else return ""</returns>
	public static string GetText(string path, string extension = ".txt")
	{
		string diskPath = DEFAULT_PATH+path+extension;
		try 
		{
			if(System.IO.File.Exists(diskPath))
			{
				System.IO.StreamReader textFile = new System.IO.StreamReader(diskPath);
				string text = textFile.ReadToEnd();
				textFile.Close();
				
				return text;
			}
			else
			{
				return (Resources.Load(path) as TextAsset).text;
			}
		} 
		catch(System.Exception ex) 
		{
			Debugger.LogError("Fail to load path = "+path+"\nError message: "+ex);
			//Debug.LogException(ex);
			return "";
		}
	}

}}