﻿using UnityEngine;
using UnityEngine.UI;

namespace PartyEngine.Console {
public class CommandLabel : MonoBehaviour 
{
	[SerializeField] Text commandField;
	string command;
	
	public void SetHelpCommand(string command)
	{
		SetCommand(command, Color.green);
	}
	public void SetCommand(string command)
	{
		SetCommand(command, Color.white);
	}
	void SetCommand(string command, Color color)
	{
		commandField.text = command;
		commandField.color = color;
		this.command = command;
	}
	
	public void SetErrorCommand(string command)
	{
		commandField.text = "Command '"+command+"' not found!";
		commandField.color = Color.red;
		this.command = command;
	}
	
	public void SetStatusText(string txt)
	{
		commandField.text = txt;
		commandField.color = Color.cyan;
	}
	
	public void OnClick()
	{
		if(!command.IsNullOrEmpty())
			Console.Instance.LoadCommand(command);
	}
}}