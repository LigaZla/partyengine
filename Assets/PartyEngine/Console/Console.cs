﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using PartyEngine.Actions;

namespace PartyEngine.Console {
public class Console : MonoBehaviour
{
	public static Console Instance {get; private set;}
	
	[SerializeField] InputField commandField;
	[SerializeField] PoolProperty poolProperty;
	[SerializeField] GameObject consoleObject;
	[SerializeField] Transform activedFieldsContainer;
	Pool<CommandLabel> pool;
	Dictionary<string, System.Action> actions = new Dictionary<string, System.Action>();
	List<string> enteredCommands = new List<string>();
	int lastCommandIndex;
	
	public bool Active { get { return consoleObject.activeInHierarchy;}}
	
	void Awake()
	{
		/*#if UNITY_EDITOR || DEVELOPMENT_BUILD
		#else
		Destroy(gameObject);
		#endif
		*/
		if(Instance != null)
		{
			Destroy(gameObject);
			return;
		}
		
		Instance = this;
		pool = new Pool<CommandLabel>(poolProperty);
		pool.Initialize();
		
		DontDestroyOnLoad(gameObject);
		
		RegisterCommand
		(
			ConsoleCommands.HELP,
			()=>ConsoleCommands.Commands.DoAction(x=>pool.Get(activedFieldsContainer).SetHelpCommand(x))
		);
		
		SetActive(false);
	}
	
	void Update()
	{
		if(UnityEngine.Input.GetKeyDown(KeyCode.BackQuote))
			SetActive(!Active);
			
		if(UnityEngine.Input.GetKeyDown(KeyCode.Return))
		{
			if(!string.IsNullOrEmpty(commandField.text))
				EnterCommand();
			
			SelectField();
		}
		
		if(UnityEngine.Input.GetKeyDown(KeyCode.UpArrow))
		{
			if(lastCommandIndex > 0)
				SetPreviousCommand(--lastCommandIndex);
		}
		
		if(UnityEngine.Input.GetKeyDown(KeyCode.DownArrow))
		{
			if(lastCommandIndex < enteredCommands.Count-1)
				SetPreviousCommand(++lastCommandIndex);
		}
	}
	
	void SetActive(bool active)
	{
		consoleObject.SetActive(active);
		commandField.text = "";
		if(active) SelectField();
	}
	
	public void RegisterCommand(string command, System.Action action)
	{
		if(!actions.ContainsKey(command))
		{
			actions.Add(command, action);
		}
		else
		{
			Debugger.LogWarning("Command '"+command+"' already exist!");
		}
	}
	
	void SetPreviousCommand(int index)
	{
		LoadCommand(enteredCommands[index]);
	}
	
	void EnterCommand()
	{
		string command = commandField.text;
		
		enteredCommands.Add(command);
		lastCommandIndex = enteredCommands.Count;
	
		if(actions.ContainsKey(command))
		{
			pool.Get(activedFieldsContainer).SetCommand(command);
			actions[command].Invoke();
		}
		else
			pool.Get(activedFieldsContainer).SetErrorCommand(command);
			
		commandField.text = "";
	}
	
	void SelectField()
	{
		EventSystem.current.SetSelectedGameObject(commandField.gameObject, null);
		commandField.OnPointerClick(new PointerEventData(EventSystem.current));
		ActionSystem.InvokeDelayed(()=>{commandField.caretPosition = commandField.text.Length;}, 0.1f);
	}
	
	public void LoadCommand(string command)
	{
		commandField.text = command;
	}
	
}}