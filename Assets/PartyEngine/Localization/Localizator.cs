﻿using System.Collections.Generic;
using System.Xml;
using System.Text.RegularExpressions;
using PartyEngine.Ext;

namespace PartyEngine.Localization {
public static class Localizator 
{
	public static string CurrentLanguage {get; private set;}	
	public static string DefaultLanguage { get { return Languages[0];}}
	public static string[] Languages {get; private set;}
	
	public static event System.Action OnChangeLanguage;
	
	static Dictionary<string, Dictionary<string, string>> texts;
	
	public static void LoadLocalization()
	{
		if(texts != null) return;

		XmlDocument doc = new XmlDocument();
		ResourcesSystem.LoadDoc(doc, "Localization/Strings");
		
		texts = new Dictionary<string, Dictionary<string, string>>();
		
		XmlAttributeCollection languages = doc.FirstChild.Attributes;
		XmlNodeList nodeList = doc.FirstChild.ChildNodes;
		// Get languages
		Languages = new string[languages.Count];
		for(int a=0; a<languages.Count; a++)
			Languages[a] = languages[a].Value;
		
		// Fill dick "key-lang-value"
		for(int i=0; i<nodeList.Count; i++)
		{
			XmlElement element = (XmlElement)nodeList[i];
			string key = element.GetAttribute("Key");
			
			if(texts.ContainsKey(key) || key.Length < 1) continue;
			
			Dictionary<string, string> txt = new Dictionary<string, string>();
			string defaultLang = Languages[0];
			foreach(string lang in Languages)
			{	
				if(element.HasAttribute(lang))
					txt.Add(lang, element.GetAttribute(lang));
				else
				{
					#if UNITY_EDITOR
					Debugger.LogWarning("404. Localization for lang '"+lang+"' with key '"+key+"' not found!");
					#endif
					txt.Add(lang, element.GetAttribute(defaultLang));
				}
			}
			
			texts.Add(key, txt);
		}
		
		ChangeLanguage(DefaultLanguage);
		
		doc = null;
	}
		
	public static void ChangeLanguage(string lang)
	{
		foreach(var element in Languages)
		{
			if(element == lang)
			{
				CurrentLanguage = lang;
				goto EXIT;
			}
		}
		
		Debugger.LogWarning("Change Language to lang '"+lang+"' is failed - not found. Language set as default.");
		CurrentLanguage = DefaultLanguage;
		
	EXIT:
		OnChangeLanguage.SafetyInvoke();
	}
	
	public static bool IsKeyExist(string key)
	{
		bool exist = texts.ContainsKey(key);
		
		if(!exist)
			Debugger.LogWarning("404. Localization key '"+key+"' not found");
		
		return exist;
	}
	
	public static bool IsKeyHasLang(string key, string lang)
	{
		bool existKey = IsKeyExist(key);

		if(existKey && !texts[key].ContainsKey(lang))
			Debugger.LogWarning("404. Localization language '"+lang+"' not found");
		
		return existKey && texts[key].ContainsKey(lang);
	}
	
	public static string GetText(string key)
	{
		return IsKeyHasLang(key, CurrentLanguage) ? texts[key][CurrentLanguage] : key;
	}
	
	public static string GetFormatedText(string key, params object[] args)
	{
		return string.Format(GetText(key), args);
	}
	public static string GetFormatedText(string key, params string[] args)
	{
		return string.Format(GetText(key), args);
	}
	
	public static string GetRegexText(string key, string symbol, string replace)
	{
		return Regex.Replace(GetText(key), "("+symbol+"\\.?)", replace);
	}

}}