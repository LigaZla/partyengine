﻿using UnityEngine;
using UnityEngine.UI;

namespace PartyEngine.Localization {
public class LocalizableTextLight : MonoBehaviour 
{
	[SerializeField] string key;
	[SerializeField] int textCompId;
	string Key { get { return key.ReplaceSymbols(" ", "");}}
	
	Text txt;
	
	void Awake()
	{
		Localizator.OnChangeLanguage += Localize;
		
		Prepare();
		Localize();
	}
	
	void Prepare()
	{
		txt = GetComponent<Text>();
		
		if(txt == null)
			txt = transform.GetChild(textCompId).GetComponent<Text>();
	}
	
	void Localize()
	{
		txt.text = Localizator.GetText(Key);
	}
	
	void OnDestroy()
	{
		Localizator.OnChangeLanguage -= Localize;
	}
}}