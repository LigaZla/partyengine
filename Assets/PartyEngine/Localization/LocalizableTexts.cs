﻿using UnityEngine;
using UnityEngine.UI;

namespace PartyEngine.Localization {
public class LocalizableTexts : MonoBehaviour 
{
	[System.Serializable]
	class KeyValue 
	{
		[SerializeField] string key = "";
		public GameObject textObject;
		public int textCompId = 0;
		
		public string Key { get { return key.ReplaceSymbols(" ", "");}}
	}
	
	[SerializeField] KeyValue[] labels;
	Text[] texts;
	
	void Awake()
	{
		Localizator.OnChangeLanguage += Localize;
		
		Prepare();
		Localize();
	}

	void Prepare()
	{
		texts = new Text[labels.Length];
		
		for(int i = 0; i < labels.Length; i++)
		{
			var label = labels[i];
			if(label.textObject == null) continue;
	            
	        texts[i] = label.textObject.GetComponent<Text>();
			
			if(texts[i] == null)
				texts[i] = label.textObject.transform.GetChild(label.textCompId).GetComponent<Text>();
		}
	}
	
	void Localize()
	{
		for(int i = 0; i < texts.Length; i++)
		{
	        if(texts[i] == null) continue;

			texts[i].text = Localizator.GetText(labels[i].Key);
		}
	}
	
	void OnDestroy()
	{
		Localizator.OnChangeLanguage -= Localize;
	}

}}