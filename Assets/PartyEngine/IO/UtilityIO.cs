﻿using System.Collections.Generic;
using UnityEngine;
using System.IO;

namespace PartyEngine.IO {
public static class UtilityIO 
{
	public static FileInfo[] GetAllFiles(string path, bool inSubfolders, string[] extensions = null)
	{
		DirectoryInfo dirInfo = new DirectoryInfo(path);
		if(extensions.IsNullOrEmpty())
			extensions = new []{""};
		
		List<FileInfo> elements = new List<FileInfo>();
		foreach(var ext in extensions)
		{
			elements.AddRange(
				dirInfo.GetFiles("*"+ext, inSubfolders ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly)
			);
		}
		
		return elements.ToArray();
	}
	
	public static DirectoryInfo[] GetAllDirectories(string path, bool inSubfolders)
	{
		return new DirectoryInfo(path).GetDirectories("*", inSubfolders ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly);
	}
	
	public static void SaveClassTo<T>(T data, string path, string extension=".txt")
	{
		SaveStringTo(JsonUtility.ToJson(data), path, extension);
	}
	public static void SaveStringTo(string text, string path, string extension=".txt")
	{
		try
		{
			StreamWriter textFile = new StreamWriter(path+extension);
			textFile.WriteLine(text);
			textFile.Close();
		}
		catch(System.Exception ex){Debug.LogException(ex);}
	}
	
	public static void SaveLinesTo(string[] lines, string path, string extension=".txt")
	{
		try
		{
			StreamWriter textFile = new StreamWriter(path+extension);
			
			foreach(var line in lines)
				textFile.WriteLine(line);
			
			textFile.Close();
		}
		catch(System.Exception ex){Debug.LogException(ex);}
	}
	
	public static T LoadClassFrom<T>(string path, string extension=".txt")
	{
		try
		{
			return JsonUtility.FromJson<T>(LoadStringFrom(path, extension));
		}
		catch(System.Exception ex)
		{
			Debug.LogException(ex);
			return default(T);
		}
	}
	public static string LoadStringFrom(string path, string extension=".txt")
	{
		try
		{
			StreamReader textFile = new StreamReader(path+extension);
			string text = textFile.ReadToEnd();
			textFile.Close();
			
			return text;
		}
		catch(System.Exception ex)
		{
			Debug.LogException(ex);
			return "";
		}
	}
	
	public static T SafetyFromJson<T>(string data)
	{
		if(string.IsNullOrEmpty(data) || data.Length < 5)// 5 - minimal parsed json length
		{
			Debugger.LogError("Json data was wrong!");
			return default(T);
		}
		
		try
		{
			return JsonUtility.FromJson<T>(data);
		}
		catch(System.Exception ex)
		{
			Debug.LogException(ex);
			return default(T);
		}
	}

	public static void DeleteFile(string path, string extension=".txt")
	{
		File.Delete(path+extension);
	}
	public static void DeleteDirectory(string path)
	{
		Directory.Delete(path);
	}
	
	public static bool ExistsFile(string path, string extension=".txt")
	{
		return File.Exists(path+extension);
	}
	public static bool ExistsDirectory(string path)
	{
		return Directory.Exists(path);
	}
	
	/// <summary> Create Directory if !exist</summary>
	public static void CheckDirectory(string path)
	{
		if(!Directory.Exists(path))
			Directory.CreateDirectory(path);
	}
}}