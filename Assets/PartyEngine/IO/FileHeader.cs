﻿
namespace PartyEngine.IO {
[System.Serializable]
public class FileHeader 
{
	public readonly string Name;
	public readonly string Extension;
	
	/// <summary>Name + extension</summary>
	public string FullName { get { return Name+Extension;}}
	public readonly string FullPath;
	
	/// <summary>Without name and extension</summary>
	public readonly string FolderPath;
	
	public FileHeader(System.IO.FileInfo fileInfo)
	{
		Name = fileInfo.Name.RemoveCharsFromEnd(fileInfo.Extension.Length);
		Extension = fileInfo.Extension;
		FullPath = fileInfo.FullName;
		FolderPath = fileInfo.DirectoryName;
	}
}}