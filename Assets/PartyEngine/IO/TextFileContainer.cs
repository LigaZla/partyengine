﻿using UnityEngine;

namespace PartyEngine.IO {
[System.Serializable]
public class TextFileContainer
{
	[SerializeField] TextAsset asset;
	[SerializeField] string filePath;
	
	public TextFileContainer(TextAsset asset)
	{
		this.asset = asset;
	}
	public TextFileContainer(string filePath)
	{
		this.filePath = filePath;
	}
	
	public string ExtractText()
	{
		if(filePath.IsNullOrEmpty()) return asset.text;
		
		if(System.IO.File.Exists(filePath))
		{
			System.IO.StreamReader textFile = new System.IO.StreamReader(filePath);
			string text = textFile.ReadToEnd();
			textFile.Close();
			
			return text;
		}
		else
		{
			Debugger.LogError("404. No found TextAsset and no file from filePath '"+filePath+"'");
			return "";
		}
	}
}}