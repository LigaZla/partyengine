﻿using UnityEngine;

namespace PartyEngine.IO {
[System.Serializable]
public abstract class AssetHeader
{
	public string Id { get { return id;}}
	public string FullPath { get { return assetPath+_name;}}
	public string AssetPath { get { return assetPath;}}
	public string Name { get { return _name;}}
	public string Extension { get { return extension;}}
	public string FullName { get { return _name+extension;}}
	public string Args { get { return args;}}

	[SerializeField] protected string id;
	[SerializeField] protected string assetPath;
	[SerializeField] protected string _name;
	[SerializeField] protected string args;
	[SerializeField] protected string extension;
}
[System.Serializable]
public abstract class AssetHeader<T> : AssetHeader
{
	public T Object { get { return _object;}}

	[SerializeField] T _object;
	
	public void SetUp(string name, string assetPath, string extension, T _object, string args="")
	{
		this._name = name;
		this.assetPath = assetPath;
		this.extension = extension;
		this._object = _object;
		this.args = args;
		this.id = assetPath + name + args;
	}
	
	public void SetObject(T _object)
	{
		this._object = _object;
	}
}
[System.Serializable]
public class SpriteHeader : AssetHeader<Sprite>
{

}

[System.Serializable]
public class PrefabHeader : AssetHeader<GameObject>
{

}

[System.Serializable]
public class AudioHeader : AssetHeader<AudioClip>
{

}

[System.Serializable]
public class DataHeader : AssetHeader<TextFileContainer>
{
	
}

}