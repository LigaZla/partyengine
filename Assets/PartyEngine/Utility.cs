﻿using UnityEngine;
using System.Collections;
using PartyEngine.Ext;

namespace PartyEngine {
public static class Utility 
{
	public static string BUILD_FOLDER_PATH { get { return Application.dataPath+"/../";}}
	public static float FPS { get { return 1 / Time.deltaTime;} }

	public static void CreateEventTriggerEntry(GameObject objWithTrigger, UnityEngine.EventSystems.EventTriggerType triggerType, System.Action action)
	{
		var eventTrigger = objWithTrigger.GuarantedGetComponent<UnityEngine.EventSystems.EventTrigger>();
		
		var entry = new UnityEngine.EventSystems.EventTrigger.Entry();
		entry.eventID = triggerType;
		entry.callback.AddListener(x => action.Invoke());
		eventTrigger.triggers.Add(entry);
	}
	
	public static string[] EnumToStrings<T>()
	{
		System.Array values = System.Enum.GetValues(typeof(T));
		string[] array = new string[values.Length];
		int i=0;
		foreach(var element in values)
			array[i++] = element.ToString();
		
		return array;
	}
	
	/// <summary>Used System.Activator to create an exemplar of type. Only empty constructor</summary>
	/// <returns>New object as 'T' if succes, or default(T) if fail</returns>
	public static T GetNewByType<T>()
	{
		try
		{
			return (T)System.Activator.CreateInstance(typeof(T));
		}
		catch(System.Exception ex)
		{
			Debug.LogException(ex);
			return default(T);
		}
	}
}}