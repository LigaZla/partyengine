﻿using UnityEngine;

namespace PartyEngine {
public static class PartyInitializer
{
	public static void Initialize()
	{
		GlobalEvents.Initialize();
		
		Localization.Localizator.LoadLocalization();
		
		InputSystem.UtilityInput.InitializeInputSystems();
	}
}}