﻿using System.Collections.Generic;
using System;

namespace PartyEngine {
public class SimpleStateMachine
{
	Dictionary<uint, Action> transitions = new Dictionary<uint, Action>();
	
	public uint CurrentState { get; private set; }
	
	public void ClearTransitions()
	{
		transitions.Clear();
	}
	
	public void AddTransition(uint state, Action transition)
	{
		ValidateFlags(state);
		transitions.Add(state, transition);
	}
	
	public void ChangeState(uint state)
	{
		ValidateFlags(state);
	
		CurrentState = state;
		transitions[state].Invoke();
	}
	
	public bool InState(uint state)
	{
		if(state != 0 && CurrentState == 0) return false;
		return CurrentState == (state & CurrentState);
	}
	
	public string StateName(uint state)
	{
		return state.ToString();
	}
	
	void ValidateFlags(uint state)
	{
		if(!IsFlag(state))
			throw new ArgumentException("State id must be zero or power of two");
	}
	
	bool IsFlag(ulong number)
	{
		return (number & (number - 1)) == 0;
	}
}}