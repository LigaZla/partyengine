﻿using System;
using System.Collections.Generic;
using UnityEngine;
using PartyEngine;

namespace PartyEngine.InputSystem.Buttons {
public static class ButtonsController
{
	readonly static Dictionary<InputKey, ButtonEventsContainer> btnsDick = new Dictionary<InputKey, ButtonEventsContainer>();

	public static void RegisterButton(InputKey[] keys, Action onDown, Action onPressed, Action onUp, TimeThread timeLayer = null)
	{
		foreach(var key in keys)
			RegisterButton(key, onDown, onPressed, onUp, timeLayer);
	}
	public static void RegisterButton(InputKey key, Action onDown, Action onPressed, Action onUp, TimeThread timeLayer = null)
	{
		GetButtonEventContainer(key, timeLayer).RegisterEvents(onDown, onPressed, onUp);
	}
	
	public static void UnregisterButton(InputKey[] keys, Action onDown, Action onPressed, Action onUp)
	{
		foreach(var key in keys)
			UnregisterButton(key, onDown, onPressed, onUp);
	}
	public static void UnregisterButton(InputKey key, Action onDown, Action onPressed, Action onUp)
	{
		GetButtonEventContainer(key).UnRegisterEvents(onDown, onPressed, onUp);
	}
	
	public static void UnregisterButton(InputKey key)
	{
		if(btnsDick.ContainsKey(key))
		{
			btnsDick[key].Close();
			btnsDick.Remove(key);
		}
	}
	
	static ButtonEventsContainer GetButtonEventContainer(InputKey key, TimeThread timeLayer = null)
	{
		if(btnsDick.ContainsKey(key))
			return btnsDick[key];
		else
		{
			BaseButton[] btns = new BaseButton[key.Keys.Length];
			for(int i = 0; i < btns.Length; i++)
				btns[i] = InputStorage.GetButton(key.Keys[i]);
			
			ButtonEventsContainer bec = new ButtonEventsContainer(btns, InputStorage.GetButton(key.FinalKey), timeLayer ?? TimeThread.Input);
			btnsDick.Add(key, bec);
			return bec;
		}
	}
	
	public static void SetPause(InputKey[] keys, bool paused)
	{
		foreach(var key in keys)
			SetPause(key, paused);
	}
	public static void SetPause(InputKey key, bool paused)
	{
		GetButtonEventContainer(key).SetPause(paused);
	}
	
	#region BUTTON AS AXIS
		
	/// <summary> Register button such as axis. </summary>
	/// <param name="positiveKey">Key equals 1</param>
	/// <param name="negativeKey">Key equals -1</param>
	public static void RegisterButtonAsAxis(InputKey positiveKey, InputKey negativeKey, Action<float> onDown, Action<float> onPressed, Action<float> onUp, TimeThread timeLayer = null)
	{
		GetButtonEventContainer(positiveKey, timeLayer).RegisterEvents(()=>onDown.Invoke(1), ()=>onPressed.Invoke(1), ()=>onUp.Invoke(1));
		GetButtonEventContainer(negativeKey, timeLayer).RegisterEvents(()=>onDown.Invoke(-1), ()=>onPressed.Invoke(-1), ()=>onUp.Invoke(-1));
	}
	
	/// <summary> Register button such as axis2D. </summary>
	/// <param name="positiveKeyX">Key equals 1 x</param>
	/// <param name="negativeKeyX">Key equals -1 x</param>
	/// /// <param name="positiveKeyY">Key equals 1 y</param>
	/// <param name="negativeKeyY">Key equals -1 y</param>
	public static void RegisterButtonAsAxis2D(
		InputKey positiveKeyX, InputKey negativeKeyX, InputKey positiveKeyY, InputKey negativeKeyY, 
		Action onDown, Action<Vector2> onPressed, Action onUp, 
		TimeThread timeLayer = null)
	{
		GetButtonEventContainer(positiveKeyX, timeLayer).RegisterEvents(onDown, ()=>onPressed.Invoke(Vector2.right), onUp);
		GetButtonEventContainer(negativeKeyX, timeLayer).RegisterEvents(onDown, ()=>onPressed.Invoke(Vector2.left), onUp);
		GetButtonEventContainer(positiveKeyY, timeLayer).RegisterEvents(onDown, ()=>onPressed.Invoke(Vector2.up), onUp);
		GetButtonEventContainer(negativeKeyY, timeLayer).RegisterEvents(onDown, ()=>onPressed.Invoke(Vector2.down), onUp);
		
		GetButtonEventContainer(new InputKey(positiveKeyX, positiveKeyY), timeLayer).RegisterEvents(onDown, ()=>onPressed.Invoke(new Vector2(1, 1)), onUp); // UpRight
		GetButtonEventContainer(new InputKey(negativeKeyX, positiveKeyY), timeLayer).RegisterEvents(onDown, ()=>onPressed.Invoke(new Vector2(-1, 1)), onUp); // UpLeft
		GetButtonEventContainer(new InputKey(positiveKeyX, negativeKeyY), timeLayer).RegisterEvents(onDown, ()=>onPressed.Invoke(new Vector2(1, -1)), onUp); // DownRight
		GetButtonEventContainer(new InputKey(negativeKeyX, negativeKeyY), timeLayer).RegisterEvents(onDown, ()=>onPressed.Invoke(new Vector2(-1, -1)), onUp); // DownLeft
	}
	
	public static void UnRegisterButtonAsAxis(InputKey positiveKey, InputKey negativeKey)
	{
		UnregisterButton(positiveKey);
		UnregisterButton(negativeKey);
	}
	public static void UnRegisterButtonAsAxis2D(InputKey positiveKeyX, InputKey negativeKeyX, InputKey positiveKeyY, InputKey negativeKeyY)
	{
		UnregisterButton(positiveKeyX);
		UnregisterButton(negativeKeyX);
		UnregisterButton(positiveKeyY);
		UnregisterButton(negativeKeyY);
		
		UnregisterButton(new InputKey(positiveKeyX, positiveKeyY)); // UpRight
		UnregisterButton(new InputKey(negativeKeyX, positiveKeyY)); // UpLeft
		UnregisterButton(new InputKey(positiveKeyX, negativeKeyY)); // DownRight
		UnregisterButton(new InputKey(negativeKeyX, negativeKeyY)); // DownLeft
	}
	#endregion
	
}}