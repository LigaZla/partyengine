﻿using PartyEngine;
using PartyEngine.Ext;
using System;

namespace PartyEngine.InputSystem.Buttons {
public class ButtonEventsContainer : InputEventsContainer
{
	readonly BaseButton[] buttons;
	readonly BaseButton finalButton;
	
	event Action onDown;
	event Action onPressed;
	event Action onUp;
	
	public ButtonEventsContainer(BaseButton[] buttons, BaseButton finalButton, TimeThread timeLayer):base(timeLayer)
	{
		this.buttons = buttons.ToEmptyIfNull();
		this.finalButton = finalButton;
	}

	public void RegisterEvents(Action onDown, Action onPressed, Action onUp)
	{
		if(onDown != null) this.onDown += onDown;
		if(onPressed != null) this.onPressed += onPressed;
		if(onUp != null) this.onUp += onUp;
	}

	public void UnRegisterEvents(Action onDown, Action onPressed, Action onUp)
	{
		if(onDown != null) this.onDown -= onDown;
		if(onPressed != null) this.onPressed -= onPressed;
		if(onUp != null) this.onUp -= onUp;
	}
	
	public override void Update()
	{
		foreach(var element in buttons)
		{
			if(!element.Pressed())
				return;
		}
		
		if(finalButton.Down()) onDown.SafetyInvoke();
		if(finalButton.Pressed()) onPressed.SafetyInvoke();
		if(finalButton.Up()) onUp.SafetyInvoke();		
	}
	/*
	/// <summary>
	/// Return key with which you can stored this class to dictionary
	/// </summary>
	/// <returns></returns>
	public string GetDictionaryKey()
	{
		System.Text.StringBuilder builder = new System.Text.StringBuilder();
		foreach(var element in buttons)
			builder.Append(element);
		
		builder.Append(finalButton);
		
		return builder.ToString();
	}*/
}}