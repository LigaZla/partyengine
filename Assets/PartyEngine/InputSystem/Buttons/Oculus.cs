﻿
namespace PartyEngine.InputSystem.Buttons {
public static class Oculus
{
	/*public class OculusButton : BaseButton
	{
		readonly OVRInput.Button btn;
		readonly OVRInput.Controller controller;
		
		public OculusButton(string key, OVRInput.Button btn, OVRInput.Controller controller)
		{
			Key = key;
			this.btn = btn;
			this.controller = controller;
		}

		public override bool Down()
		{
			return OVRInput.GetDown(btn, controller);
		}
		
		public override bool Press()
		{
			return OVRInput.Get(btn, controller);
		}
		
		public override bool Up()
		{
			return OVRInput.GetUp(btn, controller);
		}	
	}
	
	const string HEADER = "OculusButton_";
	
	public static OculusButton[] Buttons { get { return buttons;}}
	
	static readonly OculusButton[] buttons = 
	{
		new OculusButton(HEADER+"None", OVRInput.Button.None, OVRInput.Controller.None),
		
		new OculusButton(HEADER+"Any", OVRInput.Button.Any, OVRInput.Controller.All),
		new OculusButton(HEADER+"AnyL", OVRInput.Button.Any, OVRInput.Controller.LTouch),
		new OculusButton(HEADER+"AnyR", OVRInput.Button.Any, OVRInput.Controller.RTouch),

		new OculusButton(HEADER+"X", OVRInput.Button.One, OVRInput.Controller.LTouch),
		new OculusButton(HEADER+"Y", OVRInput.Button.Two, OVRInput.Controller.LTouch),
		new OculusButton(HEADER+"A", OVRInput.Button.One, OVRInput.Controller.RTouch),
		new OculusButton(HEADER+"B", OVRInput.Button.Two, OVRInput.Controller.RTouch),
		
		new OculusButton(HEADER+"LTrigger", OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.LTouch),
		new OculusButton(HEADER+"RTrigger", OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.RTouch),
		
		new OculusButton(HEADER+"LHand", OVRInput.Button.One, OVRInput.Controller.LTouch),
		new OculusButton(HEADER+"RHand", OVRInput.Button.One, OVRInput.Controller.RTouch),
		
		new OculusButton(HEADER+"LStart", OVRInput.Button.Start, OVRInput.Controller.LTouch),
		
		// Thumbstick
		new OculusButton(HEADER+"LStick", OVRInput.Button.PrimaryThumbstick, OVRInput.Controller.LTouch),
		new OculusButton(HEADER+"LStickUp", OVRInput.Button.PrimaryThumbstickUp, OVRInput.Controller.LTouch),
		new OculusButton(HEADER+"LStickDown", OVRInput.Button.PrimaryThumbstickDown, OVRInput.Controller.LTouch),
		new OculusButton(HEADER+"LStickLeft", OVRInput.Button.PrimaryThumbstickLeft, OVRInput.Controller.LTouch),
		new OculusButton(HEADER+"LStickRight", OVRInput.Button.PrimaryThumbstickRight, OVRInput.Controller.LTouch),
		
		new OculusButton(HEADER+"RStick", OVRInput.Button.PrimaryThumbstick, OVRInput.Controller.RTouch),
		new OculusButton(HEADER+"RStickUp", OVRInput.Button.PrimaryThumbstickUp, OVRInput.Controller.RTouch),
		new OculusButton(HEADER+"RStickDown", OVRInput.Button.PrimaryThumbstickDown, OVRInput.Controller.RTouch),
		new OculusButton(HEADER+"RStickLeft", OVRInput.Button.PrimaryThumbstickLeft, OVRInput.Controller.RTouch),
		new OculusButton(HEADER+"RStickRight", OVRInput.Button.PrimaryThumbstickRight, OVRInput.Controller.RTouch)
	};
	*/
}}