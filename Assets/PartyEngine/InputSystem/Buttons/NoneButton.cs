﻿
namespace PartyEngine.InputSystem.Buttons {
public class NoneButton : BaseButton
{
	public NoneButton()
	{
		Key = "NoneButton";
	}
	
	public override bool Down(){ return false;}
	public override bool Pressed(){ return false;}
	public override bool Up(){ return false;}
	
}}