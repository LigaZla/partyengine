﻿using UnityEngine;

namespace PartyEngine.InputSystem.Buttons {
public static class Default
{
	public class Button : BaseButton
	{
		public readonly KeyCode ButtonKeyCode;
		
		public Button(string key, KeyCode btn)
		{
			Key = key;
			this.ButtonKeyCode = btn;
		}

		public override bool Down()
		{
			return Input.GetKeyDown(ButtonKeyCode);
		}
		
		public override bool Pressed()
		{
			return Input.GetKey(ButtonKeyCode);
		}
		
		public override bool Up()
		{
			return Input.GetKeyUp(ButtonKeyCode);
		}	
	}

	// Prefomance hack
	static readonly Button[] buttons = Gets(buttons);
	static Button[] Gets(Button[] btns)
	{
		var keys = System.Enum.GetValues(typeof(KeyCode));
		btns = new Button[keys.Length];
		
		int i = 0;
		foreach(KeyCode code in keys)
		{
			btns[i++] = new Button(code.ToString(), code);
		}
		
		return btns;
	}
	
	static bool initialized;
	public static void Initialize()
	{
		if(initialized)
		{
			Debugger.LogWarning("InputSystem.Buttons.Default already initialized!");
			return;
		}
		
		for(int i = 0; i < buttons.Length; i++)
			InputStorage.AddInput(buttons[i]);
		
		initialized = true;
	}
	
}}