﻿
namespace PartyEngine.InputSystem.Buttons {
public abstract class BaseButton : BaseInput
{
	public abstract bool Down();
	public abstract bool Pressed();
	public abstract bool Up();
	
}}