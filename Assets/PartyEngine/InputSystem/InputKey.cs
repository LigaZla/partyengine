﻿using System.Collections.Generic;
using UnityEngine;
using PartyEngine;

namespace PartyEngine.InputSystem {
[System.Serializable]
public struct InputKey
{
	public string[] Keys { get { return keys;}}
	public string FinalKey { get { return finalKey;}}
	
	[SerializeField] string[] keys;
	[SerializeField] string finalKey;
	
	public InputKey(string finalKey)
	{
		this.keys = new string[0];
		this.finalKey = finalKey;
	}
	
	public InputKey(string[] keys, string finalKey)
	{
		this.keys = keys;
		this.finalKey = finalKey;
	}
	
	public InputKey(params string[] keys)
	{
		if(keys.Length < 2)
		{
			this.keys = new string[0];
			this.finalKey = keys[1];
		}
		else 
		{
			this.finalKey = keys.GetLastElement();
			this.keys = keys.RemoveLastElement();
		}
	}
	
	public InputKey(InputKey inputKey)
	{
		this.keys = inputKey.Keys;
		this.finalKey = inputKey.FinalKey;
	}
	
	public InputKey(params InputKey[] inputKeys)
	{
		List<string> keyList = new List<string>();
		for (int i = 0; i < inputKeys.Length; i++)
		{
			keyList.AddRange(inputKeys[i].Keys);
			keyList.Add(inputKeys[i].FinalKey);
		}
		
		this.finalKey = keyList.GetLastElement();
		this.keys = keyList.ToArray().RemoveLastElement();
	}
	
	public static explicit operator InputKey(string key)
	{
		return new InputKey(key);
	}
	
	public static bool operator==(InputKey p1, InputKey p2)
	{
		return p1.Equals(p2);
	}
	public static bool operator!=(InputKey p1, InputKey p2)
	{
		return !p1.Equals(p2);
	}
	
	public override bool Equals(object obj)
	{
		if (obj is InputKey)
			return Equals((InputKey)obj);
		else
			return false;
	}
	public bool Equals(InputKey p2)
	{
		keys.ToEmptyIfNull();
		p2.keys.ToEmptyIfNull();
		if(keys.Length != p2.keys.Length) return false;
		
		for(int i=0; i<keys.Length; i++)
			if(keys[i] != p2.keys[i])
				return false;
		
		return finalKey == p2.finalKey;
	}
	
	public override int GetHashCode()
	{
		return finalKey.GetHashCode();
	}
	
}}