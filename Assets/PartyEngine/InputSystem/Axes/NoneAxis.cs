﻿
namespace PartyEngine.InputSystem.Axes {
public class NoneAxis : BaseAxis
{
	public NoneAxis()
	{
		Key = "NoneAxis";
	}
	
	public override bool InZero()
	{
		return true;
	}
	
	public override float Get()
	{
		return 0;
	}
}
public class NoneAxis2D : BaseAxis2D
{
	public NoneAxis2D()
	{
		Key = "NoneAxis2D";
	}
	
	public override bool InZero()
	{
		return true;
	}
	
	public override UnityEngine.Vector2 Get()
	{
		return UnityEngine.Vector2.zero;
	}
}

}