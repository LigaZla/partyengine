﻿using System;
using System.Collections.Generic;
using UnityEngine;
using PartyEngine;
using PartyEngine.InputSystem;

namespace PartyEngine.InputSystem.Axes {
public static class AxesController
{
	readonly static Dictionary<InputKey, AxisEventsContainerBase> axesDick = new Dictionary<InputKey, AxisEventsContainerBase>();
	
	/// <summary></summary>
	/// <param name="onOutsideDeadZone">When axis stay in active zone</param>
	/// <param name="onExitDeadZone">When axis enter to active zone</param>
	/// <param name="onEnterDeadZone">When axis exit from active zone</param>
	/// <param name="deadZone">Zone in which no call</param>
	public static void RegisterAxis(InputKey key, Action<float> onOutsideDeadZone, Action<float> onExitDeadZone = null, Action<float> onEnterDeadZone = null, float deadZone = 0, TimeThread timeLayer = null)
	{
		GetAxisEventContainer(key, deadZone, timeLayer).RegisterEvents(onOutsideDeadZone, onExitDeadZone, onEnterDeadZone);
	}
	public static void RegisterAxis2D(InputKey key, Action<Vector2> onOutsideDeadZone, Action<Vector2> onExitDeadZone = null, Action<Vector2> onEnterDeadZone = null, Vector2 deadZone = default(Vector2), TimeThread timeLayer = null)
	{
		GetAxis2DEventContainer(key, deadZone, timeLayer).RegisterEvents(onOutsideDeadZone, onExitDeadZone, onEnterDeadZone);
	}
	
	public static void UnregisterAxis(InputKey key, Action<float> onOutsideDeadZone, Action<float> onExitDeadZone = null, Action<float> onEnterDeadZone = null)
	{
		GetAxisEventContainer(key).UnRegisterEvents(onOutsideDeadZone, onExitDeadZone, onEnterDeadZone);
	}
	public static void UnregisterAxis2D(InputKey key, Action<Vector2> onOutsideDeadZone, Action<Vector2> onExitDeadZone = null, Action<Vector2> onEnterDeadZone = null)
	{
		GetAxis2DEventContainer(key, Vector2.zero).UnRegisterEvents(onOutsideDeadZone, onExitDeadZone, onEnterDeadZone);
	}
	
	public static void UnregisterAxis(InputKey key)
	{
		if(axesDick.ContainsKey(key))
		{
			axesDick[key].Close();
			axesDick.Remove(key);
		}
	}

	public static void SetPause(InputKey[] keys, bool paused)
	{
		foreach(var key in keys)
			SetPause(key, paused);
	}
	public static void SetPause(InputKey key, bool paused)
	{
		if(axesDick.ContainsKey(key))
			axesDick[key].SetPause(paused);
		else
		{
			Debugger.LogWarning("404. Can not pause axis '"+key.FinalKey+"'! Axis not found!");
		}
	}
	
	static AxisEventsContainer GetAxisEventContainer(InputKey key, float deadZone = 0, TimeThread timeLayer = null)
	{
		if(axesDick.ContainsKey(key))
			return axesDick[key] as AxisEventsContainer;
		else
		{
			var aec = new AxisEventsContainer(InputStorage.GetAxis(key.FinalKey), deadZone, timeLayer ?? TimeThread.Input);
			axesDick.Add(key, aec);
			return aec;
		}
	}
	static AxisEventsContainer2D GetAxis2DEventContainer(InputKey key, Vector2 deadZone, TimeThread timeLayer = null)
	{
		if(axesDick.ContainsKey(key))
			return axesDick[key] as AxisEventsContainer2D;
		else
		{
			var aec = new AxisEventsContainer2D(InputStorage.GetAxis2D(key.FinalKey), deadZone, timeLayer ?? TimeThread.Input);
			axesDick.Add(key, aec);
			return aec;
		}
	}
}}