﻿using System;
using PartyEngine.Actions;
using PartyEngine.Types;
using PartyEngine.Ext;

namespace PartyEngine.InputSystem.Axes
{

	public abstract class AxisEventsContainerBase : InputEventsContainer
	{
		protected AxisEventsContainerBase(TimeThread timeLayer) : base(timeLayer)
		{
		}
	}

	public abstract class AxisEventsContainer<T> : AxisEventsContainerBase
	{
		public readonly BaseAxis<T> Axis;
		public readonly T DeadZone;
		public bool OutsideDeadZone { get; protected set; }

		protected readonly CustomEvent<T> onOutsideDeadZone = new CustomEvent<T>();
		protected readonly CustomEvent<T> onExitDeadZone = new CustomEvent<T>();
		protected readonly CustomEvent<T> onEnterDeadZone = new CustomEvent<T>();

		protected AxisEventsContainer(BaseAxis<T> axis, T deadZone, TimeThread timeLayer) : base(timeLayer)
		{
			Axis = axis;
			DeadZone = deadZone;
			OutsideDeadZone = false;
		}

		public void RegisterEvents(Action<T> onOutsideDeadZone, Action<T> onExitDeadZone, Action<T> onEnterDeadZone)
		{
			if (onOutsideDeadZone != null) this.onOutsideDeadZone.Add(onOutsideDeadZone);
			if (onExitDeadZone != null) this.onExitDeadZone.Add(onExitDeadZone);
			if (onEnterDeadZone != null) this.onEnterDeadZone.Add(onEnterDeadZone);
		}

		public void UnRegisterEvents(Action<T> onOutsideDeadZone, Action<T> onExitDeadZone, Action<T> onEnterDeadZone)
		{
			if (onOutsideDeadZone != null) this.onOutsideDeadZone.Remove(onOutsideDeadZone);
			if (onExitDeadZone != null) this.onExitDeadZone.Remove(onExitDeadZone);
			if (onEnterDeadZone != null) this.onEnterDeadZone.Remove(onEnterDeadZone);
		}

		protected T axisValue;
		public override void Update()
		{
			axisValue = Axis.Get();

			if (!InDeadZone() && !OutsideDeadZone)
			{
				OutsideDeadZone = true;
				onExitDeadZone.Invoke(axisValue);
			}

			if (!InDeadZone())
				onOutsideDeadZone.Invoke(axisValue);

			if (InDeadZone() && OutsideDeadZone)
			{
				OutsideDeadZone = false;
				onEnterDeadZone.Invoke(axisValue);
			}
		}

		protected abstract bool InDeadZone();

	}

	public class AxisEventsContainer : AxisEventsContainer<float>
	{
		public AxisEventsContainer(BaseAxis axis, float deadZone, TimeThread timeLayer) : base(axis, deadZone, timeLayer)
		{
		}

		protected override bool InDeadZone()
		{
			return axisValue.Between(-DeadZone, DeadZone);
		}
	}

	public class AxisEventsContainer2D : AxisEventsContainer<UnityEngine.Vector2>
	{
		public AxisEventsContainer2D(BaseAxis2D axis, UnityEngine.Vector2 deadZone, TimeThread timeLayer) : base(axis, deadZone, timeLayer)
		{
		}

		protected override bool InDeadZone()
		{
			return axisValue.InSquare(-DeadZone, DeadZone);
		}
	}

}