﻿using UnityEngine;

namespace PartyEngine.InputSystem.Axes {
public class Axis2D : BaseAxis2D
{
	string inputKeyX;
	string inputKeyY;
	
	public Axis2D(string key, string inputKeyX, string inputKeyY)
	{
		this.Key = key;
		this.inputKeyX = inputKeyX;
		this.inputKeyY = inputKeyY;
	}
	
	public override bool InZero()
	{
		return Get() == Vector2.zero;
	}
	
	public override Vector2 Get()
	{
		return new Vector2(Input.GetAxis(inputKeyX), Input.GetAxis(inputKeyY));
	}
	
}}