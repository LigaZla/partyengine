﻿

namespace PartyEngine.InputSystem.Axes {
public static class Oculus
{
	/*public class OculusAxis : Axis<float>
	{
		readonly OVRInput.Axis1D axis;
		readonly OVRInput.Controller controller;
		
		public OculusAxis(string key, OVRInput.Axis1D axis, OVRInput.Controller controller)
		{
			this.Key = key;
			this.axis = axis;
			this.controller = controller;
		}
		
		public override bool InZero()
		{
			return Get() == 0;
		}
		
		public override float Get()
		{
			return OVRInput.Get(axis, controller);
		}
		
	}
	
	public class OculusAxisX : Axis<float>
	{
		readonly OVRInput.Axis2D axis;
		readonly OVRInput.Controller controller;
		
		public OculusAxisX(string key, OVRInput.Axis2D axis, OVRInput.Controller controller)
		{
			this.Key = key;
			this.axis = axis;
			this.controller = controller;
		}
		
		public override bool InZero()
		{
			return Get() == 0;
		}
		
		public override float Get()
		{
			return OVRInput.Get(axis, controller).x;
		}
	}

	public class OculusAxisY : Axis<float>
	{
		readonly OVRInput.Axis2D axis;
		readonly OVRInput.Controller controller;
		
		public OculusAxisY(string key, OVRInput.Axis2D axis, OVRInput.Controller controller)
		{
			this.Key = key;
			this.axis = axis;
			this.controller = controller;
		}
		
		public override bool InZero()
		{
			return Get() == 0;
		}
		
		public override float Get()
		{
			return OVRInput.Get(axis, controller).y;
		}
	}
	
	public class OculusAxisXY : Axis<UnityEngine.Vector2>
	{
		readonly OVRInput.Axis2D axis;
		readonly OVRInput.Controller controller;
		
		public OculusAxisXY(string key, OVRInput.Axis2D axis, OVRInput.Controller controller)
		{
			this.Key = key;
			this.axis = axis;
			this.controller = controller;
		}
		
		public override bool InZero()
		{
			return Get().x == 0 && Get().y == 0;
		}
		
		public override UnityEngine.Vector2 Get()
		{
			return OVRInput.Get(axis, controller);
		}
	}
	
	const string HEADER = "OculusAxis_";
	
	public static readonly OculusAxis None = new OculusAxis(HEADER+"None",OVRInput.Axis1D.None, OVRInput.Controller.None);
	
	public static BaseAxis[] Axes { get { return axes;}}
	
	static readonly BaseAxis[] axes = 
	{
		new OculusAxis(HEADER+"Any", OVRInput.Axis1D.Any, OVRInput.Controller.All),
		new OculusAxis(HEADER+"LAny", OVRInput.Axis1D.Any, OVRInput.Controller.LTouch),
		new OculusAxis(HEADER+"RAny", OVRInput.Axis1D.Any, OVRInput.Controller.RTouch),
		
		new OculusAxis(HEADER+"LTrigger", OVRInput.Axis1D.PrimaryIndexTrigger, OVRInput.Controller.LTouch),
		new OculusAxis(HEADER+"RTrigger", OVRInput.Axis1D.PrimaryIndexTrigger, OVRInput.Controller.RTouch),
		
		new OculusAxis(HEADER+"LHand", OVRInput.Axis1D.PrimaryHandTrigger, OVRInput.Controller.LTouch),
		new OculusAxis(HEADER+"RHand", OVRInput.Axis1D.PrimaryHandTrigger, OVRInput.Controller.RTouch),
		
		// Thumbstick
		new OculusAxisX(HEADER+"LStickX", OVRInput.Axis2D.PrimaryThumbstick, OVRInput.Controller.LTouch),
		new OculusAxisY(HEADER+"LStickY", OVRInput.Axis2D.PrimaryThumbstick, OVRInput.Controller.LTouch),
		
		new OculusAxisX(HEADER+"RStickX", OVRInput.Axis2D.PrimaryThumbstick, OVRInput.Controller.RTouch),
		new OculusAxisY(HEADER+"RStickY", OVRInput.Axis2D.PrimaryThumbstick, OVRInput.Controller.RTouch),
			
		new OculusAxisXY(HEADER+"LStickXY", OVRInput.Axis2D.PrimaryThumbstick, OVRInput.Controller.LTouch),
		new OculusAxisXY(HEADER+"RStickXY", OVRInput.Axis2D.PrimaryThumbstick, OVRInput.Controller.RTouch)
	};
	*/
}}