﻿
namespace PartyEngine.InputSystem.Axes {
public abstract class BaseAxis<T> : BaseInput
{
	public abstract bool InZero();	
	public abstract T Get();
}

public abstract class BaseAxis : BaseAxis<float>
{
}

public abstract class BaseAxis2D : BaseAxis<UnityEngine.Vector2>
{
}

}