﻿using UnityEngine;

namespace PartyEngine.InputSystem.Axes {
public class Axis : BaseAxis
{
	string inputKey;
	
	public Axis(string key, string inputKey)
	{
		this.Key = key;
		this.inputKey = inputKey;
	}
	
	public override bool InZero()
	{
		return Get() == 0;
	}
	
	public override float Get()
	{
		return Input.GetAxis(inputKey);
	}	
}}