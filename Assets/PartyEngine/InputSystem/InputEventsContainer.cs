﻿using PartyEngine;
using System;

namespace PartyEngine.InputSystem {
public abstract class InputEventsContainer
{
	public bool Paused { get; private set; }
	
	protected readonly TimeThread timeLayer;
	
	protected InputEventsContainer(TimeThread timeLayer)
	{
		this.timeLayer = timeLayer;
		Subscribe();
	}
	void Subscribe()
	{
		timeLayer.Subscribe(Update);
	}
	
 	public void SetPause(bool paused)
	{
		if(Paused == paused)
		{
			#if UNITY_EDITOR
			Debugger.LogWarning("You try to double pause. Curr = '"+paused+"'");
			#endif
			return;
		}
 		
 		Paused = paused;
 		
 		if(paused) Unsubscribe();
 		else Subscribe();
	}
 	
	public abstract void Update();
	
	public void Close()
	{
		Unsubscribe();
	}
	void Unsubscribe()
	{
		timeLayer.Unsubscribe(Update);
	}
}}