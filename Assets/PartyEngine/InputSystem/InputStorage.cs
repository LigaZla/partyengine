﻿using System.Collections.Generic;
using PartyEngine;
using PartyEngine.Ext;

namespace PartyEngine.InputSystem {
public static class InputStorage
{
	public readonly static Buttons.NoneButton BUTTON_NONE = new Buttons.NoneButton();
	public readonly static Axes.NoneAxis AXIS_NONE = new Axes.NoneAxis();
	public readonly static Axes.NoneAxis2D AXIS_NONE_2D = new Axes.NoneAxis2D();

	static Dictionary<string, BaseInput> inputDick = new Dictionary<string, BaseInput>();
	
	public static void AddInput(BaseInput input)
	{
		if(!inputDick.ContainsKey(input.Key))
			inputDick.Add(input.Key, input);
		//else
			//Debugger.LogWarning("Input with key '"+input.Key+"' already exist in dick.", null);
	}
	
	public static Buttons.BaseButton GetButton(string key)
	{
		if(inputDick.ContainsKey(key))
			return inputDick[key] as Buttons.BaseButton;
		
		Debugger.LogWarning("Button with key '"+key+"' not found! Function return 'none' button", null);
		return BUTTON_NONE;
	}
	
	public static Axes.BaseAxis GetAxis(string key)
	{
		if(inputDick.ContainsKey(key))
			return inputDick[key] as Axes.BaseAxis;
		
		Debugger.LogWarning("Axis with key '"+key+"' not found! Function return 'none' Axis", null);
		return AXIS_NONE;
	}
	public static Axes.BaseAxis2D GetAxis2D(string key)
	{
		if(inputDick.ContainsKey(key))
			return inputDick[key] as Axes.BaseAxis2D;
		
		Debugger.LogWarning("Axis with key '"+key+"' not found! Function return 'none' Axis", null);
		return AXIS_NONE_2D;
	}

	/// <summary> Warrning! Bad perfomance!</summary>
	public static BaseInput[] GetAllInputs()
	{
		return inputDick.GetValueArray();
	}

	public static void RegisterAxis(string id)
	{
		AddInput(new Axes.Axis(id, id));
	}
	public static void RegisterAxis2D(string id, string axisX, string axisY)
	{
		AddInput(new Axes.Axis2D(id, axisX, axisY));
	}	
}}