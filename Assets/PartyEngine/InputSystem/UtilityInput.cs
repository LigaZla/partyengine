﻿namespace PartyEngine.InputSystem {
public static class UtilityInput 
{
	/// <summary> Warrning! Bad perfomance! </summary>
	/// <returns>Button is any pressed. Null if none pressed</returns>
	public static Buttons.BaseButton GetPressedButton()
	{
		BaseInput[] inputs = InputStorage.GetAllInputs();
		foreach(var element in inputs)
		{
			if(element is Buttons.BaseButton)
			{
				var btn = element as Buttons.BaseButton;
				if(btn.Down() || btn.Pressed())
					return btn;
			}
		}
		
		return null;
	}
	
	/// <summary> Warrning! Bad perfomance! </summary>
	/// <summary>If any btn pressed - logged his key </summary>
	public static void LogPressedButton()
	{
		var btn = GetPressedButton();
		if(btn != null)
			Debugger.Log(btn);
	}

	public static void InitializeInputSystems()
	{
		Buttons.Default.Initialize();
		
		InputStorage.RegisterAxis("Mouse X");
		InputStorage.RegisterAxis("Mouse Y");
		InputStorage.RegisterAxis("Mouse ScrollWheel");
	}
}}