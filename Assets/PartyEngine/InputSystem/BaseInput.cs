﻿
namespace PartyEngine.InputSystem {
public abstract class BaseInput
{
	public string Key { get; protected set;}
	
	public override string ToString()
	{
		return Key;
	}
}}