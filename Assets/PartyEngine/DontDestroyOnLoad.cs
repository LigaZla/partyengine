﻿using UnityEngine;

namespace PartyEngine {
public class DontDestroyOnLoad : MonoBehaviour 
{
	void Awake()
	{
		DontDestroyOnLoad(gameObject);
	}
}}