﻿using UnityEngine;
using PartyEngine.Ext;

namespace PartyEngine {
[System.Serializable]
public class PoolProperty
{
	public GameObject prefab;
	public int count;
	public Transform container;
	
	public Pool<T> CreatePool<T>() where T : Component
	{
		return new Pool<T>(prefab, count, container);
	}
}
public class Pool<T> where T : Component
{
	readonly GameObject prefab;
	readonly int count;
	readonly Transform container;
	
	public int Capacity { get { return pool.Length;}}
	public string PoolName { get { return "Pool of "+prefab.name;}}
	public bool Initialized { get { return pool != null;}}
	public bool Empty { get { return pool.IsNullOrEmpty();}}
	
	T[] pool;
	int currentIndex;
	System.Action<T> initAction;
	
	public Pool(PoolProperty proops):this(proops.prefab, proops.count, proops.container)
	{
		
	}
	public Pool(GameObject prefab, int count, Transform container)
	{
		this.prefab = prefab;
		this.count = count;
		this.container = container;
	}
	
	/// <summary>Fills the pool with objects</summary>
	/// <param name="initAction">action which call after object created</param>
	public void Initialize(System.Action<T> initAction = null)
	{
		if(initAction != null)
			this.initAction = initAction;
		
		pool = new T[count];
		for(int i=0; i<count; i++)
			pool[i] = CreateObject(i);
		
		currentIndex = -1;
	}
	T CreateObject(int id)
	{
		GameObject GO = Object.Instantiate(prefab, container);
		GO.SetActive(false);
		GO.name += " ID = "+id;

		T comp = GO.GetComponent<T>();
		initAction.SafetyInvoke(comp);
		return comp;
	}
	
	/// <param name="initAction">action which call after object created</param>
	public void SetInitAction(System.Action<T> initAction)
	{
		this.initAction = initAction;
	}
	
	public U Get<U>(Transform container = null)
	{
		if(container == null)
			return Get().GetComponent<U>();
		else
			return Get(container).GetComponent<U>();
	}
	/// <summary> Return and activate object.</summary>
	/// <param name="container">Container, whither need placed object</param>
	public T Get(Transform container)
	{
		T tempObj = Get();
		tempObj.transform.SetParent(container);
		
		return tempObj;
	}
	/// <summary>Return and activate object.</summary>
	public T Get()
	{
		for(int idx = 0; idx < pool.Length; idx++)
		{
			if(++currentIndex >= pool.Length) 
				currentIndex = 0;
			
			if(!pool[currentIndex].gameObject.activeInHierarchy)
				goto RETURN;
		}
		
		ResizeArray();
		
	RETURN:
		pool[currentIndex].gameObject.SetActive(true);
		return pool[currentIndex];
	}
	void ResizeArray()
	{
		#if UNITY_EDITOR || DEVELOPMENT_BUILD
		Debugger.LogWarning(PoolName+" need more objects");
		#endif
		// Create new array and fill him
		T[] newArray = new T[pool.Length*2];
		for(int i = 0; i < newArray.Length; i++)
			newArray[i] = i < pool.Length ? pool[i] : CreateObject(i);
		
		currentIndex = pool.Length;
		pool = newArray;
	}
	
	/// <summary>Return and object of index. Not activate </summary>
	public T GetOf(int index)
	{
		return pool[index];
	}
	
	public T[] GetAll()
	{
		return pool;
	}
	
	public void SetActiveAll(bool active)
	{
		foreach(T go in pool)
			go.gameObject.SetActive(active);
	}
	
	/// <summary>Destroy all objects</summary>
	public void DestroyAll()
	{
		foreach(T go in pool)
			Object.Destroy(go.gameObject);
		
		pool = new T[0];
		currentIndex = 0;
	}

}}