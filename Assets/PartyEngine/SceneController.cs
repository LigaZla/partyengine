﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace  PartyEngine {
public static class SceneController
{
	public static bool IsSceneActive(string name)
	{
		return UnityEngine.SceneManagement.SceneManager.GetActiveScene().name == name;
	}

	public static void LoadScene(string name)
	{
		UnityEngine.SceneManagement.SceneManager.LoadScene(name);
	}

	public static void LoadSceneAsync(string name)
	{
		UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(name);
	}
	
	public static void ReloadScene()
	{
		UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
	}

}}