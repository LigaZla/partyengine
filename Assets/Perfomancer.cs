﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PartyEngine;
using PartyEngine.Ext;
using PartyEngine.InputSystem;

public class Perfomancer : MonoBehaviour 
{
	public Image img;
	public Vector2Int size;

	void Awake() 
	{
		img.GetRectTransform().sizeDelta = size;
	}

	void Update() 
	{
		if(PartyInput.F12Down)
			StartTest();
	}

	void StartTest()
	{
		Debugger.StartWatch();
		
		Color color = new Color(1, 1, 1, 1);
		Texture2D texture = new Texture2D(size.x, size.y, TextureFormat.RGB24, true);
		
		for (int x = 0; x < size.x; x++)
		{
			for (int y = 0; y < size.y; y++)
			{
				color.r = Random.value;
				color.g = Random.value;
				color.b = Random.value;
				texture.SetPixel(x, y, color);
			}
		}
		texture.Apply();
		
		img.sprite = Sprite.Create(texture, new Rect(0, 0, size.x, size.y), new Vector2(0f, 0f));
		
		Debugger.StopAndLogWatch();
	}
}
