﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


[CustomPropertyDrawer(typeof(SceneReference))]
public class SceneReferencePropertyDrawer : PropertyDrawer
{
	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		var guid = property.FindPropertyRelative("guid");
		//var buildIndex = property.FindPropertyRelative("buildIndex");
		var name = property.FindPropertyRelative("name");
		var path = property.FindPropertyRelative("path");
		//var scene = FindSceneInBuildSettings(buildIndex.intValue);
		var scene = FindSceneInAssets(guid.stringValue);
		var newScene = EditorGUI.ObjectField(position, label, scene, typeof(SceneAsset), false) as SceneAsset;

		if(scene != newScene)
		{
			guid.stringValue = AssetDatabase.AssetPathToGUID(AssetDatabase.GetAssetOrScenePath(newScene));
			//buildIndex.intValue = SceneBuildIndex(AssetDatabase.GetAssetOrScenePath(newScene));
			/*name.stringValue = newScene.name;
			path.stringValue = EditorBuildSettings.scenes[buildIndex.intValue].path;*/
			 
			Debug.LogFormat("{0}: {1} ({2})", guid.stringValue, name.stringValue, path.stringValue);
		}

		EditorUtility.SetDirty(property.serializedObject.targetObject);
	}

	private SceneAsset FindSceneInBuildSettings(int buildIndex)
	{
		var result = default(SceneAsset);

		if (buildIndex > -1 && buildIndex < EditorBuildSettings.scenes.Length)
		{
			result = AssetDatabase.LoadAssetAtPath<SceneAsset>(EditorBuildSettings.scenes[buildIndex].path);
		}

		return result;
	}

	private SceneAsset FindSceneInAssets(string guid) {
		var result = default(SceneAsset);

		if (guid != string.Empty) {
			result = AssetDatabase.LoadAssetAtPath<SceneAsset>(UnityEditor.AssetDatabase.GUIDToAssetPath(guid));
		}

		return result;
	}

	private int SceneBuildIndex(string path)
	{
		for(int i = 0; i < EditorBuildSettings.scenes.Length; i++)
		{
			if(EditorBuildSettings.scenes[i].path == path)
			{
				return i;
			}
		}

		return -1;
	}
}