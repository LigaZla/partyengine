﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System;


[Serializable]
public sealed class SceneReference
{

    #region Private fields

    [SerializeField] private string guid;
    //[SerializeField] private int buildIndex;
    [SerializeField] private string path;
    [SerializeField] private string name;

    #endregion

    #region Public properties

    public Scene SceneObject
    {
        get { return SceneManager.GetSceneByName(name); }
    }


    public bool IsDirty
    {
        get { return SceneObject.isDirty; }
    }

    public bool IsLoaded
    {
        get { return SceneObject.isLoaded; }
    }

    public string Name
    {
        get { return name; }
    }

    public string Path
    {
        get { return path; }
    }

    public int RootCount
    {
        get { return SceneObject.rootCount; }
    }

    #endregion

    #region Constructors

    public SceneReference()
    {
       // buildIndex = -1;
    }


    #endregion

    #region Public methods

    public GameObject[] GetRootGameObjects()
    {
        return SceneObject.GetRootGameObjects();
    }


    #endregion

    #region Equality override


    #endregion

    #region Editor

    public void OnValidate() {
#if UNITY_EDITOR
        path = UnityEditor.AssetDatabase.GUIDToAssetPath(guid);
        name = System.IO.Path.GetFileNameWithoutExtension(path);
#endif
    }

    #endregion
}